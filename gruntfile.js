module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-html-snapshot');

    grunt.initConfig({
        htmlSnapshot: {
            all: {
                options: {
                    snapshotPath: 'snapshots/',
                    sitePath: 'http://localhost:8080/',
                    urls: ['#/about', '#/profile', '#/profile/profile/**']
                }
            }
        }
    });

    grunt.registerTask('default', ['htmlSnapshot']);
};