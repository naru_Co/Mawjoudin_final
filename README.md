# Mawjoudin
This application is a Majoudin organisation's social network, where users can share their opinions, questions, private life, experiences in security. 

## Development

This Social Network is a Monolithic web application which is developped using :
- Server Side : Java 8, Spring Boot, Spring Security, Maven, etc.. 
- Client Side : AngularJs, Jquery, HTML 5, CSS3, Bootstrap; WebSockets, etc..
- DataBase : PostgreSQL, Liquibase, Hazelcache, etc ... 


## Copyright
2017 © Naru .inc , All rights reserved