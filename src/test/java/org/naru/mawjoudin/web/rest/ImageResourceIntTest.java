package org.naru.mawjoudin.web.rest;

import org.naru.mawjoudin.Mawjoudin2App;

import org.naru.mawjoudin.domain.Image;
import org.naru.mawjoudin.repository.ImageRepository;
import org.naru.mawjoudin.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ImageResource REST controller.
 *
 * @see ImageResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Mawjoudin2App.class)
public class ImageResourceIntTest {

    // private static final String DEFAULT_HIGH_RES = "AAAAAAAAAA";
    // private static final String UPDATED_HIGH_RES = "BBBBBBBBBB";

    // private static final String DEFAULT_NORMAL_RES = "AAAAAAAAAA";
    // private static final String UPDATED_NORMAL_RES = "BBBBBBBBBB";

    // private static final String DEFAULT_MEDIUM_RES = "AAAAAAAAAA";
    // private static final String UPDATED_MEDIUM_RES = "BBBBBBBBBB";

    // private static final String DEFAULT_THUMBNAIL = "AAAAAAAAAA";
    // private static final String UPDATED_THUMBNAIL = "BBBBBBBBBB";

    // @Autowired
    // private ImageRepository imageRepository;

    // @Autowired
    // private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    // @Autowired
    // private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    // @Autowired
    // private ExceptionTranslator exceptionTranslator;

    // @Autowired
    // private EntityManager em;

    // private MockMvc restImageMockMvc;

    // private Image image;

    // @Before
    // public void setup() {
    //     MockitoAnnotations.initMocks(this);
    //     ImageResource imageResource = new ImageResource(imageRepository);
    //     this.restImageMockMvc = MockMvcBuilders.standaloneSetup(imageResource)
    //         .setCustomArgumentResolvers(pageableArgumentResolver)
    //         .setControllerAdvice(exceptionTranslator)
    //         .setMessageConverters(jacksonMessageConverter).build();
    // }

    // /**
    //  * Create an entity for this test.
    //  *
    //  * This is a static method, as tests for other entities might also need it,
    //  * if they test an entity which requires the current entity.
    //  */
    // public static Image createEntity(EntityManager em) {
    //     Image image = new Image()
    //         .highRes(DEFAULT_HIGH_RES)
    //         .normalRes(DEFAULT_NORMAL_RES)
    //         .mediumRes(DEFAULT_MEDIUM_RES)
    //         .thumbnail(DEFAULT_THUMBNAIL);
    //     return image;
    // }

    // @Before
    // public void initTest() {
    //     image = createEntity(em);
    // }

    // @Test
    // @Transactional
    // public void createImage() throws Exception {
    //     int databaseSizeBeforeCreate = imageRepository.findAll().size();

    //     // Create the Image
    //     restImageMockMvc.perform(post("/api/images")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(image)))
    //         .andExpect(status().isCreated());

    //     // Validate the Image in the database
    //     List<Image> imageList = imageRepository.findAll();
    //     assertThat(imageList).hasSize(databaseSizeBeforeCreate + 1);
    //     Image testImage = imageList.get(imageList.size() - 1);
    //     assertThat(testImage.getHighRes()).isEqualTo(DEFAULT_HIGH_RES);
    //     assertThat(testImage.getNormalRes()).isEqualTo(DEFAULT_NORMAL_RES);
    //     assertThat(testImage.getMediumRes()).isEqualTo(DEFAULT_MEDIUM_RES);
    //     assertThat(testImage.getThumbnail()).isEqualTo(DEFAULT_THUMBNAIL);
    // }

    // @Test
    // @Transactional
    // public void createImageWithExistingId() throws Exception {
    //     int databaseSizeBeforeCreate = imageRepository.findAll().size();

    //     // Create the Image with an existing ID
    //     image.setId(1L);

    //     // An entity with an existing ID cannot be created, so this API call must fail
    //     restImageMockMvc.perform(post("/api/images")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(image)))
    //         .andExpect(status().isBadRequest());

    //     // Validate the Alice in the database
    //     List<Image> imageList = imageRepository.findAll();
    //     assertThat(imageList).hasSize(databaseSizeBeforeCreate);
    // }

    // @Test
    // @Transactional
    // public void getAllImages() throws Exception {
    //     // Initialize the database
    //     imageRepository.saveAndFlush(image);

    //     // Get all the imageList
    //     restImageMockMvc.perform(get("/api/images?sort=id,desc"))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.[*].id").value(hasItem(image.getId().intValue())))
    //         .andExpect(jsonPath("$.[*].highRes").value(hasItem(DEFAULT_HIGH_RES.toString())))
    //         .andExpect(jsonPath("$.[*].normalRes").value(hasItem(DEFAULT_NORMAL_RES.toString())))
    //         .andExpect(jsonPath("$.[*].mediumRes").value(hasItem(DEFAULT_MEDIUM_RES.toString())))
    //         .andExpect(jsonPath("$.[*].thumbnail").value(hasItem(DEFAULT_THUMBNAIL.toString())));
    // }

    // @Test
    // @Transactional
    // public void getImage() throws Exception {
    //     // Initialize the database
    //     imageRepository.saveAndFlush(image);

    //     // Get the image
    //     restImageMockMvc.perform(get("/api/images/{id}", image.getId()))
    //         .andExpect(status().isOk())
    //         .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
    //         .andExpect(jsonPath("$.id").value(image.getId().intValue()))
    //         .andExpect(jsonPath("$.highRes").value(DEFAULT_HIGH_RES.toString()))
    //         .andExpect(jsonPath("$.normalRes").value(DEFAULT_NORMAL_RES.toString()))
    //         .andExpect(jsonPath("$.mediumRes").value(DEFAULT_MEDIUM_RES.toString()))
    //         .andExpect(jsonPath("$.thumbnail").value(DEFAULT_THUMBNAIL.toString()));
    // }

    // @Test
    // @Transactional
    // public void getNonExistingImage() throws Exception {
    //     // Get the image
    //     restImageMockMvc.perform(get("/api/images/{id}", Long.MAX_VALUE))
    //         .andExpect(status().isNotFound());
    // }

    // @Test
    // @Transactional
    // public void updateImage() throws Exception {
    //     // Initialize the database
    //     imageRepository.saveAndFlush(image);
    //     int databaseSizeBeforeUpdate = imageRepository.findAll().size();

    //     // Update the image
    //     Image updatedImage = imageRepository.findOne(image.getId());
    //     updatedImage
    //         .highRes(UPDATED_HIGH_RES)
    //         .normalRes(UPDATED_NORMAL_RES)
    //         .mediumRes(UPDATED_MEDIUM_RES)
    //         .thumbnail(UPDATED_THUMBNAIL);

    //     restImageMockMvc.perform(put("/api/images")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(updatedImage)))
    //         .andExpect(status().isOk());

    //     // Validate the Image in the database
    //     List<Image> imageList = imageRepository.findAll();
    //     assertThat(imageList).hasSize(databaseSizeBeforeUpdate);
    //     Image testImage = imageList.get(imageList.size() - 1);
    //     assertThat(testImage.getHighRes()).isEqualTo(UPDATED_HIGH_RES);
    //     assertThat(testImage.getNormalRes()).isEqualTo(UPDATED_NORMAL_RES);
    //     assertThat(testImage.getMediumRes()).isEqualTo(UPDATED_MEDIUM_RES);
    //     assertThat(testImage.getThumbnail()).isEqualTo(UPDATED_THUMBNAIL);
    // }

    // @Test
    // @Transactional
    // public void updateNonExistingImage() throws Exception {
    //     int databaseSizeBeforeUpdate = imageRepository.findAll().size();

    //     // Create the Image

    //     // If the entity doesn't have an ID, it will be created instead of just being updated
    //     restImageMockMvc.perform(put("/api/images")
    //         .contentType(TestUtil.APPLICATION_JSON_UTF8)
    //         .content(TestUtil.convertObjectToJsonBytes(image)))
    //         .andExpect(status().isCreated());

    //     // Validate the Image in the database
    //     List<Image> imageList = imageRepository.findAll();
    //     assertThat(imageList).hasSize(databaseSizeBeforeUpdate + 1);
    // }

    // @Test
    // @Transactional
    // public void deleteImage() throws Exception {
    //     // Initialize the database
    //     imageRepository.saveAndFlush(image);
    //     int databaseSizeBeforeDelete = imageRepository.findAll().size();

    //     // Get the image
    //     restImageMockMvc.perform(delete("/api/images/{id}", image.getId())
    //         .accept(TestUtil.APPLICATION_JSON_UTF8))
    //         .andExpect(status().isOk());

    //     // Validate the database is empty
    //     List<Image> imageList = imageRepository.findAll();
    //     assertThat(imageList).hasSize(databaseSizeBeforeDelete - 1);
    // }

    // @Test
    // @Transactional
    // public void equalsVerifier() throws Exception {
    //     TestUtil.equalsVerifier(Image.class);
    //     Image image1 = new Image();
    //     image1.setId(1L);
    //     Image image2 = new Image();
    //     image2.setId(image1.getId());
    //     assertThat(image1).isEqualTo(image2);
    //     image2.setId(2L);
    //     assertThat(image1).isNotEqualTo(image2);
    //     image1.setId(null);
    //     assertThat(image1).isNotEqualTo(image2);
    // }
}
