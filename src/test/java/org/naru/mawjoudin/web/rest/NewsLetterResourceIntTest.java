package org.naru.mawjoudin.web.rest;

import org.naru.mawjoudin.Mawjoudin2App;

import org.naru.mawjoudin.domain.NewsLetter;
import org.naru.mawjoudin.repository.NewsLetterRepository;
import org.naru.mawjoudin.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static org.naru.mawjoudin.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NewsLetterResource REST controller.
 *
 * @see NewsLetterResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Mawjoudin2App.class)
public class NewsLetterResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_SENDING_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_SENDING_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT = "BBBBBBBBBB";

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    @Autowired
    private NewsLetterRepository newsLetterRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNewsLetterMockMvc;

    private NewsLetter newsLetter;
  /**
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        NewsLetterResource newsLetterResource = new NewsLetterResource(newsLetterRepository);
        this.restNewsLetterMockMvc = MockMvcBuilders.standaloneSetup(newsLetterResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

  
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NewsLetter createEntity(EntityManager em) {
        NewsLetter newsLetter = new NewsLetter()
            .title(DEFAULT_TITLE)
            .creationDate(DEFAULT_CREATION_DATE)
            .sendingDate(DEFAULT_SENDING_DATE)
            .content(DEFAULT_CONTENT)
            .status(DEFAULT_STATUS);
        return newsLetter;
    }

    @Before
    public void initTest() {
        newsLetter = createEntity(em);
    }

    @Test
    @Transactional
    public void createNewsLetter() throws Exception {
        int databaseSizeBeforeCreate = newsLetterRepository.findAll().size();

        // Create the NewsLetter
        restNewsLetterMockMvc.perform(post("/api/news-letters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsLetter)))
            .andExpect(status().isCreated());

        // Validate the NewsLetter in the database
        List<NewsLetter> newsLetterList = newsLetterRepository.findAll();
        assertThat(newsLetterList).hasSize(databaseSizeBeforeCreate + 1);
        NewsLetter testNewsLetter = newsLetterList.get(newsLetterList.size() - 1);
        assertThat(testNewsLetter.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testNewsLetter.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testNewsLetter.getSendingDate()).isEqualTo(DEFAULT_SENDING_DATE);
        assertThat(testNewsLetter.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testNewsLetter.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createNewsLetterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = newsLetterRepository.findAll().size();

        // Create the NewsLetter with an existing ID
        newsLetter.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNewsLetterMockMvc.perform(post("/api/news-letters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsLetter)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<NewsLetter> newsLetterList = newsLetterRepository.findAll();
        assertThat(newsLetterList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllNewsLetters() throws Exception {
        // Initialize the database
        newsLetterRepository.saveAndFlush(newsLetter);

        // Get all the newsLetterList
        restNewsLetterMockMvc.perform(get("/api/news-letters?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(newsLetter.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(sameInstant(DEFAULT_CREATION_DATE))))
            .andExpect(jsonPath("$.[*].sendingDate").value(hasItem(sameInstant(DEFAULT_SENDING_DATE))))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));
    }

    @Test
    @Transactional
    public void getNewsLetter() throws Exception {
        // Initialize the database
        newsLetterRepository.saveAndFlush(newsLetter);

        // Get the newsLetter
        restNewsLetterMockMvc.perform(get("/api/news-letters/{id}", newsLetter.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(newsLetter.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.creationDate").value(sameInstant(DEFAULT_CREATION_DATE)))
            .andExpect(jsonPath("$.sendingDate").value(sameInstant(DEFAULT_SENDING_DATE)))
            .andExpect(jsonPath("$.content").value(DEFAULT_CONTENT.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS));
    }

    @Test
    @Transactional
    public void getNonExistingNewsLetter() throws Exception {
        // Get the newsLetter
        restNewsLetterMockMvc.perform(get("/api/news-letters/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNewsLetter() throws Exception {
        // Initialize the database
        newsLetterRepository.saveAndFlush(newsLetter);
        int databaseSizeBeforeUpdate = newsLetterRepository.findAll().size();

        // Update the newsLetter
        NewsLetter updatedNewsLetter = newsLetterRepository.findOne(newsLetter.getId());
        updatedNewsLetter
            .title(UPDATED_TITLE)
            .creationDate(UPDATED_CREATION_DATE)
            .sendingDate(UPDATED_SENDING_DATE)
            .content(UPDATED_CONTENT)
            .status(UPDATED_STATUS);

        restNewsLetterMockMvc.perform(put("/api/news-letters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNewsLetter)))
            .andExpect(status().isOk());

        // Validate the NewsLetter in the database
        List<NewsLetter> newsLetterList = newsLetterRepository.findAll();
        assertThat(newsLetterList).hasSize(databaseSizeBeforeUpdate);
        NewsLetter testNewsLetter = newsLetterList.get(newsLetterList.size() - 1);
        assertThat(testNewsLetter.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testNewsLetter.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testNewsLetter.getSendingDate()).isEqualTo(UPDATED_SENDING_DATE);
        assertThat(testNewsLetter.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testNewsLetter.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingNewsLetter() throws Exception {
        int databaseSizeBeforeUpdate = newsLetterRepository.findAll().size();

        // Create the NewsLetter

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNewsLetterMockMvc.perform(put("/api/news-letters")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(newsLetter)))
            .andExpect(status().isCreated());

        // Validate the NewsLetter in the database
        List<NewsLetter> newsLetterList = newsLetterRepository.findAll();
        assertThat(newsLetterList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNewsLetter() throws Exception {
        // Initialize the database
        newsLetterRepository.saveAndFlush(newsLetter);
        int databaseSizeBeforeDelete = newsLetterRepository.findAll().size();

        // Get the newsLetter
        restNewsLetterMockMvc.perform(delete("/api/news-letters/{id}", newsLetter.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<NewsLetter> newsLetterList = newsLetterRepository.findAll();
        assertThat(newsLetterList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NewsLetter.class);
        NewsLetter newsLetter1 = new NewsLetter();
        newsLetter1.setId(1L);
        NewsLetter newsLetter2 = new NewsLetter();
        newsLetter2.setId(newsLetter1.getId());
        assertThat(newsLetter1).isEqualTo(newsLetter2);
        newsLetter2.setId(2L);
        assertThat(newsLetter1).isNotEqualTo(newsLetter2);
        newsLetter1.setId(null);
        assertThat(newsLetter1).isNotEqualTo(newsLetter2);
    }
}
