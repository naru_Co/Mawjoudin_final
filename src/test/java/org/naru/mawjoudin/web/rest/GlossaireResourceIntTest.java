package org.naru.mawjoudin.web.rest;

import org.naru.mawjoudin.Mawjoudin2App;

import org.naru.mawjoudin.domain.Glossaire;
import org.naru.mawjoudin.repository.GlossaireRepository;
import org.naru.mawjoudin.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the GlossaireResource REST controller.
 *
 * @see GlossaireResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Mawjoudin2App.class)
public class GlossaireResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_LANG = 1;
    private static final Integer UPDATED_LANG = 2;

    @Autowired
    private GlossaireRepository glossaireRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restGlossaireMockMvc;

    private Glossaire glossaire;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        GlossaireResource glossaireResource = new GlossaireResource(glossaireRepository);
        this.restGlossaireMockMvc = MockMvcBuilders.standaloneSetup(glossaireResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Glossaire createEntity(EntityManager em) {
        Glossaire glossaire = new Glossaire()
            .title(DEFAULT_TITLE)
            .description(DEFAULT_DESCRIPTION)
            .lang(DEFAULT_LANG);
        return glossaire;
    }

    @Before
    public void initTest() {
        glossaire = createEntity(em);
    }

    @Test
    @Transactional
    public void createGlossaire() throws Exception {
        int databaseSizeBeforeCreate = glossaireRepository.findAll().size();

        // Create the Glossaire
        restGlossaireMockMvc.perform(post("/api/glossaires")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(glossaire)))
            .andExpect(status().isCreated());

        // Validate the Glossaire in the database
        List<Glossaire> glossaireList = glossaireRepository.findAll();
        assertThat(glossaireList).hasSize(databaseSizeBeforeCreate + 1);
        Glossaire testGlossaire = glossaireList.get(glossaireList.size() - 1);
        assertThat(testGlossaire.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testGlossaire.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testGlossaire.getLang()).isEqualTo(DEFAULT_LANG);
    }

    @Test
    @Transactional
    public void createGlossaireWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = glossaireRepository.findAll().size();

        // Create the Glossaire with an existing ID
        glossaire.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restGlossaireMockMvc.perform(post("/api/glossaires")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(glossaire)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Glossaire> glossaireList = glossaireRepository.findAll();
        assertThat(glossaireList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllGlossaires() throws Exception {
        // Initialize the database
        glossaireRepository.saveAndFlush(glossaire);

        // Get all the glossaireList
        restGlossaireMockMvc.perform(get("/api/glossaires?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(glossaire.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].lang").value(hasItem(DEFAULT_LANG)));
    }

    @Test
    @Transactional
    public void getGlossaire() throws Exception {
        // Initialize the database
        glossaireRepository.saveAndFlush(glossaire);

        // Get the glossaire
        restGlossaireMockMvc.perform(get("/api/glossaires/{id}", glossaire.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(glossaire.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.lang").value(DEFAULT_LANG));
    }

    @Test
    @Transactional
    public void getNonExistingGlossaire() throws Exception {
        // Get the glossaire
        restGlossaireMockMvc.perform(get("/api/glossaires/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGlossaire() throws Exception {
        // Initialize the database
        glossaireRepository.saveAndFlush(glossaire);
        int databaseSizeBeforeUpdate = glossaireRepository.findAll().size();

        // Update the glossaire
        Glossaire updatedGlossaire = glossaireRepository.findOne(glossaire.getId());
        updatedGlossaire
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .lang(UPDATED_LANG);

        restGlossaireMockMvc.perform(put("/api/glossaires")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedGlossaire)))
            .andExpect(status().isOk());

        // Validate the Glossaire in the database
        List<Glossaire> glossaireList = glossaireRepository.findAll();
        assertThat(glossaireList).hasSize(databaseSizeBeforeUpdate);
        Glossaire testGlossaire = glossaireList.get(glossaireList.size() - 1);
        assertThat(testGlossaire.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testGlossaire.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testGlossaire.getLang()).isEqualTo(UPDATED_LANG);
    }

    @Test
    @Transactional
    public void updateNonExistingGlossaire() throws Exception {
        int databaseSizeBeforeUpdate = glossaireRepository.findAll().size();

        // Create the Glossaire

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restGlossaireMockMvc.perform(put("/api/glossaires")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(glossaire)))
            .andExpect(status().isCreated());

        // Validate the Glossaire in the database
        List<Glossaire> glossaireList = glossaireRepository.findAll();
        assertThat(glossaireList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteGlossaire() throws Exception {
        // Initialize the database
        glossaireRepository.saveAndFlush(glossaire);
        int databaseSizeBeforeDelete = glossaireRepository.findAll().size();

        // Get the glossaire
        restGlossaireMockMvc.perform(delete("/api/glossaires/{id}", glossaire.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Glossaire> glossaireList = glossaireRepository.findAll();
        assertThat(glossaireList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Glossaire.class);
        Glossaire glossaire1 = new Glossaire();
        glossaire1.setId(1L);
        Glossaire glossaire2 = new Glossaire();
        glossaire2.setId(glossaire1.getId());
        assertThat(glossaire1).isEqualTo(glossaire2);
        glossaire2.setId(2L);
        assertThat(glossaire1).isNotEqualTo(glossaire2);
        glossaire1.setId(null);
        assertThat(glossaire1).isNotEqualTo(glossaire2);
    }
}
