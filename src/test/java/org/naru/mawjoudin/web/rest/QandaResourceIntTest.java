package org.naru.mawjoudin.web.rest;

import org.naru.mawjoudin.Mawjoudin2App;

import org.naru.mawjoudin.domain.Qanda;
import org.naru.mawjoudin.repository.QandaRepository;
import org.naru.mawjoudin.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the QandaResource REST controller.
 *
 * @see QandaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Mawjoudin2App.class)
public class QandaResourceIntTest {

    private static final String DEFAULT_QUESTION = "AAAAAAAAAA";
    private static final String UPDATED_QUESTION = "BBBBBBBBBB";

    private static final String DEFAULT_ANSWER = "AAAAAAAAAA";
    private static final String UPDATED_ANSWER = "BBBBBBBBBB";

    private static final Integer DEFAULT_LANG = 1;
    private static final Integer UPDATED_LANG = 2;

    @Autowired
    private QandaRepository qandaRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restQandaMockMvc;

    private Qanda qanda;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        QandaResource qandaResource = new QandaResource(qandaRepository);
        this.restQandaMockMvc = MockMvcBuilders.standaloneSetup(qandaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Qanda createEntity(EntityManager em) {
        Qanda qanda = new Qanda()
            .question(DEFAULT_QUESTION)
            .answer(DEFAULT_ANSWER)
            .lang(DEFAULT_LANG);
        return qanda;
    }

    @Before
    public void initTest() {
        qanda = createEntity(em);
    }

    @Test
    @Transactional
    public void createQanda() throws Exception {
        int databaseSizeBeforeCreate = qandaRepository.findAll().size();

        // Create the Qanda
        restQandaMockMvc.perform(post("/api/qandas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(qanda)))
            .andExpect(status().isCreated());

        // Validate the Qanda in the database
        List<Qanda> qandaList = qandaRepository.findAll();
        assertThat(qandaList).hasSize(databaseSizeBeforeCreate + 1);
        Qanda testQanda = qandaList.get(qandaList.size() - 1);
        assertThat(testQanda.getQuestion()).isEqualTo(DEFAULT_QUESTION);
        assertThat(testQanda.getAnswer()).isEqualTo(DEFAULT_ANSWER);
        assertThat(testQanda.getLang()).isEqualTo(DEFAULT_LANG);
    }

    @Test
    @Transactional
    public void createQandaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = qandaRepository.findAll().size();

        // Create the Qanda with an existing ID
        qanda.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restQandaMockMvc.perform(post("/api/qandas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(qanda)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Qanda> qandaList = qandaRepository.findAll();
        assertThat(qandaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllQandas() throws Exception {
        // Initialize the database
        qandaRepository.saveAndFlush(qanda);

        // Get all the qandaList
        restQandaMockMvc.perform(get("/api/qandas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(qanda.getId().intValue())))
            .andExpect(jsonPath("$.[*].question").value(hasItem(DEFAULT_QUESTION.toString())))
            .andExpect(jsonPath("$.[*].answer").value(hasItem(DEFAULT_ANSWER.toString())))
            .andExpect(jsonPath("$.[*].lang").value(hasItem(DEFAULT_LANG)));
    }

    @Test
    @Transactional
    public void getQanda() throws Exception {
        // Initialize the database
        qandaRepository.saveAndFlush(qanda);

        // Get the qanda
        restQandaMockMvc.perform(get("/api/qandas/{id}", qanda.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(qanda.getId().intValue()))
            .andExpect(jsonPath("$.question").value(DEFAULT_QUESTION.toString()))
            .andExpect(jsonPath("$.answer").value(DEFAULT_ANSWER.toString()))
            .andExpect(jsonPath("$.lang").value(DEFAULT_LANG));
    }

    @Test
    @Transactional
    public void getNonExistingQanda() throws Exception {
        // Get the qanda
        restQandaMockMvc.perform(get("/api/qandas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateQanda() throws Exception {
        // Initialize the database
        qandaRepository.saveAndFlush(qanda);
        int databaseSizeBeforeUpdate = qandaRepository.findAll().size();

        // Update the qanda
        Qanda updatedQanda = qandaRepository.findOne(qanda.getId());
        updatedQanda
            .question(UPDATED_QUESTION)
            .answer(UPDATED_ANSWER)
            .lang(UPDATED_LANG);

        restQandaMockMvc.perform(put("/api/qandas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedQanda)))
            .andExpect(status().isOk());

        // Validate the Qanda in the database
        List<Qanda> qandaList = qandaRepository.findAll();
        assertThat(qandaList).hasSize(databaseSizeBeforeUpdate);
        Qanda testQanda = qandaList.get(qandaList.size() - 1);
        assertThat(testQanda.getQuestion()).isEqualTo(UPDATED_QUESTION);
        assertThat(testQanda.getAnswer()).isEqualTo(UPDATED_ANSWER);
        assertThat(testQanda.getLang()).isEqualTo(UPDATED_LANG);
    }

    @Test
    @Transactional
    public void updateNonExistingQanda() throws Exception {
        int databaseSizeBeforeUpdate = qandaRepository.findAll().size();

        // Create the Qanda

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restQandaMockMvc.perform(put("/api/qandas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(qanda)))
            .andExpect(status().isCreated());

        // Validate the Qanda in the database
        List<Qanda> qandaList = qandaRepository.findAll();
        assertThat(qandaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteQanda() throws Exception {
        // Initialize the database
        qandaRepository.saveAndFlush(qanda);
        int databaseSizeBeforeDelete = qandaRepository.findAll().size();

        // Get the qanda
        restQandaMockMvc.perform(delete("/api/qandas/{id}", qanda.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Qanda> qandaList = qandaRepository.findAll();
        assertThat(qandaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Qanda.class);
        Qanda qanda1 = new Qanda();
        qanda1.setId(1L);
        Qanda qanda2 = new Qanda();
        qanda2.setId(qanda1.getId());
        assertThat(qanda1).isEqualTo(qanda2);
        qanda2.setId(2L);
        assertThat(qanda1).isNotEqualTo(qanda2);
        qanda1.setId(null);
        assertThat(qanda1).isNotEqualTo(qanda2);
    }
}
