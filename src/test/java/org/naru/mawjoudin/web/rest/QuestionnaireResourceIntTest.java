package org.naru.mawjoudin.web.rest;

import org.naru.mawjoudin.Mawjoudin2App;

import org.naru.mawjoudin.domain.Questionnaire;
import org.naru.mawjoudin.repository.QuestionnaireRepository;
import org.naru.mawjoudin.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static org.naru.mawjoudin.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the QuestionnaireResource REST controller.
 *
 * @see QuestionnaireResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Mawjoudin2App.class)
public class QuestionnaireResourceIntTest {

    private static final String DEFAULT_FULL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FULL_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_BIOLOGICAL_SEX = 1;
    private static final Integer UPDATED_BIOLOGICAL_SEX = 2;

    private static final Integer DEFAULT_GENDER_IDENTITY = 1;
    private static final Integer UPDATED_GENDER_IDENTITY = 2;

    private static final Integer DEFAULT_SEXUAL_ORIENTATION = 1;
    private static final Integer UPDATED_SEXUAL_ORIENTATION = 2;

    private static final Long DEFAULT_AGE = 1L;
    private static final Long UPDATED_AGE = 2L;

    private static final String DEFAULT_ADRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADRESS = "BBBBBBBBBB";

    private static final Integer DEFAULT_LIVE_IN = 1;
    private static final Integer UPDATED_LIVE_IN = 2;

    private static final String DEFAULT_PHONE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_NATIONALITY = "AAAAAAAAAA";
    private static final String UPDATED_NATIONALITY = "BBBBBBBBBB";

    private static final Integer DEFAULT_CONNECTION_WITH_LGBT = 1;
    private static final Integer UPDATED_CONNECTION_WITH_LGBT = 2;

    private static final Integer DEFAULT_EXTENT = 1;
    private static final Integer UPDATED_EXTENT = 2;

    private static final ZonedDateTime DEFAULT_TRUE_DATE_OF_INCIDENT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TRUE_DATE_OF_INCIDENT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_ADRESS_INCIDENT = "AAAAAAAAAA";
    private static final String UPDATED_ADRESS_INCIDENT = "BBBBBBBBBB";

    private static final Integer DEFAULT_PLACE_INCIDENT = 1;
    private static final Integer UPDATED_PLACE_INCIDENT = 2;

    private static final String DEFAULT_TYPE_OF_INCIDENT = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_OF_INCIDENT = "BBBBBBBBBB";

    private static final String DEFAULT_INCIDENT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_INCIDENT_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_OCCURENCE_INCIDENT = 1;
    private static final Integer UPDATED_OCCURENCE_INCIDENT = 2;

    private static final Integer DEFAULT_NUMBER_PERPETRATORS = 1;
    private static final Integer UPDATED_NUMBER_PERPETRATORS = 2;

    private static final String DEFAULT_THREAT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_THREAT_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_IDENTITYPERPETRATOR = 1;
    private static final Integer UPDATED_IDENTITYPERPETRATOR = 2;

    private static final String DEFAULT_IDENTIFIER_PERPETRATOR = "AAAAAAAAAA";
    private static final String UPDATED_IDENTIFIER_PERPETRATOR = "BBBBBBBBBB";

    private static final String DEFAULT_AGE_PERPETRATOR = "AAAAAAAAAA";
    private static final String UPDATED_AGE_PERPETRATOR = "BBBBBBBBBB";

    private static final String DEFAULT_WITNESS_REACTION = "AAAAAAAAAA";
    private static final String UPDATED_WITNESS_REACTION = "BBBBBBBBBB";

    private static final String DEFAULT_WITNESS_DETAIL = "AAAAAAAAAA";
    private static final String UPDATED_WITNESS_DETAIL = "BBBBBBBBBB";

    private static final Integer DEFAULT_INCIDENT_LINK = 1;
    private static final Integer UPDATED_INCIDENT_LINK = 2;

    private static final Integer DEFAULT_INCIDENT_LINK_INDIC = 1;
    private static final Integer UPDATED_INCIDENT_LINK_INDIC = 2;

    private static final Integer DEFAULT_IMPACT = 1;
    private static final Integer UPDATED_IMPACT = 2;

    private static final String DEFAULT_IMPACT_DETAIL = "AAAAAAAAAA";
    private static final String UPDATED_IMPACT_DETAIL = "BBBBBBBBBB";

    private static final Integer DEFAULT_MEDICAL_CARE = 1;
    private static final Integer UPDATED_MEDICAL_CARE = 2;

    private static final String DEFAULT_MEDICAL_STAFF = "AAAAAAAAAA";
    private static final String UPDATED_MEDICAL_STAFF = "BBBBBBBBBB";

    private static final String DEFAULT_PSY_IMACT = "AAAAAAAAAA";
    private static final String UPDATED_PSY_IMACT = "BBBBBBBBBB";

    private static final Integer DEFAULT_SUPPORT_KIND = 1;
    private static final Integer UPDATED_SUPPORT_KIND = 2;

    private static final Integer DEFAULT_POLICE = 1;
    private static final Integer UPDATED_POLICE = 2;

    private static final String DEFAULT_POLICE_DESC = "AAAAAAAAAA";
    private static final String UPDATED_POLICE_DESC = "BBBBBBBBBB";

    private static final String DEFAULT_FORCIBLY_POLICE = "AAAAAAAAAA";
    private static final String UPDATED_FORCIBLY_POLICE = "BBBBBBBBBB";

    private static final String DEFAULT_ARREST_PERIOD = "AAAAAAAAAA";
    private static final String UPDATED_ARREST_PERIOD = "BBBBBBBBBB";

    private static final Integer DEFAULT_ARREST_MOTIF = 1;
    private static final Integer UPDATED_ARREST_MOTIF = 2;

    private static final String DEFAULT_ARREST_DETAIL = "AAAAAAAAAA";
    private static final String UPDATED_ARREST_DETAIL = "BBBBBBBBBB";

    private static final Integer DEFAULT_INCIDENT_REPORT = 1;
    private static final Integer UPDATED_INCIDENT_REPORT = 2;

    private static final String DEFAULT_ANYTHING_TO_ADD = "AAAAAAAAAA";
    private static final String UPDATED_ANYTHING_TO_ADD = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private QuestionnaireRepository questionnaireRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restQuestionnaireMockMvc;

    private Questionnaire questionnaire;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        QuestionnaireResource questionnaireResource = new QuestionnaireResource(questionnaireRepository);
        this.restQuestionnaireMockMvc = MockMvcBuilders.standaloneSetup(questionnaireResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Questionnaire createEntity(EntityManager em) {
        Questionnaire questionnaire = new Questionnaire()
            .fullName(DEFAULT_FULL_NAME)
            .biologicalSex(DEFAULT_BIOLOGICAL_SEX)
            .genderIdentity(DEFAULT_GENDER_IDENTITY)
            .sexualOrientation(DEFAULT_SEXUAL_ORIENTATION)
            .age(DEFAULT_AGE)
            .adress(DEFAULT_ADRESS)
            .liveIn(DEFAULT_LIVE_IN)
            .phoneNumber(DEFAULT_PHONE_NUMBER)
            .email(DEFAULT_EMAIL)
            .nationality(DEFAULT_NATIONALITY)
            .connectionWithLgbt(DEFAULT_CONNECTION_WITH_LGBT)
            .extent(DEFAULT_EXTENT)
            .trueDateOfIncident(DEFAULT_TRUE_DATE_OF_INCIDENT)
            .adressIncident(DEFAULT_ADRESS_INCIDENT)
            .placeIncident(DEFAULT_PLACE_INCIDENT)
            .typeOfIncident(DEFAULT_TYPE_OF_INCIDENT)
            .incidentDescription(DEFAULT_INCIDENT_DESCRIPTION)
            .occurenceIncident(DEFAULT_OCCURENCE_INCIDENT)
            .numberPerpetrators(DEFAULT_NUMBER_PERPETRATORS)
            .threatDescription(DEFAULT_THREAT_DESCRIPTION)
            .identityperpetrator(DEFAULT_IDENTITYPERPETRATOR)
            .identifierPerpetrator(DEFAULT_IDENTIFIER_PERPETRATOR)
            .agePerpetrator(DEFAULT_AGE_PERPETRATOR)
            .witnessReaction(DEFAULT_WITNESS_REACTION)
            .witnessDetail(DEFAULT_WITNESS_DETAIL)
            .incidentLink(DEFAULT_INCIDENT_LINK)
            .incidentLinkIndic(DEFAULT_INCIDENT_LINK_INDIC)
            .impact(DEFAULT_IMPACT)
            .impactDetail(DEFAULT_IMPACT_DETAIL)
            .medicalCare(DEFAULT_MEDICAL_CARE)
            .medicalStaff(DEFAULT_MEDICAL_STAFF)
            .psyImact(DEFAULT_PSY_IMACT)
            .supportKind(DEFAULT_SUPPORT_KIND)
            .police(DEFAULT_POLICE)
            .policeDesc(DEFAULT_POLICE_DESC)
            .forciblyPolice(DEFAULT_FORCIBLY_POLICE)
            .arrestPeriod(DEFAULT_ARREST_PERIOD)
            .arrestMotif(DEFAULT_ARREST_MOTIF)
            .arrestDetail(DEFAULT_ARREST_DETAIL)
            .incidentReport(DEFAULT_INCIDENT_REPORT)
            .anythingToAdd(DEFAULT_ANYTHING_TO_ADD)
            .creationDate(DEFAULT_CREATION_DATE);
        return questionnaire;
    }

    @Before
    public void initTest() {
        questionnaire = createEntity(em);
    }

    @Test
    @Transactional
    public void createQuestionnaire() throws Exception {
        int databaseSizeBeforeCreate = questionnaireRepository.findAll().size();

        // Create the Questionnaire
        restQuestionnaireMockMvc.perform(post("/api/questionnaires")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(questionnaire)))
            .andExpect(status().isCreated());

        // Validate the Questionnaire in the database
        List<Questionnaire> questionnaireList = questionnaireRepository.findAll();
        assertThat(questionnaireList).hasSize(databaseSizeBeforeCreate + 1);
        Questionnaire testQuestionnaire = questionnaireList.get(questionnaireList.size() - 1);
        assertThat(testQuestionnaire.getFullName()).isEqualTo(DEFAULT_FULL_NAME);
        assertThat(testQuestionnaire.getBiologicalSex()).isEqualTo(DEFAULT_BIOLOGICAL_SEX);
        assertThat(testQuestionnaire.getGenderIdentity()).isEqualTo(DEFAULT_GENDER_IDENTITY);
        assertThat(testQuestionnaire.getSexualOrientation()).isEqualTo(DEFAULT_SEXUAL_ORIENTATION);
        assertThat(testQuestionnaire.getAge()).isEqualTo(DEFAULT_AGE);
        assertThat(testQuestionnaire.getAdress()).isEqualTo(DEFAULT_ADRESS);
        assertThat(testQuestionnaire.getLiveIn()).isEqualTo(DEFAULT_LIVE_IN);
        assertThat(testQuestionnaire.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
        assertThat(testQuestionnaire.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testQuestionnaire.getNationality()).isEqualTo(DEFAULT_NATIONALITY);
        assertThat(testQuestionnaire.getConnectionWithLgbt()).isEqualTo(DEFAULT_CONNECTION_WITH_LGBT);
        assertThat(testQuestionnaire.getExtent()).isEqualTo(DEFAULT_EXTENT);
        assertThat(testQuestionnaire.getTrueDateOfIncident()).isEqualTo(DEFAULT_TRUE_DATE_OF_INCIDENT);
        assertThat(testQuestionnaire.getAdressIncident()).isEqualTo(DEFAULT_ADRESS_INCIDENT);
        assertThat(testQuestionnaire.getPlaceIncident()).isEqualTo(DEFAULT_PLACE_INCIDENT);
        assertThat(testQuestionnaire.getTypeOfIncident()).isEqualTo(DEFAULT_TYPE_OF_INCIDENT);
        assertThat(testQuestionnaire.getIncidentDescription()).isEqualTo(DEFAULT_INCIDENT_DESCRIPTION);
        assertThat(testQuestionnaire.getOccurenceIncident()).isEqualTo(DEFAULT_OCCURENCE_INCIDENT);
        assertThat(testQuestionnaire.getNumberPerpetrators()).isEqualTo(DEFAULT_NUMBER_PERPETRATORS);
        assertThat(testQuestionnaire.getThreatDescription()).isEqualTo(DEFAULT_THREAT_DESCRIPTION);
        assertThat(testQuestionnaire.getIdentityperpetrator()).isEqualTo(DEFAULT_IDENTITYPERPETRATOR);
        assertThat(testQuestionnaire.getIdentifierPerpetrator()).isEqualTo(DEFAULT_IDENTIFIER_PERPETRATOR);
        assertThat(testQuestionnaire.getAgePerpetrator()).isEqualTo(DEFAULT_AGE_PERPETRATOR);
        assertThat(testQuestionnaire.getWitnessReaction()).isEqualTo(DEFAULT_WITNESS_REACTION);
        assertThat(testQuestionnaire.getWitnessDetail()).isEqualTo(DEFAULT_WITNESS_DETAIL);
        assertThat(testQuestionnaire.getIncidentLink()).isEqualTo(DEFAULT_INCIDENT_LINK);
        assertThat(testQuestionnaire.getIncidentLinkIndic()).isEqualTo(DEFAULT_INCIDENT_LINK_INDIC);
        assertThat(testQuestionnaire.getImpact()).isEqualTo(DEFAULT_IMPACT);
        assertThat(testQuestionnaire.getImpactDetail()).isEqualTo(DEFAULT_IMPACT_DETAIL);
        assertThat(testQuestionnaire.getMedicalCare()).isEqualTo(DEFAULT_MEDICAL_CARE);
        assertThat(testQuestionnaire.getMedicalStaff()).isEqualTo(DEFAULT_MEDICAL_STAFF);
        assertThat(testQuestionnaire.getPsyImact()).isEqualTo(DEFAULT_PSY_IMACT);
        assertThat(testQuestionnaire.getSupportKind()).isEqualTo(DEFAULT_SUPPORT_KIND);
        assertThat(testQuestionnaire.getPolice()).isEqualTo(DEFAULT_POLICE);
        assertThat(testQuestionnaire.getPoliceDesc()).isEqualTo(DEFAULT_POLICE_DESC);
        assertThat(testQuestionnaire.getForciblyPolice()).isEqualTo(DEFAULT_FORCIBLY_POLICE);
        assertThat(testQuestionnaire.getArrestPeriod()).isEqualTo(DEFAULT_ARREST_PERIOD);
        assertThat(testQuestionnaire.getArrestMotif()).isEqualTo(DEFAULT_ARREST_MOTIF);
        assertThat(testQuestionnaire.getArrestDetail()).isEqualTo(DEFAULT_ARREST_DETAIL);
        assertThat(testQuestionnaire.getIncidentReport()).isEqualTo(DEFAULT_INCIDENT_REPORT);
        assertThat(testQuestionnaire.getAnythingToAdd()).isEqualTo(DEFAULT_ANYTHING_TO_ADD);
        assertThat(testQuestionnaire.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
    }

    @Test
    @Transactional
    public void createQuestionnaireWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = questionnaireRepository.findAll().size();

        // Create the Questionnaire with an existing ID
        questionnaire.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restQuestionnaireMockMvc.perform(post("/api/questionnaires")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(questionnaire)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Questionnaire> questionnaireList = questionnaireRepository.findAll();
        assertThat(questionnaireList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllQuestionnaires() throws Exception {
        // Initialize the database
        questionnaireRepository.saveAndFlush(questionnaire);

        // Get all the questionnaireList
        restQuestionnaireMockMvc.perform(get("/api/questionnaires?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(questionnaire.getId().intValue())))
            .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME.toString())))
            .andExpect(jsonPath("$.[*].biologicalSex").value(hasItem(DEFAULT_BIOLOGICAL_SEX)))
            .andExpect(jsonPath("$.[*].genderIdentity").value(hasItem(DEFAULT_GENDER_IDENTITY)))
            .andExpect(jsonPath("$.[*].sexualOrientation").value(hasItem(DEFAULT_SEXUAL_ORIENTATION)))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE.intValue())))
            .andExpect(jsonPath("$.[*].adress").value(hasItem(DEFAULT_ADRESS.toString())))
            .andExpect(jsonPath("$.[*].liveIn").value(hasItem(DEFAULT_LIVE_IN)))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].nationality").value(hasItem(DEFAULT_NATIONALITY.toString())))
            .andExpect(jsonPath("$.[*].connectionWithLgbt").value(hasItem(DEFAULT_CONNECTION_WITH_LGBT)))
            .andExpect(jsonPath("$.[*].extent").value(hasItem(DEFAULT_EXTENT)))
            .andExpect(jsonPath("$.[*].trueDateOfIncident").value(hasItem(sameInstant(DEFAULT_TRUE_DATE_OF_INCIDENT))))
            .andExpect(jsonPath("$.[*].adressIncident").value(hasItem(DEFAULT_ADRESS_INCIDENT.toString())))
            .andExpect(jsonPath("$.[*].placeIncident").value(hasItem(DEFAULT_PLACE_INCIDENT)))
            .andExpect(jsonPath("$.[*].typeOfIncident").value(hasItem(DEFAULT_TYPE_OF_INCIDENT.toString())))
            .andExpect(jsonPath("$.[*].incidentDescription").value(hasItem(DEFAULT_INCIDENT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].occurenceIncident").value(hasItem(DEFAULT_OCCURENCE_INCIDENT)))
            .andExpect(jsonPath("$.[*].numberPerpetrators").value(hasItem(DEFAULT_NUMBER_PERPETRATORS)))
            .andExpect(jsonPath("$.[*].threatDescription").value(hasItem(DEFAULT_THREAT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].identityperpetrator").value(hasItem(DEFAULT_IDENTITYPERPETRATOR)))
            .andExpect(jsonPath("$.[*].identifierPerpetrator").value(hasItem(DEFAULT_IDENTIFIER_PERPETRATOR.toString())))
            .andExpect(jsonPath("$.[*].agePerpetrator").value(hasItem(DEFAULT_AGE_PERPETRATOR.toString())))
            .andExpect(jsonPath("$.[*].witnessReaction").value(hasItem(DEFAULT_WITNESS_REACTION.toString())))
            .andExpect(jsonPath("$.[*].witnessDetail").value(hasItem(DEFAULT_WITNESS_DETAIL.toString())))
            .andExpect(jsonPath("$.[*].incidentLink").value(hasItem(DEFAULT_INCIDENT_LINK)))
            .andExpect(jsonPath("$.[*].incidentLinkIndic").value(hasItem(DEFAULT_INCIDENT_LINK_INDIC)))
            .andExpect(jsonPath("$.[*].impact").value(hasItem(DEFAULT_IMPACT)))
            .andExpect(jsonPath("$.[*].impactDetail").value(hasItem(DEFAULT_IMPACT_DETAIL.toString())))
            .andExpect(jsonPath("$.[*].medicalCare").value(hasItem(DEFAULT_MEDICAL_CARE)))
            .andExpect(jsonPath("$.[*].medicalStaff").value(hasItem(DEFAULT_MEDICAL_STAFF.toString())))
            .andExpect(jsonPath("$.[*].psyImact").value(hasItem(DEFAULT_PSY_IMACT.toString())))
            .andExpect(jsonPath("$.[*].supportKind").value(hasItem(DEFAULT_SUPPORT_KIND)))
            .andExpect(jsonPath("$.[*].police").value(hasItem(DEFAULT_POLICE)))
            .andExpect(jsonPath("$.[*].policeDesc").value(hasItem(DEFAULT_POLICE_DESC.toString())))
            .andExpect(jsonPath("$.[*].forciblyPolice").value(hasItem(DEFAULT_FORCIBLY_POLICE.toString())))
            .andExpect(jsonPath("$.[*].arrestPeriod").value(hasItem(DEFAULT_ARREST_PERIOD.toString())))
            .andExpect(jsonPath("$.[*].arrestMotif").value(hasItem(DEFAULT_ARREST_MOTIF)))
            .andExpect(jsonPath("$.[*].arrestDetail").value(hasItem(DEFAULT_ARREST_DETAIL.toString())))
            .andExpect(jsonPath("$.[*].incidentReport").value(hasItem(DEFAULT_INCIDENT_REPORT)))
            .andExpect(jsonPath("$.[*].anythingToAdd").value(hasItem(DEFAULT_ANYTHING_TO_ADD.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(sameInstant(DEFAULT_CREATION_DATE))));
    }

    @Test
    @Transactional
    public void getQuestionnaire() throws Exception {
        // Initialize the database
        questionnaireRepository.saveAndFlush(questionnaire);

        // Get the questionnaire
        restQuestionnaireMockMvc.perform(get("/api/questionnaires/{id}", questionnaire.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(questionnaire.getId().intValue()))
            .andExpect(jsonPath("$.fullName").value(DEFAULT_FULL_NAME.toString()))
            .andExpect(jsonPath("$.biologicalSex").value(DEFAULT_BIOLOGICAL_SEX))
            .andExpect(jsonPath("$.genderIdentity").value(DEFAULT_GENDER_IDENTITY))
            .andExpect(jsonPath("$.sexualOrientation").value(DEFAULT_SEXUAL_ORIENTATION))
            .andExpect(jsonPath("$.age").value(DEFAULT_AGE.intValue()))
            .andExpect(jsonPath("$.adress").value(DEFAULT_ADRESS.toString()))
            .andExpect(jsonPath("$.liveIn").value(DEFAULT_LIVE_IN))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.nationality").value(DEFAULT_NATIONALITY.toString()))
            .andExpect(jsonPath("$.connectionWithLgbt").value(DEFAULT_CONNECTION_WITH_LGBT))
            .andExpect(jsonPath("$.extent").value(DEFAULT_EXTENT))
            .andExpect(jsonPath("$.trueDateOfIncident").value(sameInstant(DEFAULT_TRUE_DATE_OF_INCIDENT)))
            .andExpect(jsonPath("$.adressIncident").value(DEFAULT_ADRESS_INCIDENT.toString()))
            .andExpect(jsonPath("$.placeIncident").value(DEFAULT_PLACE_INCIDENT))
            .andExpect(jsonPath("$.typeOfIncident").value(DEFAULT_TYPE_OF_INCIDENT.toString()))
            .andExpect(jsonPath("$.incidentDescription").value(DEFAULT_INCIDENT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.occurenceIncident").value(DEFAULT_OCCURENCE_INCIDENT))
            .andExpect(jsonPath("$.numberPerpetrators").value(DEFAULT_NUMBER_PERPETRATORS))
            .andExpect(jsonPath("$.threatDescription").value(DEFAULT_THREAT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.identityperpetrator").value(DEFAULT_IDENTITYPERPETRATOR))
            .andExpect(jsonPath("$.identifierPerpetrator").value(DEFAULT_IDENTIFIER_PERPETRATOR.toString()))
            .andExpect(jsonPath("$.agePerpetrator").value(DEFAULT_AGE_PERPETRATOR.toString()))
            .andExpect(jsonPath("$.witnessReaction").value(DEFAULT_WITNESS_REACTION.toString()))
            .andExpect(jsonPath("$.witnessDetail").value(DEFAULT_WITNESS_DETAIL.toString()))
            .andExpect(jsonPath("$.incidentLink").value(DEFAULT_INCIDENT_LINK))
            .andExpect(jsonPath("$.incidentLinkIndic").value(DEFAULT_INCIDENT_LINK_INDIC))
            .andExpect(jsonPath("$.impact").value(DEFAULT_IMPACT))
            .andExpect(jsonPath("$.impactDetail").value(DEFAULT_IMPACT_DETAIL.toString()))
            .andExpect(jsonPath("$.medicalCare").value(DEFAULT_MEDICAL_CARE))
            .andExpect(jsonPath("$.medicalStaff").value(DEFAULT_MEDICAL_STAFF.toString()))
            .andExpect(jsonPath("$.psyImact").value(DEFAULT_PSY_IMACT.toString()))
            .andExpect(jsonPath("$.supportKind").value(DEFAULT_SUPPORT_KIND))
            .andExpect(jsonPath("$.police").value(DEFAULT_POLICE))
            .andExpect(jsonPath("$.policeDesc").value(DEFAULT_POLICE_DESC.toString()))
            .andExpect(jsonPath("$.forciblyPolice").value(DEFAULT_FORCIBLY_POLICE.toString()))
            .andExpect(jsonPath("$.arrestPeriod").value(DEFAULT_ARREST_PERIOD.toString()))
            .andExpect(jsonPath("$.arrestMotif").value(DEFAULT_ARREST_MOTIF))
            .andExpect(jsonPath("$.arrestDetail").value(DEFAULT_ARREST_DETAIL.toString()))
            .andExpect(jsonPath("$.incidentReport").value(DEFAULT_INCIDENT_REPORT))
            .andExpect(jsonPath("$.anythingToAdd").value(DEFAULT_ANYTHING_TO_ADD.toString()))
            .andExpect(jsonPath("$.creationDate").value(sameInstant(DEFAULT_CREATION_DATE)));
    }

    @Test
    @Transactional
    public void getNonExistingQuestionnaire() throws Exception {
        // Get the questionnaire
        restQuestionnaireMockMvc.perform(get("/api/questionnaires/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateQuestionnaire() throws Exception {
        // Initialize the database
        questionnaireRepository.saveAndFlush(questionnaire);
        int databaseSizeBeforeUpdate = questionnaireRepository.findAll().size();

        // Update the questionnaire
        Questionnaire updatedQuestionnaire = questionnaireRepository.findOne(questionnaire.getId());
        updatedQuestionnaire
            .fullName(UPDATED_FULL_NAME)
            .biologicalSex(UPDATED_BIOLOGICAL_SEX)
            .genderIdentity(UPDATED_GENDER_IDENTITY)
            .sexualOrientation(UPDATED_SEXUAL_ORIENTATION)
            .age(UPDATED_AGE)
            .adress(UPDATED_ADRESS)
            .liveIn(UPDATED_LIVE_IN)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .email(UPDATED_EMAIL)
            .nationality(UPDATED_NATIONALITY)
            .connectionWithLgbt(UPDATED_CONNECTION_WITH_LGBT)
            .extent(UPDATED_EXTENT)
            .trueDateOfIncident(UPDATED_TRUE_DATE_OF_INCIDENT)
            .adressIncident(UPDATED_ADRESS_INCIDENT)
            .placeIncident(UPDATED_PLACE_INCIDENT)
            .typeOfIncident(UPDATED_TYPE_OF_INCIDENT)
            .incidentDescription(UPDATED_INCIDENT_DESCRIPTION)
            .occurenceIncident(UPDATED_OCCURENCE_INCIDENT)
            .numberPerpetrators(UPDATED_NUMBER_PERPETRATORS)
            .threatDescription(UPDATED_THREAT_DESCRIPTION)
            .identityperpetrator(UPDATED_IDENTITYPERPETRATOR)
            .identifierPerpetrator(UPDATED_IDENTIFIER_PERPETRATOR)
            .agePerpetrator(UPDATED_AGE_PERPETRATOR)
            .witnessReaction(UPDATED_WITNESS_REACTION)
            .witnessDetail(UPDATED_WITNESS_DETAIL)
            .incidentLink(UPDATED_INCIDENT_LINK)
            .incidentLinkIndic(UPDATED_INCIDENT_LINK_INDIC)
            .impact(UPDATED_IMPACT)
            .impactDetail(UPDATED_IMPACT_DETAIL)
            .medicalCare(UPDATED_MEDICAL_CARE)
            .medicalStaff(UPDATED_MEDICAL_STAFF)
            .psyImact(UPDATED_PSY_IMACT)
            .supportKind(UPDATED_SUPPORT_KIND)
            .police(UPDATED_POLICE)
            .policeDesc(UPDATED_POLICE_DESC)
            .forciblyPolice(UPDATED_FORCIBLY_POLICE)
            .arrestPeriod(UPDATED_ARREST_PERIOD)
            .arrestMotif(UPDATED_ARREST_MOTIF)
            .arrestDetail(UPDATED_ARREST_DETAIL)
            .incidentReport(UPDATED_INCIDENT_REPORT)
            .anythingToAdd(UPDATED_ANYTHING_TO_ADD)
            .creationDate(UPDATED_CREATION_DATE);

        restQuestionnaireMockMvc.perform(put("/api/questionnaires")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedQuestionnaire)))
            .andExpect(status().isOk());

        // Validate the Questionnaire in the database
        List<Questionnaire> questionnaireList = questionnaireRepository.findAll();
        assertThat(questionnaireList).hasSize(databaseSizeBeforeUpdate);
        Questionnaire testQuestionnaire = questionnaireList.get(questionnaireList.size() - 1);
        assertThat(testQuestionnaire.getFullName()).isEqualTo(UPDATED_FULL_NAME);
        assertThat(testQuestionnaire.getBiologicalSex()).isEqualTo(UPDATED_BIOLOGICAL_SEX);
        assertThat(testQuestionnaire.getGenderIdentity()).isEqualTo(UPDATED_GENDER_IDENTITY);
        assertThat(testQuestionnaire.getSexualOrientation()).isEqualTo(UPDATED_SEXUAL_ORIENTATION);
        assertThat(testQuestionnaire.getAge()).isEqualTo(UPDATED_AGE);
        assertThat(testQuestionnaire.getAdress()).isEqualTo(UPDATED_ADRESS);
        assertThat(testQuestionnaire.getLiveIn()).isEqualTo(UPDATED_LIVE_IN);
        assertThat(testQuestionnaire.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testQuestionnaire.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testQuestionnaire.getNationality()).isEqualTo(UPDATED_NATIONALITY);
        assertThat(testQuestionnaire.getConnectionWithLgbt()).isEqualTo(UPDATED_CONNECTION_WITH_LGBT);
        assertThat(testQuestionnaire.getExtent()).isEqualTo(UPDATED_EXTENT);
        assertThat(testQuestionnaire.getTrueDateOfIncident()).isEqualTo(UPDATED_TRUE_DATE_OF_INCIDENT);
        assertThat(testQuestionnaire.getAdressIncident()).isEqualTo(UPDATED_ADRESS_INCIDENT);
        assertThat(testQuestionnaire.getPlaceIncident()).isEqualTo(UPDATED_PLACE_INCIDENT);
        assertThat(testQuestionnaire.getTypeOfIncident()).isEqualTo(UPDATED_TYPE_OF_INCIDENT);
        assertThat(testQuestionnaire.getIncidentDescription()).isEqualTo(UPDATED_INCIDENT_DESCRIPTION);
        assertThat(testQuestionnaire.getOccurenceIncident()).isEqualTo(UPDATED_OCCURENCE_INCIDENT);
        assertThat(testQuestionnaire.getNumberPerpetrators()).isEqualTo(UPDATED_NUMBER_PERPETRATORS);
        assertThat(testQuestionnaire.getThreatDescription()).isEqualTo(UPDATED_THREAT_DESCRIPTION);
        assertThat(testQuestionnaire.getIdentityperpetrator()).isEqualTo(UPDATED_IDENTITYPERPETRATOR);
        assertThat(testQuestionnaire.getIdentifierPerpetrator()).isEqualTo(UPDATED_IDENTIFIER_PERPETRATOR);
        assertThat(testQuestionnaire.getAgePerpetrator()).isEqualTo(UPDATED_AGE_PERPETRATOR);
        assertThat(testQuestionnaire.getWitnessReaction()).isEqualTo(UPDATED_WITNESS_REACTION);
        assertThat(testQuestionnaire.getWitnessDetail()).isEqualTo(UPDATED_WITNESS_DETAIL);
        assertThat(testQuestionnaire.getIncidentLink()).isEqualTo(UPDATED_INCIDENT_LINK);
        assertThat(testQuestionnaire.getIncidentLinkIndic()).isEqualTo(UPDATED_INCIDENT_LINK_INDIC);
        assertThat(testQuestionnaire.getImpact()).isEqualTo(UPDATED_IMPACT);
        assertThat(testQuestionnaire.getImpactDetail()).isEqualTo(UPDATED_IMPACT_DETAIL);
        assertThat(testQuestionnaire.getMedicalCare()).isEqualTo(UPDATED_MEDICAL_CARE);
        assertThat(testQuestionnaire.getMedicalStaff()).isEqualTo(UPDATED_MEDICAL_STAFF);
        assertThat(testQuestionnaire.getPsyImact()).isEqualTo(UPDATED_PSY_IMACT);
        assertThat(testQuestionnaire.getSupportKind()).isEqualTo(UPDATED_SUPPORT_KIND);
        assertThat(testQuestionnaire.getPolice()).isEqualTo(UPDATED_POLICE);
        assertThat(testQuestionnaire.getPoliceDesc()).isEqualTo(UPDATED_POLICE_DESC);
        assertThat(testQuestionnaire.getForciblyPolice()).isEqualTo(UPDATED_FORCIBLY_POLICE);
        assertThat(testQuestionnaire.getArrestPeriod()).isEqualTo(UPDATED_ARREST_PERIOD);
        assertThat(testQuestionnaire.getArrestMotif()).isEqualTo(UPDATED_ARREST_MOTIF);
        assertThat(testQuestionnaire.getArrestDetail()).isEqualTo(UPDATED_ARREST_DETAIL);
        assertThat(testQuestionnaire.getIncidentReport()).isEqualTo(UPDATED_INCIDENT_REPORT);
        assertThat(testQuestionnaire.getAnythingToAdd()).isEqualTo(UPDATED_ANYTHING_TO_ADD);
        assertThat(testQuestionnaire.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingQuestionnaire() throws Exception {
        int databaseSizeBeforeUpdate = questionnaireRepository.findAll().size();

        // Create the Questionnaire

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restQuestionnaireMockMvc.perform(put("/api/questionnaires")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(questionnaire)))
            .andExpect(status().isCreated());

        // Validate the Questionnaire in the database
        List<Questionnaire> questionnaireList = questionnaireRepository.findAll();
        assertThat(questionnaireList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteQuestionnaire() throws Exception {
        // Initialize the database
        questionnaireRepository.saveAndFlush(questionnaire);
        int databaseSizeBeforeDelete = questionnaireRepository.findAll().size();

        // Get the questionnaire
        restQuestionnaireMockMvc.perform(delete("/api/questionnaires/{id}", questionnaire.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Questionnaire> questionnaireList = questionnaireRepository.findAll();
        assertThat(questionnaireList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Questionnaire.class);
        Questionnaire questionnaire1 = new Questionnaire();
        questionnaire1.setId(1L);
        Questionnaire questionnaire2 = new Questionnaire();
        questionnaire2.setId(questionnaire1.getId());
        assertThat(questionnaire1).isEqualTo(questionnaire2);
        questionnaire2.setId(2L);
        assertThat(questionnaire1).isNotEqualTo(questionnaire2);
        questionnaire1.setId(null);
        assertThat(questionnaire1).isNotEqualTo(questionnaire2);
    }
}
