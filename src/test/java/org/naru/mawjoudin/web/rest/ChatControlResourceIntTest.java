package org.naru.mawjoudin.web.rest;

import org.naru.mawjoudin.Mawjoudin2App;

import org.naru.mawjoudin.domain.ChatControl;
import org.naru.mawjoudin.repository.ChatControlRepository;
import org.naru.mawjoudin.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static org.naru.mawjoudin.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ChatControlResource REST controller.
 *
 * @see ChatControlResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Mawjoudin2App.class)
public class ChatControlResourceIntTest {

    private static final ZonedDateTime DEFAULT_START_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_START_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_END_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_END_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    @Autowired
    private ChatControlRepository chatControlRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restChatControlMockMvc;

    private ChatControl chatControl;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ChatControlResource chatControlResource = new ChatControlResource(chatControlRepository);
        this.restChatControlMockMvc = MockMvcBuilders.standaloneSetup(chatControlResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ChatControl createEntity(EntityManager em) {
        ChatControl chatControl = new ChatControl()
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .status(DEFAULT_STATUS);
        return chatControl;
    }

    @Before
    public void initTest() {
        chatControl = createEntity(em);
    }

    @Test
    @Transactional
    public void createChatControl() throws Exception {
        int databaseSizeBeforeCreate = chatControlRepository.findAll().size();

        // Create the ChatControl
        restChatControlMockMvc.perform(post("/api/chat-controls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chatControl)))
            .andExpect(status().isCreated());

        // Validate the ChatControl in the database
        List<ChatControl> chatControlList = chatControlRepository.findAll();
        assertThat(chatControlList).hasSize(databaseSizeBeforeCreate + 1);
        ChatControl testChatControl = chatControlList.get(chatControlList.size() - 1);
        assertThat(testChatControl.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testChatControl.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testChatControl.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createChatControlWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = chatControlRepository.findAll().size();

        // Create the ChatControl with an existing ID
        chatControl.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restChatControlMockMvc.perform(post("/api/chat-controls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chatControl)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<ChatControl> chatControlList = chatControlRepository.findAll();
        assertThat(chatControlList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllChatControls() throws Exception {
        // Initialize the database
        chatControlRepository.saveAndFlush(chatControl);

        // Get all the chatControlList
        restChatControlMockMvc.perform(get("/api/chat-controls?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(chatControl.getId().intValue())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(sameInstant(DEFAULT_START_DATE))))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(sameInstant(DEFAULT_END_DATE))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)));
    }

    @Test
    @Transactional
    public void getChatControl() throws Exception {
        // Initialize the database
        chatControlRepository.saveAndFlush(chatControl);

        // Get the chatControl
        restChatControlMockMvc.perform(get("/api/chat-controls/{id}", chatControl.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(chatControl.getId().intValue()))
            .andExpect(jsonPath("$.startDate").value(sameInstant(DEFAULT_START_DATE)))
            .andExpect(jsonPath("$.endDate").value(sameInstant(DEFAULT_END_DATE)))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS));
    }

    @Test
    @Transactional
    public void getNonExistingChatControl() throws Exception {
        // Get the chatControl
        restChatControlMockMvc.perform(get("/api/chat-controls/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateChatControl() throws Exception {
        // Initialize the database
        chatControlRepository.saveAndFlush(chatControl);
        int databaseSizeBeforeUpdate = chatControlRepository.findAll().size();

        // Update the chatControl
        ChatControl updatedChatControl = chatControlRepository.findOne(chatControl.getId());
        updatedChatControl
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .status(UPDATED_STATUS);

        restChatControlMockMvc.perform(put("/api/chat-controls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedChatControl)))
            .andExpect(status().isOk());

        // Validate the ChatControl in the database
        List<ChatControl> chatControlList = chatControlRepository.findAll();
        assertThat(chatControlList).hasSize(databaseSizeBeforeUpdate);
        ChatControl testChatControl = chatControlList.get(chatControlList.size() - 1);
        assertThat(testChatControl.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testChatControl.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testChatControl.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingChatControl() throws Exception {
        int databaseSizeBeforeUpdate = chatControlRepository.findAll().size();

        // Create the ChatControl

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restChatControlMockMvc.perform(put("/api/chat-controls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(chatControl)))
            .andExpect(status().isCreated());

        // Validate the ChatControl in the database
        List<ChatControl> chatControlList = chatControlRepository.findAll();
        assertThat(chatControlList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteChatControl() throws Exception {
        // Initialize the database
        chatControlRepository.saveAndFlush(chatControl);
        int databaseSizeBeforeDelete = chatControlRepository.findAll().size();

        // Get the chatControl
        restChatControlMockMvc.perform(delete("/api/chat-controls/{id}", chatControl.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ChatControl> chatControlList = chatControlRepository.findAll();
        assertThat(chatControlList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChatControl.class);
        ChatControl chatControl1 = new ChatControl();
        chatControl1.setId(1L);
        ChatControl chatControl2 = new ChatControl();
        chatControl2.setId(chatControl1.getId());
        assertThat(chatControl1).isEqualTo(chatControl2);
        chatControl2.setId(2L);
        assertThat(chatControl1).isNotEqualTo(chatControl2);
        chatControl1.setId(null);
        assertThat(chatControl1).isNotEqualTo(chatControl2);
    }
}
