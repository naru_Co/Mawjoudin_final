package org.naru.mawjoudin.web.rest;

import org.naru.mawjoudin.Mawjoudin2App;

import org.naru.mawjoudin.domain.Slider;
import org.naru.mawjoudin.repository.SliderRepository;
import org.naru.mawjoudin.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SliderResource REST controller.
 *
 * @see SliderResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Mawjoudin2App.class)
public class SliderResourceIntTest {

    private static final String DEFAULT_HEADLLINE = "AAAAAAAAAA";
    private static final String UPDATED_HEADLLINE = "BBBBBBBBBB";

    private static final String DEFAULT_SIMPLE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_SIMPLE_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_HEADLINE_FR = "AAAAAAAAAA";
    private static final String UPDATED_HEADLINE_FR = "BBBBBBBBBB";

    private static final String DEFAULT_SIMPLE_TEXT_FR = "AAAAAAAAAA";
    private static final String UPDATED_SIMPLE_TEXT_FR = "BBBBBBBBBB";

    private static final String DEFAULT_HEADLINE_AR = "AAAAAAAAAA";
    private static final String UPDATED_HEADLINE_AR = "BBBBBBBBBB";

    private static final String DEFAULT_SIMPLE_TEXT_AR = "AAAAAAAAAA";
    private static final String UPDATED_SIMPLE_TEXT_AR = "BBBBBBBBBB";

    @Autowired
    private SliderRepository sliderRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSliderMockMvc;

    private Slider slider;
/**
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SliderResource sliderResource = new SliderResource(sliderRepository);
        this.restSliderMockMvc = MockMvcBuilders.standaloneSetup(sliderResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Slider createEntity(EntityManager em) {
        Slider slider = new Slider()
            .headlline(DEFAULT_HEADLLINE)
            .simpleText(DEFAULT_SIMPLE_TEXT)
            .headlineFr(DEFAULT_HEADLINE_FR)
            .simpleTextFr(DEFAULT_SIMPLE_TEXT_FR)
            .headlineAr(DEFAULT_HEADLINE_AR)
            .simpleTextAr(DEFAULT_SIMPLE_TEXT_AR);
        return slider;
    }

    @Before
    public void initTest() {
        slider = createEntity(em);
    }

    @Test
    @Transactional
    public void createSlider() throws Exception {
        int databaseSizeBeforeCreate = sliderRepository.findAll().size();

        // Create the Slider
        restSliderMockMvc.perform(post("/api/sliders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(slider)))
            .andExpect(status().isCreated());

        // Validate the Slider in the database
        List<Slider> sliderList = sliderRepository.findAll();
        assertThat(sliderList).hasSize(databaseSizeBeforeCreate + 1);
        Slider testSlider = sliderList.get(sliderList.size() - 1);
        assertThat(testSlider.getHeadlline()).isEqualTo(DEFAULT_HEADLLINE);
        assertThat(testSlider.getSimpleText()).isEqualTo(DEFAULT_SIMPLE_TEXT);
        assertThat(testSlider.getHeadlineFr()).isEqualTo(DEFAULT_HEADLINE_FR);
        assertThat(testSlider.getSimpleTextFr()).isEqualTo(DEFAULT_SIMPLE_TEXT_FR);
        assertThat(testSlider.getHeadlineAr()).isEqualTo(DEFAULT_HEADLINE_AR);
        assertThat(testSlider.getSimpleTextAr()).isEqualTo(DEFAULT_SIMPLE_TEXT_AR);
    }

    @Test
    @Transactional
    public void createSliderWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sliderRepository.findAll().size();

        // Create the Slider with an existing ID
        slider.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSliderMockMvc.perform(post("/api/sliders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(slider)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Slider> sliderList = sliderRepository.findAll();
        assertThat(sliderList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSliders() throws Exception {
        // Initialize the database
        sliderRepository.saveAndFlush(slider);

        // Get all the sliderList
        restSliderMockMvc.perform(get("/api/sliders?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(slider.getId().intValue())))
            .andExpect(jsonPath("$.[*].headlline").value(hasItem(DEFAULT_HEADLLINE.toString())))
            .andExpect(jsonPath("$.[*].simpleText").value(hasItem(DEFAULT_SIMPLE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].headlineFr").value(hasItem(DEFAULT_HEADLINE_FR.toString())))
            .andExpect(jsonPath("$.[*].simpleTextFr").value(hasItem(DEFAULT_SIMPLE_TEXT_FR.toString())))
            .andExpect(jsonPath("$.[*].headlineAr").value(hasItem(DEFAULT_HEADLINE_AR.toString())))
            .andExpect(jsonPath("$.[*].simpleTextAr").value(hasItem(DEFAULT_SIMPLE_TEXT_AR.toString())));
    }

    @Test
    @Transactional
    public void getSlider() throws Exception {
        // Initialize the database
        sliderRepository.saveAndFlush(slider);

        // Get the slider
        restSliderMockMvc.perform(get("/api/sliders/{id}", slider.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(slider.getId().intValue()))
            .andExpect(jsonPath("$.headlline").value(DEFAULT_HEADLLINE.toString()))
            .andExpect(jsonPath("$.simpleText").value(DEFAULT_SIMPLE_TEXT.toString()))
            .andExpect(jsonPath("$.headlineFr").value(DEFAULT_HEADLINE_FR.toString()))
            .andExpect(jsonPath("$.simpleTextFr").value(DEFAULT_SIMPLE_TEXT_FR.toString()))
            .andExpect(jsonPath("$.headlineAr").value(DEFAULT_HEADLINE_AR.toString()))
            .andExpect(jsonPath("$.simpleTextAr").value(DEFAULT_SIMPLE_TEXT_AR.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSlider() throws Exception {
        // Get the slider
        restSliderMockMvc.perform(get("/api/sliders/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSlider() throws Exception {
        // Initialize the database
        sliderRepository.saveAndFlush(slider);
        int databaseSizeBeforeUpdate = sliderRepository.findAll().size();

        // Update the slider
        Slider updatedSlider = sliderRepository.findOne(slider.getId());
        updatedSlider
            .headlline(UPDATED_HEADLLINE)
            .simpleText(UPDATED_SIMPLE_TEXT)
            .headlineFr(UPDATED_HEADLINE_FR)
            .simpleTextFr(UPDATED_SIMPLE_TEXT_FR)
            .headlineAr(UPDATED_HEADLINE_AR)
            .simpleTextAr(UPDATED_SIMPLE_TEXT_AR);

        restSliderMockMvc.perform(put("/api/sliders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSlider)))
            .andExpect(status().isOk());

        // Validate the Slider in the database
        List<Slider> sliderList = sliderRepository.findAll();
        assertThat(sliderList).hasSize(databaseSizeBeforeUpdate);
        Slider testSlider = sliderList.get(sliderList.size() - 1);
        assertThat(testSlider.getHeadlline()).isEqualTo(UPDATED_HEADLLINE);
        assertThat(testSlider.getSimpleText()).isEqualTo(UPDATED_SIMPLE_TEXT);
        assertThat(testSlider.getHeadlineFr()).isEqualTo(UPDATED_HEADLINE_FR);
        assertThat(testSlider.getSimpleTextFr()).isEqualTo(UPDATED_SIMPLE_TEXT_FR);
        assertThat(testSlider.getHeadlineAr()).isEqualTo(UPDATED_HEADLINE_AR);
        assertThat(testSlider.getSimpleTextAr()).isEqualTo(UPDATED_SIMPLE_TEXT_AR);
    }

    @Test
    @Transactional
    public void updateNonExistingSlider() throws Exception {
        int databaseSizeBeforeUpdate = sliderRepository.findAll().size();

        // Create the Slider

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSliderMockMvc.perform(put("/api/sliders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(slider)))
            .andExpect(status().isCreated());

        // Validate the Slider in the database
        List<Slider> sliderList = sliderRepository.findAll();
        assertThat(sliderList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSlider() throws Exception {
        // Initialize the database
        sliderRepository.saveAndFlush(slider);
        int databaseSizeBeforeDelete = sliderRepository.findAll().size();

        // Get the slider
        restSliderMockMvc.perform(delete("/api/sliders/{id}", slider.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Slider> sliderList = sliderRepository.findAll();
        assertThat(sliderList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Slider.class);
        Slider slider1 = new Slider();
        slider1.setId(1L);
        Slider slider2 = new Slider();
        slider2.setId(slider1.getId());
        assertThat(slider1).isEqualTo(slider2);
        slider2.setId(2L);
        assertThat(slider1).isNotEqualTo(slider2);
        slider1.setId(null);
        assertThat(slider1).isNotEqualTo(slider2);
    }
}
