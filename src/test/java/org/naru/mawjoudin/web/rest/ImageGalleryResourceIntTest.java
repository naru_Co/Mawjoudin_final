package org.naru.mawjoudin.web.rest;

import org.naru.mawjoudin.Mawjoudin2App;

import org.naru.mawjoudin.domain.ImageGallery;
import org.naru.mawjoudin.repository.ImageGalleryRepository;
import org.naru.mawjoudin.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static org.naru.mawjoudin.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ImageGalleryResource REST controller.
 *
 * @see ImageGalleryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Mawjoudin2App.class)
public class ImageGalleryResourceIntTest {

    private static final ZonedDateTime DEFAULT_CREATION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_TYPE = 1;
    private static final Integer UPDATED_TYPE = 2;

    @Autowired
    private ImageGalleryRepository imageGalleryRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restImageGalleryMockMvc;

    private ImageGallery imageGallery;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ImageGalleryResource imageGalleryResource = new ImageGalleryResource(imageGalleryRepository);
        this.restImageGalleryMockMvc = MockMvcBuilders.standaloneSetup(imageGalleryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ImageGallery createEntity(EntityManager em) {
        ImageGallery imageGallery = new ImageGallery()
            .creationDate(DEFAULT_CREATION_DATE)
            .description(DEFAULT_DESCRIPTION)
            .type(DEFAULT_TYPE);
        return imageGallery;
    }

    @Before
    public void initTest() {
        imageGallery = createEntity(em);
    }

    @Test
    @Transactional
    public void createImageGallery() throws Exception {
        int databaseSizeBeforeCreate = imageGalleryRepository.findAll().size();

        // Create the ImageGallery
        restImageGalleryMockMvc.perform(post("/api/image-galleries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(imageGallery)))
            .andExpect(status().isCreated());

        // Validate the ImageGallery in the database
        List<ImageGallery> imageGalleryList = imageGalleryRepository.findAll();
        assertThat(imageGalleryList).hasSize(databaseSizeBeforeCreate + 1);
        ImageGallery testImageGallery = imageGalleryList.get(imageGalleryList.size() - 1);
        assertThat(testImageGallery.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testImageGallery.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testImageGallery.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    public void createImageGalleryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = imageGalleryRepository.findAll().size();

        // Create the ImageGallery with an existing ID
        imageGallery.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restImageGalleryMockMvc.perform(post("/api/image-galleries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(imageGallery)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<ImageGallery> imageGalleryList = imageGalleryRepository.findAll();
        assertThat(imageGalleryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllImageGalleries() throws Exception {
        // Initialize the database
        imageGalleryRepository.saveAndFlush(imageGallery);

        // Get all the imageGalleryList
        restImageGalleryMockMvc.perform(get("/api/image-galleries?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(imageGallery.getId().intValue())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(sameInstant(DEFAULT_CREATION_DATE))))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)));
    }

    @Test
    @Transactional
    public void getImageGallery() throws Exception {
        // Initialize the database
        imageGalleryRepository.saveAndFlush(imageGallery);

        // Get the imageGallery
        restImageGalleryMockMvc.perform(get("/api/image-galleries/{id}", imageGallery.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(imageGallery.getId().intValue()))
            .andExpect(jsonPath("$.creationDate").value(sameInstant(DEFAULT_CREATION_DATE)))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE));
    }

    @Test
    @Transactional
    public void getNonExistingImageGallery() throws Exception {
        // Get the imageGallery
        restImageGalleryMockMvc.perform(get("/api/image-galleries/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateImageGallery() throws Exception {
        // Initialize the database
        imageGalleryRepository.saveAndFlush(imageGallery);
        int databaseSizeBeforeUpdate = imageGalleryRepository.findAll().size();

        // Update the imageGallery
        ImageGallery updatedImageGallery = imageGalleryRepository.findOne(imageGallery.getId());
        updatedImageGallery
            .creationDate(UPDATED_CREATION_DATE)
            .description(UPDATED_DESCRIPTION)
            .type(UPDATED_TYPE);

        restImageGalleryMockMvc.perform(put("/api/image-galleries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedImageGallery)))
            .andExpect(status().isOk());

        // Validate the ImageGallery in the database
        List<ImageGallery> imageGalleryList = imageGalleryRepository.findAll();
        assertThat(imageGalleryList).hasSize(databaseSizeBeforeUpdate);
        ImageGallery testImageGallery = imageGalleryList.get(imageGalleryList.size() - 1);
        assertThat(testImageGallery.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testImageGallery.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testImageGallery.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingImageGallery() throws Exception {
        int databaseSizeBeforeUpdate = imageGalleryRepository.findAll().size();

        // Create the ImageGallery

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restImageGalleryMockMvc.perform(put("/api/image-galleries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(imageGallery)))
            .andExpect(status().isCreated());

        // Validate the ImageGallery in the database
        List<ImageGallery> imageGalleryList = imageGalleryRepository.findAll();
        assertThat(imageGalleryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteImageGallery() throws Exception {
        // Initialize the database
        imageGalleryRepository.saveAndFlush(imageGallery);
        int databaseSizeBeforeDelete = imageGalleryRepository.findAll().size();

        // Get the imageGallery
        restImageGalleryMockMvc.perform(delete("/api/image-galleries/{id}", imageGallery.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ImageGallery> imageGalleryList = imageGalleryRepository.findAll();
        assertThat(imageGalleryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ImageGallery.class);
        ImageGallery imageGallery1 = new ImageGallery();
        imageGallery1.setId(1L);
        ImageGallery imageGallery2 = new ImageGallery();
        imageGallery2.setId(imageGallery1.getId());
        assertThat(imageGallery1).isEqualTo(imageGallery2);
        imageGallery2.setId(2L);
        assertThat(imageGallery1).isNotEqualTo(imageGallery2);
        imageGallery1.setId(null);
        assertThat(imageGallery1).isNotEqualTo(imageGallery2);
    }
}
