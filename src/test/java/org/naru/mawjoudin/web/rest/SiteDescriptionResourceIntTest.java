package org.naru.mawjoudin.web.rest;

import org.naru.mawjoudin.Mawjoudin2App;

import org.naru.mawjoudin.domain.SiteDescription;
import org.naru.mawjoudin.repository.SiteDescriptionRepository;
import org.naru.mawjoudin.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SiteDescriptionResource REST controller.
 *
 * @see SiteDescriptionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Mawjoudin2App.class)
public class SiteDescriptionResourceIntTest {

    private static final String DEFAULT_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT = "BBBBBBBBBB";

    private static final String DEFAULT_LANG = "AAAAAAAAAA";
    private static final String UPDATED_LANG = "BBBBBBBBBB";

    @Autowired
    private SiteDescriptionRepository siteDescriptionRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSiteDescriptionMockMvc;

    private SiteDescription siteDescription;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SiteDescriptionResource siteDescriptionResource = new SiteDescriptionResource(siteDescriptionRepository);
        this.restSiteDescriptionMockMvc = MockMvcBuilders.standaloneSetup(siteDescriptionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SiteDescription createEntity(EntityManager em) {
        SiteDescription siteDescription = new SiteDescription()
            .content(DEFAULT_CONTENT)
            .lang(DEFAULT_LANG);
        return siteDescription;
    }

    @Before
    public void initTest() {
        siteDescription = createEntity(em);
    }

    @Test
    @Transactional
    public void createSiteDescription() throws Exception {
        int databaseSizeBeforeCreate = siteDescriptionRepository.findAll().size();

        // Create the SiteDescription
        restSiteDescriptionMockMvc.perform(post("/api/site-descriptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteDescription)))
            .andExpect(status().isCreated());

        // Validate the SiteDescription in the database
        List<SiteDescription> siteDescriptionList = siteDescriptionRepository.findAll();
        assertThat(siteDescriptionList).hasSize(databaseSizeBeforeCreate + 1);
        SiteDescription testSiteDescription = siteDescriptionList.get(siteDescriptionList.size() - 1);
        assertThat(testSiteDescription.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testSiteDescription.getLang()).isEqualTo(DEFAULT_LANG);
    }

    @Test
    @Transactional
    public void createSiteDescriptionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = siteDescriptionRepository.findAll().size();

        // Create the SiteDescription with an existing ID
        siteDescription.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSiteDescriptionMockMvc.perform(post("/api/site-descriptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteDescription)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<SiteDescription> siteDescriptionList = siteDescriptionRepository.findAll();
        assertThat(siteDescriptionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSiteDescriptions() throws Exception {
        // Initialize the database
        siteDescriptionRepository.saveAndFlush(siteDescription);

        // Get all the siteDescriptionList
        restSiteDescriptionMockMvc.perform(get("/api/site-descriptions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(siteDescription.getId().intValue())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].lang").value(hasItem(DEFAULT_LANG.toString())));
    }

    @Test
    @Transactional
    public void getSiteDescription() throws Exception {
        // Initialize the database
        siteDescriptionRepository.saveAndFlush(siteDescription);

        // Get the siteDescription
        restSiteDescriptionMockMvc.perform(get("/api/site-descriptions/{id}", siteDescription.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(siteDescription.getId().intValue()))
            .andExpect(jsonPath("$.content").value(DEFAULT_CONTENT.toString()))
            .andExpect(jsonPath("$.lang").value(DEFAULT_LANG.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSiteDescription() throws Exception {
        // Get the siteDescription
        restSiteDescriptionMockMvc.perform(get("/api/site-descriptions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSiteDescription() throws Exception {
        // Initialize the database
        siteDescriptionRepository.saveAndFlush(siteDescription);
        int databaseSizeBeforeUpdate = siteDescriptionRepository.findAll().size();

        // Update the siteDescription
        SiteDescription updatedSiteDescription = siteDescriptionRepository.findOne(siteDescription.getId());
        updatedSiteDescription
            .content(UPDATED_CONTENT)
            .lang(UPDATED_LANG);

        restSiteDescriptionMockMvc.perform(put("/api/site-descriptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSiteDescription)))
            .andExpect(status().isOk());

        // Validate the SiteDescription in the database
        List<SiteDescription> siteDescriptionList = siteDescriptionRepository.findAll();
        assertThat(siteDescriptionList).hasSize(databaseSizeBeforeUpdate);
        SiteDescription testSiteDescription = siteDescriptionList.get(siteDescriptionList.size() - 1);
        assertThat(testSiteDescription.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testSiteDescription.getLang()).isEqualTo(UPDATED_LANG);
    }

    @Test
    @Transactional
    public void updateNonExistingSiteDescription() throws Exception {
        int databaseSizeBeforeUpdate = siteDescriptionRepository.findAll().size();

        // Create the SiteDescription

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSiteDescriptionMockMvc.perform(put("/api/site-descriptions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(siteDescription)))
            .andExpect(status().isCreated());

        // Validate the SiteDescription in the database
        List<SiteDescription> siteDescriptionList = siteDescriptionRepository.findAll();
        assertThat(siteDescriptionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSiteDescription() throws Exception {
        // Initialize the database
        siteDescriptionRepository.saveAndFlush(siteDescription);
        int databaseSizeBeforeDelete = siteDescriptionRepository.findAll().size();

        // Get the siteDescription
        restSiteDescriptionMockMvc.perform(delete("/api/site-descriptions/{id}", siteDescription.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SiteDescription> siteDescriptionList = siteDescriptionRepository.findAll();
        assertThat(siteDescriptionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SiteDescription.class);
        SiteDescription siteDescription1 = new SiteDescription();
        siteDescription1.setId(1L);
        SiteDescription siteDescription2 = new SiteDescription();
        siteDescription2.setId(siteDescription1.getId());
        assertThat(siteDescription1).isEqualTo(siteDescription2);
        siteDescription2.setId(2L);
        assertThat(siteDescription1).isNotEqualTo(siteDescription2);
        siteDescription1.setId(null);
        assertThat(siteDescription1).isNotEqualTo(siteDescription2);
    }
}
