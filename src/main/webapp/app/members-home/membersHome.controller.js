(function () {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('membershomeController', membershomeController);

    membershomeController.$inject = ['$filter', 'AlertService', 'FriendsByProfile', 'Principal', '$scope', 'DataUtils', 'PublicationsByProfileAndTime', 'BlogByProfileAndStateAndTime', 'NewsOfyesterday', 'ProjectOfYesterday', 'AgendaOfYesterdayByProfil', 'CommentByPublication', 'PubComment'];

    function membershomeController($filter, AlertService, FriendsByProfile, Principal, $scope, DataUtils, PublicationsByProfileAndTime, BlogByProfileAndStateAndTime, NewsOfyesterday, ProjectOfYesterday, AgendaOfYesterdayByProfil, CommentByPublication, PubComment) {

        var vm = this;
        getAccount();
        function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
                if (vm.account != null) {
                    vm.isAuthenticated = Principal.isAuthenticated;
                    loadAll(account)
                }
            });
        }


        vm.publications = [];
        vm.blogs = [];

        $scope.recentArticles = [];
        $scope.recentProjects = [];
        $scope.currentActivities = [];

        function loadAll(account) {

            $scope.recentArticles = NewsOfyesterday.get()
            $scope.recentProjects = ProjectOfYesterday.get();
            $scope.currentActivities = AgendaOfYesterdayByProfil.get({ profil_id: account.profile.id }, function (result) {
                for (var i = 0; i < result.length; i++) {
                    result[i].startsAt = new Date(result[i].startsAt);
                    result[i].endsAt = new Date(result[i].endsAt);
                }
                $scope.currentActivities = result
            })
            loadAllFriends(account);


        }


        function loadAllFriends(account) {


            FriendsByProfile.get({
                receiver_id: account.profile.id,
                state: 1

            }, onSuccess, onError);


            function onSuccess(data, headers) {

                for (var i = 0; i < data.length; i++) {
                    PublicationsByProfileAndTime.get({ profil_id: data[i].id }, function (result) {
                        for (var j = 0; j < result.length; j++) {
                            vm.publications.push(result[j]);
                            loadAllComments(result[j])
                        }
                    });
                    BlogByProfileAndStateAndTime.get({ profil_id: data[i].id }, function (result) {
                        for (var j = 0; j < result.length; j++) {
                            vm.blogs.push(result[j]);

                        }
                    })

                }


            }

            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        // comments part //

        vm.comment = {};
        vm.comments = [];
        vm.saveComment = saveComment;
        vm.loadAllComments = loadAllComments;


        function loadAllComments(publication) {
            var translate = $filter('translate');
            translate('netbedlooApp.profile.follow')
            var comments = CommentByPublication.get({
                pub_id: publication.id
            }, function (data) {
                for (var j = 0; j < data.length; j++) {
                    var today = new Date();
                    var Christmas = new Date(data[j].creationDate);
                    var diffMs = (today - Christmas);
                    var diffDays = Math.floor(diffMs / 86400000);
                    var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
                    var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);
                    // minutes
                    if(diffDays===0){
                        if(diffHrs===0){
                            if(diffMins===0){
                             data[j].diff=translate('membersHome.seconds')   
                            }else if(diffMins<60){
                             data[j].diff=diffMins+' '+translate('membersHome.minutes')
                            }
                         }else if(diffHrs<24){
                             data[j].diff=diffHrs+' '+translate('membersHome.hours')
                         }
                        }else if(diffDays<7){
                         data[j].diff=diffDays+' '+translate('membersHome.days')
                        }else{
                         data[j].diff=0   
                        }
                    }
                  
                


                publication.comments = data
            });

        }


        $scope.loadMore = function (comments) {
            $scope.x = comments.length;
        }
       

        function saveComment(publication) {

            vm.isSaving = true;
            vm.comment.publication = publication;
            vm.comment.profile = vm.account.profile;
            vm.comment.creationDate = new Date();
            PubComment.save(vm.comment, function (result) {
                $scope.$emit('mawjoudin2App:commentUpdate', result);
                vm.isSaving = false;
                loadAllComments(publication)
                vm.comment = {};
            });

        }


    }
})();