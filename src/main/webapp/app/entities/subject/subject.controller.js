(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('SubjectController', SubjectController);

    SubjectController.$inject = ['$scope','Subject','Principal'];

    function SubjectController($scope,Subject,Principal) {

        var vm = this;
$scope.searchFn="";
        vm.subjects = [];
        vm.getAccount = getAccount;
function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;

            });
        }
        getAccount();
        loadAll();

        function loadAll() {
            Subject.query(function(result) {
                vm.subjects = result;
                vm.searchQuery = null;
            });
        }
    }
})();
