(function() {
    'use strict';
    angular
        .module('mawjoudin2App')
        .factory('Subject', Subject);

    Subject.$inject = ['$resource', 'DateUtils'];

    function Subject ($resource, DateUtils) {
        var resourceUrl =  'api/subjects/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creationDate = DateUtils.convertDateTimeFromServer(data.creationDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
