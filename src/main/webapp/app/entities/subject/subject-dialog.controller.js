(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('SubjectDialogController', SubjectDialogController);

    SubjectDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Subject', 'Principal', 'PubComment', 'Profile'];

    function SubjectDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Subject, Principal, PubComment, Profile) {
        var vm = this;

        vm.subject = entity;
        vm.clear = clear;
        vm.save = save;
        vm.getAccount = getAccount;
          
          
          function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
              
                vm.isAuthenticated = Principal.isAuthenticated;

            });
        }
        getAccount();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.subject.id !== null) {
                Subject.update(vm.subject, onSaveSuccess, onSaveError);
            } else {
                vm.subject.profile=vm.account.profile
                Subject.save(vm.subject, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('mawjoudin2App:subjectUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        
    }
})();
