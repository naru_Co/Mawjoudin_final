(function () {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('SubjectDetailController', SubjectDetailController);

    SubjectDetailController.$inject = ['$scope', '$rootScope', '$stateParams', '$mdDialog', 'previousState', 'ParseLinks', 'paginationConstants', 'entity', 'Principal', 'Subject', 'PubComment', 'Profile', 'CommentByForum'];

    function SubjectDetailController($scope, $rootScope, $stateParams, $mdDialog, previousState, ParseLinks, paginationConstants, entity, Principal, Subject, PubComment, Profile, CommentByForum) {
        var vm = this;

        vm.subject = entity;
        vm.previousState = previousState.name;
        vm.getAccount = getAccount;


        function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;

            });
        }
        getAccount();
        /* -----------------------------------------------------------------------------------------

                                 Working with Publications and Comments
                                                                   
        --------------------------------------------------------------------------------------------- */

        /*--------------------------------Publication Save and Get -------------------------*/
        vm.comment = {};
        vm.comments = [];
        vm.loadPage = loadPage;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.page = 0;
        vm.links = {
            last: 0
        };
        vm.predicate = 'id';
        vm.reverse = true;
        vm.loadAll = loadAll;
        vm.save = save;
        loadAll()

        function loadAll() {


            CommentByForum.get({
                subject_id: vm.subject.id,
                page: vm.page,
                size: vm.itemsPerPage,
                sort: sort()
            }, onSuccess, onError);




            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                for (var i = 0; i < data.length; i++) {

                    vm.comments.push(data[i]);

                }
            }

            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function reset() {
            vm.page = 0;
            vm.comments = [];
            loadAll();
        }

        function loadPage(page) {
            vm.page = page;
            loadAll();
        }

        function clear() {
            vm.comments = [];
            vm.links = {
                last: 0
            };
            vm.page = 0;
            vm.predicate = 'id';
            vm.reverse = true;
            vm.searchQuery = null;
            vm.currentSearch = null;
            vm.loadAll();
        }
        function save() {
            vm.isSaving = true;
            vm.comment.profile = vm.account.profile;
            vm.comment.subject = vm.subject;
            PubComment.save(vm.comment, onSaveSuccess, onSaveError);

        }

        function onSaveSuccess(result) {
            $scope.$emit('mawjoudin2App:commentUpdate', result);
            vm.isSaving = false;
            vm.comments.push(vm.comment)
            vm.comment = {};
        }

        function onSaveError() {
            vm.isSaving = false;
        }
        $scope.showConfirm4 = function (ev, comment) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Effacer Mon Commentaire')
                .textContent('Êtes-vous sûr ?')
                .ariaLabel('Je Supprime!')
                .targetEvent(ev)
                .ok('Oui!')
                .cancel('Non');

            $mdDialog.show(confirm).then(function () {
                deleteComment(comment)

            }, function () { });
        };
        function deleteComment(comment) {
            var disComment = vm.comments.indexOf(comment);
            vm.comments.splice(disComment, 1);
            PubComment.delete({ id: comment.id });
        }

        var unsubscribe = $rootScope.$on('mawjoudin2App:subjectUpdate', function (event, result) {
            vm.subject = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
