(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('chat-control', {
            parent: 'entity',
            url: '/chat-control',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'mawjoudin2App.chatControl.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/chat-control/chat-controls.html',
                    controller: 'ChatControlController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('chatControl');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('chat-control-detail', {
            parent: 'chat-control',
            url: '/chat-control/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'mawjoudin2App.chatControl.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/chat-control/chat-control-detail.html',
                    controller: 'ChatControlDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('chatControl');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ChatControl', function($stateParams, ChatControl) {
                    return ChatControl.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'chat-control',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('chat-control-detail.edit', {
            parent: 'chat-control-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/chat-control/chat-control-dialog.html',
                    controller: 'ChatControlDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ChatControl', function(ChatControl) {
                            return ChatControl.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('chat-control.new', {
            parent: 'chat-control',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/chat-control/chat-control-dialog.html',
                    controller: 'ChatControlDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                startDate: null,
                                endDate: null,
                                status: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('chat-control', null, { reload: 'chat-control' });
                }, function() {
                    $state.go('chat-control');
                });
            }]
        })
        .state('chat-control.edit', {
            parent: 'chat-control',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/chat-control/chat-control-dialog.html',
                    controller: 'ChatControlDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ChatControl', function(ChatControl) {
                            return ChatControl.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('chat-control', null, { reload: 'chat-control' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('chat-control.delete', {
            parent: 'chat-control',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/chat-control/chat-control-delete-dialog.html',
                    controller: 'ChatControlDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ChatControl', function(ChatControl) {
                            return ChatControl.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('chat-control', null, { reload: 'chat-control' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
