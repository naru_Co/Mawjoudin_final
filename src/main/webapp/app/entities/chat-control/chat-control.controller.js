(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('ChatControlController', ChatControlController);

    ChatControlController.$inject = ['ChatControl'];

    function ChatControlController(ChatControl) {

        var vm = this;

        vm.chatControls = [];

        loadAll();

        function loadAll() {
            ChatControl.query(function(result) {
                vm.chatControls = result;
                vm.searchQuery = null;
            });
        }
    }
})();
