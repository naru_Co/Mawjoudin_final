(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('ChatControlDetailController', ChatControlDetailController);

    ChatControlDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ChatControl'];

    function ChatControlDetailController($scope, $rootScope, $stateParams, previousState, entity, ChatControl) {
        var vm = this;

        vm.chatControl = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('mawjoudin2App:chatControlUpdate', function(event, result) {
            vm.chatControl = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
