(function() {
    'use strict';
    angular
        .module('mawjoudin2App')
        .factory('ChatControl', ChatControl);

    ChatControl.$inject = ['$resource', 'DateUtils'];

    function ChatControl ($resource, DateUtils) {
        var resourceUrl =  'api/chat-controls/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.startDate = DateUtils.convertDateTimeFromServer(data.startDate);
                        data.endDate = DateUtils.convertDateTimeFromServer(data.endDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
