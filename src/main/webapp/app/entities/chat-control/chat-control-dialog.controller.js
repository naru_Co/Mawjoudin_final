(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('ChatControlDialogController', ChatControlDialogController);

    ChatControlDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ChatControl'];

    function ChatControlDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ChatControl) {
        var vm = this;

        vm.chatControl = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.chatControl.id !== null) {
                ChatControl.update(vm.chatControl, onSaveSuccess, onSaveError);
            } else {
                ChatControl.save(vm.chatControl, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('mawjoudin2App:chatControlUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.startDate = false;
        vm.datePickerOpenStatus.endDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
