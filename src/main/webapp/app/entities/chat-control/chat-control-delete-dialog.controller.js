(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('ChatControlDeleteController',ChatControlDeleteController);

    ChatControlDeleteController.$inject = ['$uibModalInstance', 'entity', 'ChatControl'];

    function ChatControlDeleteController($uibModalInstance, entity, ChatControl) {
        var vm = this;

        vm.chatControl = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ChatControl.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
