(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('PublicationDialogController', PublicationDialogController);

    PublicationDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Publication', 'Image', 'Profile'];

    function PublicationDialogController($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Publication, Image, Profile) {
        var vm = this;

        vm.publication = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.images = Image.query({ filter: 'publication-is-null' });
        $q.all([vm.publication.$promise, vm.images.$promise]).then(function() {
            if (!vm.publication.image || !vm.publication.image.id) {
                return $q.reject();
            }
            return Image.get({ id: vm.publication.image.id }).$promise;
        }).then(function(image) {
            vm.images.push(image);
        });
        vm.profiles = Profile.query();

        $timeout(function() {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.publication.id !== null) {
                Publication.update(vm.publication, onSaveSuccess, onSaveError);
            } else {
                Publication.save(vm.publication, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('mawjoudin2App:publicationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.creationDate = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();