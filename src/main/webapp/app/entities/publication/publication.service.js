(function() {
    'use strict';
    angular
        .module('mawjoudin2App')
        .factory('Publication', Publication)
        .factory('PublicationsByProfileAndTime', PublicationsByProfileAndTime)
        .factory('PublicationsByProfile', PublicationsByProfile);



    PublicationsByProfile.$inject = ['$resource', 'DateUtils'];

    function PublicationsByProfile($resource, DateUtils) {
        var resourceUrl = 'api/publicationsbyprofil/:profil_id';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });
    }
    PublicationsByProfileAndTime.$inject = ['$resource', 'DateUtils'];

    function PublicationsByProfileAndTime($resource, DateUtils) {
        var resourceUrl = 'api/publicationsbyprofilandtime/:profil_id';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });
    }



    Publication.$inject = ['$resource', 'DateUtils'];

    function Publication($resource, DateUtils) {
        var resourceUrl = 'api/publications/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creationDate = DateUtils.convertDateTimeFromServer(data.creationDate);
                    }
                    return data;
                }
            },
            'update': { method: 'PUT' }
        });
    }
})();