(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('PublicationDetailController', PublicationDetailController);

    PublicationDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Publication', 'Image', 'Profile'];

    function PublicationDetailController($scope, $rootScope, $stateParams, entity, Publication, Image, Profile) {
        var vm = this;

        vm.publication = entity;


        var unsubscribe = $rootScope.$on('mawjoudin2App:publicationUpdate', function(event, result) {
            vm.publication = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();