(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('SliderDialogController', SliderDialogController);

    SliderDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Slider', 'Image'];

    function SliderDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Slider, Image) {
        var vm = this;

        vm.slider = entity;
        vm.clear = clear;
        vm.save = save;
        vm.images = Image.query({filter: 'slider-is-null'});
        $q.all([vm.slider.$promise, vm.images.$promise]).then(function() {
            if (!vm.slider.image || !vm.slider.image.id) {
                return $q.reject();
            }
            return Image.get({id : vm.slider.image.id}).$promise;
        }).then(function(image) {
            vm.images.push(image);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.slider.id !== null) {
                Slider.update(vm.slider, onSaveSuccess, onSaveError);
            } else {
                Slider.save(vm.slider, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('mawjoudin2App:sliderUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
