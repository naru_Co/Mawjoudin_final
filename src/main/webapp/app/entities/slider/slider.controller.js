(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('SliderController', SliderController);

    SliderController.$inject = ['Slider'];

    function SliderController(Slider) {

        var vm = this;

        vm.sliders = [];

        loadAll();

        function loadAll() {
            Slider.query(function(result) {
                vm.sliders = result;
                vm.searchQuery = null;
            });
        }
    }
})();
