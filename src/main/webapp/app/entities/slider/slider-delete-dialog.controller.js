(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('SliderDeleteController',SliderDeleteController);

    SliderDeleteController.$inject = ['$uibModalInstance', 'entity', 'Slider'];

    function SliderDeleteController($uibModalInstance, entity, Slider) {
        var vm = this;

        vm.slider = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Slider.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
