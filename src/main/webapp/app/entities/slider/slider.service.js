(function() {
    'use strict';
    angular
        .module('mawjoudin2App')
        .factory('Slider', Slider);

    Slider.$inject = ['$resource'];

    function Slider ($resource) {
        var resourceUrl =  'api/sliders/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
