(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('slider', {
            parent: 'entity',
            url: '/slider',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'mawjoudin2App.slider.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/slider/sliders.html',
                    controller: 'SliderController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('slider');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('slider-detail', {
            parent: 'slider',
            url: '/slider/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'mawjoudin2App.slider.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/slider/slider-detail.html',
                    controller: 'SliderDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('slider');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Slider', function($stateParams, Slider) {
                    return Slider.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'slider',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('slider-detail.edit', {
            parent: 'slider-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/slider/slider-dialog.html',
                    controller: 'SliderDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Slider', function(Slider) {
                            return Slider.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('slider.new', {
            parent: 'slider',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/slider/slider-dialog.html',
                    controller: 'SliderDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                headlline: null,
                                simpleText: null,
                                headlineFr: null,
                                simpleTextFr: null,
                                headlineAr: null,
                                simpleTextAr: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('slider', null, { reload: 'slider' });
                }, function() {
                    $state.go('slider');
                });
            }]
        })
        .state('slider.edit', {
            parent: 'slider',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/slider/slider-dialog.html',
                    controller: 'SliderDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Slider', function(Slider) {
                            return Slider.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('slider', null, { reload: 'slider' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('slider.delete', {
            parent: 'slider',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/slider/slider-delete-dialog.html',
                    controller: 'SliderDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Slider', function(Slider) {
                            return Slider.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('slider', null, { reload: 'slider' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
