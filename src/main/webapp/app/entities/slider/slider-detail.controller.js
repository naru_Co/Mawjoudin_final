(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('SliderDetailController', SliderDetailController);

    SliderDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Slider', 'Image'];

    function SliderDetailController($scope, $rootScope, $stateParams, previousState, entity, Slider, Image) {
        var vm = this;

        vm.slider = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('mawjoudin2App:sliderUpdate', function(event, result) {
            vm.slider = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
