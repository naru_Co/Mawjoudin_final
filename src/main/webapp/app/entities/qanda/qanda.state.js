(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('qanda', {
            parent: 'entity',
            url: '/qanda',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'mawjoudin2App.qanda.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/qanda/qandas.html',
                    controller: 'QandaController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('qanda');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('qanda-detail', {
            parent: 'qanda',
            url: '/qanda/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'mawjoudin2App.qanda.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/qanda/qanda-detail.html',
                    controller: 'QandaDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('qanda');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Qanda', function($stateParams, Qanda) {
                    return Qanda.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'qanda',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('qanda-detail.edit', {
            parent: 'qanda-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/qanda/qanda-dialog.html',
                    controller: 'QandaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Qanda', function(Qanda) {
                            return Qanda.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('qanda.new', {
            parent: 'qanda',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/qanda/qanda-dialog.html',
                    controller: 'QandaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                question: null,
                                answer: null,
                                lang: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('qanda', null, { reload: 'qanda' });
                }, function() {
                    $state.go('qanda');
                });
            }]
        })
        .state('qanda.edit', {
            parent: 'qanda',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/qanda/qanda-dialog.html',
                    controller: 'QandaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Qanda', function(Qanda) {
                            return Qanda.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('qanda', null, { reload: 'qanda' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('qanda.delete', {
            parent: 'qanda',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/qanda/qanda-delete-dialog.html',
                    controller: 'QandaDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Qanda', function(Qanda) {
                            return Qanda.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('qanda', null, { reload: 'qanda' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
