(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('QandaDeleteController',QandaDeleteController);

    QandaDeleteController.$inject = ['$uibModalInstance', 'entity', 'Qanda'];

    function QandaDeleteController($uibModalInstance, entity, Qanda) {
        var vm = this;

        vm.qanda = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Qanda.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
