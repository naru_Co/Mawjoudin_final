(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('QandaController', QandaController);

    QandaController.$inject = ['DataUtils', 'Qanda'];

    function QandaController(DataUtils, Qanda) {

        var vm = this;

        vm.qandas = [];
        vm.openFile = DataUtils.openFile;
        vm.byteSize = DataUtils.byteSize;

        loadAll();

        function loadAll() {
            Qanda.query(function(result) {
                vm.qandas = result;
                vm.searchQuery = null;
            });
        }
    }
})();
