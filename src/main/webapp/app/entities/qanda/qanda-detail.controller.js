(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('QandaDetailController', QandaDetailController);

    QandaDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'Qanda'];

    function QandaDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, Qanda) {
        var vm = this;

        vm.qanda = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('mawjoudin2App:qandaUpdate', function(event, result) {
            vm.qanda = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
