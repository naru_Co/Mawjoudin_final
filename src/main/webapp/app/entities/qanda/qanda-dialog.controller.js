(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('QandaDialogController', QandaDialogController);

    QandaDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'Qanda'];

    function QandaDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, Qanda) {
        var vm = this;

        vm.qanda = entity;
        vm.clear = clear;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.qanda.id !== null) {
                Qanda.update(vm.qanda, onSaveSuccess, onSaveError);
            } else {
                Qanda.save(vm.qanda, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('mawjoudin2App:qandaUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
