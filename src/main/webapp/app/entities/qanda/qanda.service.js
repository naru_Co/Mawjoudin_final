(function() {
    'use strict';
    angular
        .module('mawjoudin2App')
        .factory('Qanda', Qanda);

    Qanda.$inject = ['$resource'];

    function Qanda ($resource) {
        var resourceUrl =  'api/qandas/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
