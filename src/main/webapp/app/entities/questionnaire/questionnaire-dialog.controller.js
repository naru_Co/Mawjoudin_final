(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('QuestionnaireDialogController', QuestionnaireDialogController);

    QuestionnaireDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'Questionnaire'];

    function QuestionnaireDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, Questionnaire) {
        var vm = this;

        vm.questionnaire = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.questionnaire.id !== null) {
                Questionnaire.update(vm.questionnaire, onSaveSuccess, onSaveError);
            } else {
                Questionnaire.save(vm.questionnaire, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('mawjoudin2App:questionnaireUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.trueDateOfIncident = false;
        vm.datePickerOpenStatus.creationDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
