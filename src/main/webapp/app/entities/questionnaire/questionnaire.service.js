(function() {
    'use strict';
    angular
        .module('mawjoudin2App')
        .factory('Questionnaire', Questionnaire);

    Questionnaire.$inject = ['$resource', 'DateUtils'];

    function Questionnaire ($resource, DateUtils) {
        var resourceUrl =  'api/questionnaires/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.trueDateOfIncident = DateUtils.convertDateTimeFromServer(data.trueDateOfIncident);
                        data.creationDate = DateUtils.convertDateTimeFromServer(data.creationDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
