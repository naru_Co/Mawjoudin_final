(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('AgendaController', AgendaController);

    AgendaController.$inject = ['Agenda'];

    function AgendaController(Agenda) {

        var vm = this;

        vm.agenda = [];

        loadAll();

        function loadAll() {
            Agenda.query(function(result) {
                vm.agenda = result;
                vm.searchQuery = null;
            });
        }
    }
})();
