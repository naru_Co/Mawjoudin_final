(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('AgendaDetailController', AgendaDetailController);

    AgendaDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Agenda', 'Profile'];

    function AgendaDetailController($scope, $rootScope, $stateParams, previousState, entity, Agenda, Profile) {
        var vm = this;

        vm.agenda = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('mawjoudin2App:agendaUpdate', function(event, result) {
            vm.agenda = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
