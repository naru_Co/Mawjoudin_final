(function() {
    'use strict';
    angular
        .module('mawjoudin2App')
        .factory('Agenda', Agenda)
        .factory('ProfileByName', ProfileByName)
        .factory('EventsByProfil', EventsByProfil)
        .factory('AgendaOfYesterdayByProfil', AgendaOfYesterdayByProfil);
    AgendaOfYesterdayByProfil.$inject = ['$resource', 'DateUtils'];

    function AgendaOfYesterdayByProfil($resource, DateUtils) {
        var resourceUrl = 'api/agendaofyesterdaybyprofile/:profil_id';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });
    }
    EventsByProfil.$inject = ['$resource', 'DateUtils'];

    function EventsByProfil($resource, DateUtils) {
        var resourceUrl = 'api//agendabyprofile/:profil_id';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });
    }

    ProfileByName.$inject = ['$resource', 'DateUtils']

    function ProfileByName($resource, DateUtils) {
        var resourceUrl = 'api/profilesbyname/:name';
        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });
    }

    Agenda.$inject = ['$resource', 'DateUtils'];

    function Agenda($resource, DateUtils) {
        var resourceUrl = 'api/agenda/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creationDate = DateUtils.convertDateTimeFromServer(data.creationDate);
                        data.startsAt = DateUtils.convertDateTimeFromServer(data.startsAt);
                        data.endsAt = DateUtils.convertDateTimeFromServer(data.endsAt);
                    }
                    return data;
                }
            },
            'update': { method: 'PUT' }
        });
    }
})();