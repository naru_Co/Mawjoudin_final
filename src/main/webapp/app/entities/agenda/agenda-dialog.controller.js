(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('AgendaDialogController', AgendaDialogController);

    AgendaDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Agenda', 'Profile'];

    function AgendaDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Agenda, Profile) {
        var vm = this;

        vm.agenda = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.profiles = Profile.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.agenda.id !== null) {
                Agenda.update(vm.agenda, onSaveSuccess, onSaveError);
            } else {
                Agenda.save(vm.agenda, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('mawjoudin2App:agendaUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.creationDate = false;
        vm.datePickerOpenStatus.startsAt = false;
        vm.datePickerOpenStatus.endsAt = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
