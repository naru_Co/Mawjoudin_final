(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('agenda', {
            parent: 'entity',
            url: '/agenda',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'mawjoudin2App.agenda.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/agenda/agenda.html',
                    controller: 'AgendaController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('agenda');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('agenda-detail', {
            parent: 'agenda',
            url: '/agenda/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'mawjoudin2App.agenda.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/agenda/agenda-detail.html',
                    controller: 'AgendaDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('agenda');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Agenda', function($stateParams, Agenda) {
                    return Agenda.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'agenda',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('agenda-detail.edit', {
            parent: 'agenda-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/agenda/agenda-dialog.html',
                    controller: 'AgendaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Agenda', function(Agenda) {
                            return Agenda.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('agenda.new', {
            parent: 'agenda',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/agenda/agenda-dialog.html',
                    controller: 'AgendaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                title: null,
                                content: null,
                                creationDate: null,
                                startsAt: null,
                                endsAt: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('agenda', null, { reload: 'agenda' });
                }, function() {
                    $state.go('agenda');
                });
            }]
        })
        .state('agenda.edit', {
            parent: 'agenda',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/agenda/agenda-dialog.html',
                    controller: 'AgendaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Agenda', function(Agenda) {
                            return Agenda.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('agenda', null, { reload: 'agenda' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('agenda.delete', {
            parent: 'agenda',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/agenda/agenda-delete-dialog.html',
                    controller: 'AgendaDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Agenda', function(Agenda) {
                            return Agenda.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('agenda', null, { reload: 'agenda' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
