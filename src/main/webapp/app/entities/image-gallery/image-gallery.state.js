(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('image-gallery', {
            parent: 'entity',
            url: '/image-gallery',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'mawjoudin2App.imageGallery.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/image-gallery/image-galleries.html',
                    controller: 'ImageGalleryController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('imageGallery');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('image-gallery-detail', {
            parent: 'image-gallery',
            url: '/image-gallery/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'mawjoudin2App.imageGallery.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/image-gallery/image-gallery-detail.html',
                    controller: 'ImageGalleryDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('imageGallery');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'ImageGallery', function($stateParams, ImageGallery) {
                    return ImageGallery.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'image-gallery',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('image-gallery-detail.edit', {
            parent: 'image-gallery-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/image-gallery/image-gallery-dialog.html',
                    controller: 'ImageGalleryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ImageGallery', function(ImageGallery) {
                            return ImageGallery.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('image-gallery.new', {
            parent: 'image-gallery',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/image-gallery/image-gallery-dialog.html',
                    controller: 'ImageGalleryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                creationDate: null,
                                description: null,
                                type: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('image-gallery', null, { reload: 'image-gallery' });
                }, function() {
                    $state.go('image-gallery');
                });
            }]
        })
        .state('image-gallery.edit', {
            parent: 'image-gallery',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/image-gallery/image-gallery-dialog.html',
                    controller: 'ImageGalleryDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['ImageGallery', function(ImageGallery) {
                            return ImageGallery.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('image-gallery', null, { reload: 'image-gallery' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('image-gallery.delete', {
            parent: 'image-gallery',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/image-gallery/image-gallery-delete-dialog.html',
                    controller: 'ImageGalleryDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['ImageGallery', function(ImageGallery) {
                            return ImageGallery.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('image-gallery', null, { reload: 'image-gallery' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
