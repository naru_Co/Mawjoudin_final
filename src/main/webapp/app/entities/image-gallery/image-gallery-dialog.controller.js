(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('ImageGalleryDialogController', ImageGalleryDialogController);

    ImageGalleryDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'ImageGallery', 'Profile', 'Image'];

    function ImageGalleryDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, ImageGallery, Profile, Image) {
        var vm = this;

        vm.imageGallery = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.profiles = Profile.query();
        vm.images = Image.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.imageGallery.id !== null) {
                ImageGallery.update(vm.imageGallery, onSaveSuccess, onSaveError);
            } else {
                ImageGallery.save(vm.imageGallery, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('mawjoudin2App:imageGalleryUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.creationDate = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
