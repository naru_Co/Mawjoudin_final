(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('ImageGalleryDetailController', ImageGalleryDetailController);

    ImageGalleryDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'ImageGallery', 'Profile', 'Image'];

    function ImageGalleryDetailController($scope, $rootScope, $stateParams, previousState, entity, ImageGallery, Profile, Image) {
        var vm = this;

        vm.imageGallery = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('mawjoudin2App:imageGalleryUpdate', function(event, result) {
            vm.imageGallery = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
