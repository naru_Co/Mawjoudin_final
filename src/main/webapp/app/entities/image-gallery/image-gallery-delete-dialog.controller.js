(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('ImageGalleryDeleteController',ImageGalleryDeleteController);

    ImageGalleryDeleteController.$inject = ['$uibModalInstance', 'entity', 'ImageGallery'];

    function ImageGalleryDeleteController($uibModalInstance, entity, ImageGallery) {
        var vm = this;

        vm.imageGallery = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            ImageGallery.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
