(function() {
    'use strict';
    angular
        .module('mawjoudin2App')
        .factory('ImageGallery', ImageGallery)
        .factory('GalleryByProfileAndType', GalleryByProfileAndType)

    .factory('ImageByGallery', ImageByGallery);

    ImageByGallery.$inject = ['$resource', 'DateUtils'];

    function ImageByGallery($resource, DateUtils) {
        var resourceUrl = 'api/imagesbyGallery/:imageGallery_id';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });
    }

    GalleryByProfileAndType.$inject = ['$resource', 'DateUtils'];

    function GalleryByProfileAndType($resource, DateUtils) {
        var resourceUrl = 'api/gallerybyprofileandtype/:profil_id/:type/';

        return $resource(resourceUrl, {}, {
            'get': {
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.birthdate = DateUtils.convertLocalDateFromServer(data.birthdate);
                    }
                    return data;
                }
            }
        });
    }

    ImageGallery.$inject = ['$resource', 'DateUtils'];

    function ImageGallery($resource, DateUtils) {
        var resourceUrl = 'api/image-galleries/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creationDate = DateUtils.convertDateTimeFromServer(data.creationDate);
                    }
                    return data;
                }
            },
            'update': { method: 'PUT' }
        });
    }
})();