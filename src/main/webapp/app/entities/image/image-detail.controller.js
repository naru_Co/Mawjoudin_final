(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('ImageDetailController', ImageDetailController);

    ImageDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Image', 'NewsLetter', 'ImageGallery'];

    function ImageDetailController($scope, $rootScope, $stateParams, previousState, entity, Image, NewsLetter, ImageGallery) {
        var vm = this;

        vm.image = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('mawjoudin2App:imageUpdate', function(event, result) {
            vm.image = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
