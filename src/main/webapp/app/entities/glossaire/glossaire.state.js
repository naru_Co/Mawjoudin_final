(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('glossaire', {
            parent: 'entity',
            url: '/glossaire',
            data: {
                pageTitle: 'mawjoudin2App.glossaire.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/glossaire/glossaires.html',
                    controller: 'GlossaireController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('glossaire');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('glossaire-detail', {
            parent: 'glossaire',
            url: '/glossaire/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'mawjoudin2App.glossaire.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/glossaire/glossaire-detail.html',
                    controller: 'GlossaireDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('glossaire');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Glossaire', function($stateParams, Glossaire) {
                    return Glossaire.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'glossaire',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('glossaire-detail.edit', {
            parent: 'glossaire-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/glossaire/glossaire-dialog.html',
                    controller: 'GlossaireDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Glossaire', function(Glossaire) {
                            return Glossaire.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('glossaire.new', {
            parent: 'glossaire',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/glossaire/glossaire-dialog.html',
                    controller: 'GlossaireDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                title: null,
                                description: null,
                                lang: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('glossaire', null, { reload: 'glossaire' });
                }, function() {
                    $state.go('glossaire');
                });
            }]
        })
        .state('glossaire.edit', {
            parent: 'glossaire',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/glossaire/glossaire-dialog.html',
                    controller: 'GlossaireDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Glossaire', function(Glossaire) {
                            return Glossaire.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('glossaire', null, { reload: 'glossaire' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('glossaire.delete', {
            parent: 'glossaire',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/glossaire/glossaire-delete-dialog.html',
                    controller: 'GlossaireDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Glossaire', function(Glossaire) {
                            return Glossaire.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('glossaire', null, { reload: 'glossaire' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
