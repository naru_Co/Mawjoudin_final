(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('GlossaireDetailController', GlossaireDetailController);

    GlossaireDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Glossaire'];

    function GlossaireDetailController($scope, $rootScope, $stateParams, previousState, entity, Glossaire) {
        var vm = this;

        vm.glossaire = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('mawjoudin2App:glossaireUpdate', function(event, result) {
            vm.glossaire = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
