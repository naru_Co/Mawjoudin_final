(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('GlossaireController', GlossaireController);

    GlossaireController.$inject = ['Glossaire','$scope'];

    function GlossaireController(Glossaire,$scope) {

        var vm = this;

        vm.glossaires = [];

        loadAll();
        $scope.allGlossaire = Glossaire.query()

        function loadAll() {
            Glossaire.query(function(result) {
                vm.glossaires = result;
                vm.searchQuery = null;
            });
        }
    }
})();
