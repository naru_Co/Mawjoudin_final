(function() {
    'use strict';
    angular
        .module('mawjoudin2App')
        .factory('Glossaire', Glossaire);

    Glossaire.$inject = ['$resource'];

    function Glossaire ($resource) {
        var resourceUrl =  'api/glossaires/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
