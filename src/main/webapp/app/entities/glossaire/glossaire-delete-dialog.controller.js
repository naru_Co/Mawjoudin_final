(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('GlossaireDeleteController',GlossaireDeleteController);

    GlossaireDeleteController.$inject = ['$uibModalInstance', 'entity', 'Glossaire'];

    function GlossaireDeleteController($uibModalInstance, entity, Glossaire) {
        var vm = this;

        vm.glossaire = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Glossaire.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
