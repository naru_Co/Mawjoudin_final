(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('GlossaireDialogController', GlossaireDialogController);

    GlossaireDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Glossaire'];

    function GlossaireDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Glossaire) {
        var vm = this;

        vm.glossaire = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.glossaire.id !== null) {
                Glossaire.update(vm.glossaire, onSaveSuccess, onSaveError);
            } else {
                Glossaire.save(vm.glossaire, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('mawjoudin2App:glossaireUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
