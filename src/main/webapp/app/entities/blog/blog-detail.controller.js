(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('BlogDetailController', BlogDetailController);

    BlogDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Blog', 'Principal', 'Image', 'Profile'];

    function BlogDetailController($scope, $rootScope, $stateParams, previousState, entity, Blog, Principal, Image, Profile) {
        var vm = this;

        vm.blog = entity;
        vm.previousState = previousState.name;
        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;

            });
        }
        getAccount();
       
        $scope.arab = false;
        $scope.isArabic = function(text) {
            var pattern = /[\u0600-\u06FF\u0750-\u077F]/;
            $scope.arab = pattern.test(text);
            


            return $scope.arab;

        }

        var unsubscribe = $rootScope.$on('mawjoudin2App:blogUpdate', function(event, result) {
            vm.blog = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();