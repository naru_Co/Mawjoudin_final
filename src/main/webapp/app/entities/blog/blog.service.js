(function() {
    'use strict';
    angular
        .module('mawjoudin2App')
        .factory('Blog', Blog)
        .factory('BlogByState', BlogByState)
        .factory('BlogByProfileAndStateAndTime', BlogByProfileAndStateAndTime)
        .factory('BlogByProfileAndState', BlogByProfileAndState);
    BlogByState.$inject = ['$resource', 'DateUtils'];

    function BlogByState($resource, DateUtils) {
        var resourceUrl = 'api/blogsbyState/:state';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });
    }
    BlogByProfileAndStateAndTime.$inject = ['$resource', 'DateUtils'];

    function BlogByProfileAndStateAndTime($resource, DateUtils) {
        var resourceUrl = 'api/blogsbyProfilAndStateAndTime/:profil_id';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });
    }

    BlogByProfileAndState.$inject = ['$resource', 'DateUtils'];

    function BlogByProfileAndState($resource, DateUtils) {
        var resourceUrl = 'api/blogsbyProfilAndState/:profil_id/:state/';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });
    }


    Blog.$inject = ['$resource', 'DateUtils'];

    function Blog($resource, DateUtils) {
        var resourceUrl = 'api/blogs/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creationDate = DateUtils.convertDateTimeFromServer(data.creationDate);
                    }
                    return data;
                }
            },
            'update': { method: 'PUT' }
        });
    }
})();