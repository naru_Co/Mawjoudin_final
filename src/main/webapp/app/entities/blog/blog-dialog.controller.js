(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('BlogDialogController', BlogDialogController);

    BlogDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Blog', 'Image', 'Profile','Principal'];

    function BlogDialogController($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Blog, Image, Profile,Principal) {
        var vm = this;

        vm.blog = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
       
        vm.save = save;
      
       
        $timeout(function() {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
                Blog.update(vm.blog, onSaveSuccess, onSaveError);
           
        }

        function onSaveSuccess(result) {
            $scope.$emit('mawjoudin2App:blogUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

      
    }
})();