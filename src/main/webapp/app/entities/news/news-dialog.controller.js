(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('NewsDialogController', NewsDialogController);

    NewsDialogController.$inject = ['DataUtils', '$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'News', 'Image', 'Profile'];

    function NewsDialogController(DataUtils, $timeout, $scope, $stateParams, $uibModalInstance, $q, entity, News, Image, Profile) {
        var vm = this;

        vm.news = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;


        function uploadEditorImage(image) {
            DataUtils.toBase64(image[0], function(base64Data) {
                $scope.pureImage.file = base64Data;
                $scope.iscompleted = true
                Image.save($scope.pureImage, function(result) {
                    var file_path = result.normalRes;
                    $scope.iscompleted = false
                    $scope.editor.summernote('insertImage', file_path, 'mawjoudin.jpg');
                }, function() {
                    $scope.iscompleted = false
                });


            });

        }

        vm.images = Image.query({ filter: 'news-is-null' });
        $q.all([vm.news.$promise, vm.images.$promise]).then(function() {
            if (!vm.news.image || !vm.news.image.id) {
                return $q.reject();
            }
            return Image.get({ id: vm.news.image.id }).$promise;
        }).then(function(image) {
            vm.images.push(image);
        });

        $timeout(function() {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.news.id !== null) {
                News.update(vm.news, onSaveSuccess, onSaveError);
            } else {
                News.save(vm.news, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('mawjoudin2App:newsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.creationDate = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();