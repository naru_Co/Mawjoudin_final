(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('NewsDetailController', NewsDetailController);

    NewsDetailController.$inject = ['$sce', '$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'News', 'Image', 'Profile'];

    function NewsDetailController($sce, $scope, $rootScope, $stateParams, previousState, entity, News, Image, Profile) {
        var vm = this;

        vm.news = entity;
        vm.trusted = $sce.trustAsHtml(vm.news.description);

        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('mawjoudin2App:newsUpdate', function(event, result) {
            vm.news = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();