(function() {
    'use strict';
    angular
        .module('mawjoudin2App')
        .factory('ProjectOfYesterday', ProjectOfYesterday)

    .factory('NewsOfyesterday', NewsOfyesterday)
        .factory('News', News);
    NewsOfyesterday.$inject = ['$resource', 'DateUtils'];

    function NewsOfyesterday($resource, DateUtils) {
        var resourceUrl = 'api/newsByTime';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });
    }
    ProjectOfYesterday.$inject = ['$resource', 'DateUtils'];

    function ProjectOfYesterday($resource, DateUtils) {
        var resourceUrl = 'api/projectsofyesterday';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });
    }

    News.$inject = ['$resource', 'DateUtils'];

    function News($resource, DateUtils) {
        var resourceUrl = 'api/news/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creationDate = DateUtils.convertDateTimeFromServer(data.creationDate);
                    }
                    return data;
                }
            },
            'update': { method: 'PUT' }
        });
    }
})();