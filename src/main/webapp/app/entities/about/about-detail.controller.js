(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('AboutDetailController', AboutDetailController);

    AboutDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'About', 'Image'];

    function AboutDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, About, Image) {
        var vm = this;

        vm.about = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('mawjoudin2App:aboutUpdate', function(event, result) {
            vm.about = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
