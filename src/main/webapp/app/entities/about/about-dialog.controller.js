(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('AboutDialogController', AboutDialogController);

    AboutDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'DataUtils', 'entity', 'About', 'Image'];

    function AboutDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, DataUtils, entity, About, Image) {
        var vm = this;

        vm.about = entity;
        vm.clear = clear;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;
        vm.images = Image.query({filter: 'about-is-null'});
        $q.all([vm.about.$promise, vm.images.$promise]).then(function() {
            if (!vm.about.image || !vm.about.image.id) {
                return $q.reject();
            }
            return Image.get({id : vm.about.image.id}).$promise;
        }).then(function(image) {
            vm.images.push(image);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.about.id !== null) {
                About.update(vm.about, onSaveSuccess, onSaveError);
            } else {
                About.save(vm.about, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('mawjoudin2App:aboutUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
