(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('AboutController', AboutController);

    AboutController.$inject = ['DataUtils', 'About'];

    function AboutController(DataUtils, About) {

        var vm = this;

        vm.abouts = [];
        vm.openFile = DataUtils.openFile;
        vm.byteSize = DataUtils.byteSize;

        loadAll();

        function loadAll() {
            About.query(function(result) {
                vm.abouts = result;
                vm.searchQuery = null;
            });
        }
    }
})();
