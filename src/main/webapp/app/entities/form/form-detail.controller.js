(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('FormDetailController', FormDetailController);

    FormDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'Form'];

    function FormDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, Form) {
        var vm = this;

        vm.form = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('mawjoudin2App:formUpdate', function(event, result) {
            vm.form = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
