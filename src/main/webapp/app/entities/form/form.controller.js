(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('FormController', FormController);

    FormController.$inject = ['DataUtils', 'Form'];

    function FormController(DataUtils, Form) {

        var vm = this;

        vm.forms = [];
        vm.openFile = DataUtils.openFile;
        vm.byteSize = DataUtils.byteSize;

        loadAll();

        function loadAll() {
            Form.query(function(result) {
                vm.forms = result;
                vm.searchQuery = null;
            });
        }
    }
})();