(function() {
    'use strict';
    angular
        .module('mawjoudin2App')
        .factory('Form', Form);

    Form.$inject = ['$resource', 'DateUtils'];

    function Form ($resource, DateUtils) {
        var resourceUrl =  'api/forms/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creationDate = DateUtils.convertDateTimeFromServer(data.creationDate);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
