(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('NewQuestionnaryController', NewQuestionnaryController)
    NewQuestionnaryController.$inject = ['Questionnaire', '$mdDialog', '$scope'];

    function NewQuestionnaryController(Questionnaire, $mdDialog, $scope) {

        var vm = this;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.form = {};
        vm.save = save;
        $scope.bsexes = [{ id: 1, name: 'Male' }, { id: 2, name: 'Female' }, { id: 3, name: 'Intersex' }];
        $scope.gidentities = [{ id: 1, name: 'Cisman' }, { id: 2, name: 'Ciswoman' }, { id: 3, name: 'Transman' }, { id: 4, name: 'Transwoman' }, { id: 5, name: 'Trans*' }, { id: 6, name: 'Non-binary' }, { id: 7, name: 'Gender queer' }, { id: 8, name: 'Other' }];
        $scope.sexualOrientations = [{ id: 1, name: 'Gay' }, { id: 2, name: 'Lesbian' }, { id: 3, name: 'Bisexual' }, { id: 4, name: 'Pansexual' }, { id: 5, name: 'Polysexual' }, { id: 6, name: 'Asexual' }, { id: 7, name: 'Queer' }, { id: 8, name: 'Heterosexual' }, { id: 9, name: 'Not sure' }, { id: 10, name: 'Other' }];
        $scope.liveIns = [{ id: 1, name: 'City' }, { id: 2, name: 'Countryside' }];
        $scope.lgbCons = [{ id: 1, name: 'Very Involved' }, { id: 2, name: 'Participate at some LGBTQI+ events' }, { id: 3, name: 'member of an LGBTQI+ organisation' }, { id: 4, name: 'LGBTQI+ Activist' }, { id: 5, name: 'Not involved' }, { id: 6, name: 'Other' }];
        $scope.extents = [{ id: 1, name: 'Out to all my family' }, { id: 2, name: 'out to my brother/sister' }, { id: 3, name: 'out to my mother' }, { id: 4, name: 'out to my father' }, { id: 5, name: 'out to my friends' }, { id: 6, name: 'out in my workplace publicly known' }, { id: 7, name: 'out to everyone' }, { id: 7, name: 'out in the Media' }, { id: 8, name: 'Other' }];
        $scope.places = [{ id: 1, name: 'Home' }, { id: 2, name: 'Near Home' }, { id: 3, name: 'School' }, { id: 4, name: 'Work' }, { id: 5, name: 'LGBTQI+ organisation buildings' }, { id: 6, name: 'police station' }, { id: 7, name: 'police station' }, { id: 8, name: 'cruising area' }, { id: 9, name: 'private space' }, { id: 10, name: 'public space' }, { id: 11, name: 'state institution' }, { id: 12, name: 'Other' }]
        $scope.verbalInc = [{ id: 1, name: 'insults' }, { id: 2, name: 'threat of violence' }, { id: 3, name: 'Threats' }, { id: 4, name: 'Screaming at you' }, { id: 5, name: 'Calling names' }, { id: 6, name: 'Humiliating and criticizing your sexuality and your gender identity and expression and your body' }, { id: 7, name: 'Threats to beat you, kill you, or destroy you mentally' }, { id: 8, name: 'Threatening to harm the person or her or his family if she or he leaves' }, { id: 9, name: 'Threatening to harm oneself if you don\'t cooperate' }, { id: 10, name: 'Threats of violence' }, { id: 11, name: 'Threats of abandonment' }, { id: 12, name: 'Threatening to have the person reported to the authorities' }, { id: 13, name: 'Threatening to “out” you' }, { id: 14, name: 'Accusations' }, { id: 15, name: 'Claiming that you lie' }]

        $scope.physicalInc = [{ id: 16, name: 'Pressing you against the wall' }, { id: 17, name: 'Forcing you to do things that you find humiliating' }, { id: 18, name: 'Hitting with hand' }, { id: 19, name: 'Hitting with objects' }, { id: 20, name: 'Kicking' }, { id: 21, name: 'Pulling hair' }, { id: 22, name: 'Throwing objects at you' }, { id: 23, name: 'Choking' }, { id: 24, name: 'Pushing' }, { id: 25, name: 'Spitting' }, { id: 26, name: 'Twisting your arms' }, { id: 27, name: 'Chasing' }, { id: 28, name: 'Threatening with weapons' }, { id: 29, name: 'Kicking in doors and boxing in the wall' }, { id: 30, name: 'Deliberate exposure to severe weather or inappropriate room temperatures' }, { id: 31, name: 'Pinching' }, { id: 32, name: 'strangling' }, { id: 33, name: 'burning' }, { id: 34, name: 'stabbing' }, { id: 35, name: 'slapping' }, { id: 36, name: 'biting' }, { id: 37, name: 'force-feeding' }, { id: 38, name: 'Unwarranted use of medication to control a person' }]

        $scope.sexualInc = [{ id: 39, name: 'sexually assaulted' }, { id: 40, name: 'Sexual harassment' }, { id: 41, name: 'Sexual exploitation' }, { id: 42, name: 'Touching your body when you do not want to' }, { id: 43, name: 'Insisting on taking your clothes off when you do not want to' }, { id: 44, name: 'Touching your body in a way that you do not want to' }, { id: 45, name: 'Forcing you to sexual activity that you do not want (for example, oral sex, anal intercourse, watching pornographic movies and magazines, imitating pornographic movies)' }, { id: 46, name: 'Forcing you into sex' }, { id: 47, name: 'Being raped' }, { id: 48, name: 'Rape-marriage' }, { id: 49, name: 'Sexual slavery' }, { id: 50, name: 'Touching in a sexual manner without consent' }]

        $scope.psycoInc = [{ id: 51, name: 'Intimidation' }, { id: 52, name: 'Monitoring' }, { id: 53, name: 'Outing' }, { id: 54, name: 'Blackmail' }, { id: 55, name: 'Isolation' }, { id: 56, name: 'Bullying' }, { id: 57, name: 'Keeping you from seeing your friends' }, { id: 58, name: 'Not allowing you to leave the house' }, { id: 59, name: 'Locking you in the house' }, { id: 60, name: 'Criticism and ridicule of your feelings, opinions, beliefs' }, { id: 61, name: 'Disregarding and neglecting your opinion' }, { id: 62, name: 'Deliberately causing guilt feelings' }, { id: 63, name: 'Persuading you that you are mad or that there is something wrong with you' }, { id: 64, name: 'Inducing feelings of fear' }, { id: 65, name: 'Justifying violent behaviour as if it was committed by accident' }, { id: 66, name: 'Justifying violent behaviour by having been drunk, under stress..' }, { id: 67, name: 'Persuading you it’s your fault' }, { id: 68, name: 'Persuading you that he/she/they are violent because he/she/they love(s) you' }, { id: 69, name: 'Persuading you that he/she/they will improve his/her/their behaviour' }, { id: 70, name: 'Denying having committed violence (sexual, psychological, physical ...) against you or anyone else' }, { id: 71, name: 'Severely criticizing your past relationships' }, { id: 72, name: 'Socially isolating the person' }, { id: 73, name: 'Not allowing access to a telephone' }, { id: 74, name: 'Not allowing a competent person to make decisions' }, { id: 75, name: 'Inappropriately controlling the person’s activities' }, { id: 76, name: 'Requirements that you dress differently (gender conforming, more masculine, more feminine, more sexually or less sexual)' }, { id: 77, name: 'Manipulating with lies' }, { id: 78, name: 'Banishment' }, { id: 79, name: 'Using one’s spiritual or religious position, rituals or practices to manipulate, dominate or control a person' }]

        $scope.ecoInc = [{ id: 80, name: 'Limiting the access to family money' }, { id: 81, name: 'Destroying or damaging your personal belongings and your property' }, { id: 82, name: 'Being fired' }];
        $scope.preIdentity = [{ id: 1, name: 'public officials' }, { id: 2, name: 'police' }, { id: 3, name: 'private individuals known to you' }, { id: 4, name: 'private individuals unknown to you' }];
        $scope.linkInc = [{ id: 1, name: 'sexual orientation' }, { id: 2, name: 'sexual practice' }, { id: 3, name: 'sexuality' }, { id: 4, name: 'gender identity' }, { id: 5, name: 'gender expression' }, { id: 6, name: 'sex characteristics' }, { id: 7, name: 'Body parts, anatomical properties' }, { id: 8, name: 'Color' }, { id: 9, name: 'Region' }, { id: 10, name: 'Social ranking' }, { id: 11, name: 'race/ethnicity' }, { id: 12, name: 'religion' }, { id: 13, name: 'cultural heritage or background' }, { id: 14, name: 'lifestyle' }, { id: 15, name: 'political statement or opinion (activism)' }, { id: 16, name: 'age' }, { id: 17, name: 'disability' }, { id: 18, name: 'nationality' }];

        $scope.LinkIncWhy = [{ id: 1, name: 'language and words used' }, { id: 2, name: 'place where the incident took place' }, { id: 3, name: 'the victim’s sexual orientation, gender identity and expression and sex characteristics are publicly known' }, { id: 4, name: 'previous threats made or other incident' }]
        $scope.affect = [{ id: 1, name: 'on mental health' }, { id: 2, name: 'on physical health' }, { id: 3, name: 'on personal and social life' }, { id: 4, name: 'on living' }, { id: 5, name: 'on job conditions ' }]
        $scope.support = [{ id: 1, name: 'psychologist' }, { id: 2, name: 'victim support group' }, { id: 3, name: 'friends' }, { id: 4, name: 'family' }];
        $scope.reportTo = [{ id: 1, name: 'LGBTQI+ NGOs' }, { id: 2, name: 'Human rights NGOs' }, { id: 3, name: 'Administrative complaint procedure' }];
        $scope.yorno = [{ id: 1, name: 'Yes' }, { id: 2, name: 'No' }]
        $scope.arMotif = [{ id: 1, name: 'Article 230' }, { id: 2, name: 'Article 226' }, { id: 3, name: 'Article 226 Bis' }, { id: 4, name: 'other' }]





















        $scope.items = [1, 2, 3, 4, 5];
        $scope.selected = [];
        $scope.selectedVerbalInc = [];
        vm.questionnaire = { typeOfIncident: '' };

        $scope.toggle = function(item, list) {
            var idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            } else {
                list.push(item);
            }
        };

        $scope.exists = function(item, list) {
            return list.indexOf(item) > -1;
        };



        vm.datePickerOpenStatus.trueDateOfIncident = false;
        vm.datePickerOpenStatus.creationDate = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }
        $scope.showAlert = function() {
            // Appending dialog to document.body to cover sidenav in docs app
            // Modal dialogs should fully cover application
            // to prevent interaction outside of dialog
            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title("Votre message vient d'etre envoyé")
                .textContent('Il sera traité dans les plus brefs délais')
                .ariaLabel('Alert Dialog')
                .ok('Ok')
            );
        };

        function save() {
            vm.isSaving = true;
            vm.questionnaire.typeOfIncident = JSON.stringify($scope.selectedVerbalInc);
            Questionnaire.save(vm.questionnaire, onSaveSuccess, onSaveError);

        }

        function onSaveSuccess(result) {
            vm.questionnaire = {};
            $scope.showAlert()
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }


    }
})();