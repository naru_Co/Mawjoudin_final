(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('NewFormController', NewFormController)
    NewFormController.$inject = ['Form', '$mdDialog', '$scope'];

    function NewFormController(Form, $mdDialog, $scope) {

        var vm = this;
        vm.form = {};
        vm.save = save;

        $scope.showAlert = function() {
            // Appending dialog to document.body to cover sidenav in docs app
            // Modal dialogs should fully cover application
            // to prevent interaction outside of dialog
            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title("Votre message vient d'etre envoyé")
                .textContent('Il sera traité dans les plus brefs délais')
                .ariaLabel('Alert Dialog')
                .ok('Ok')
            );
        };

        function save() {
            vm.isSaving = true;

            Form.save(vm.form, onSaveSuccess, onSaveError);

        }

        function onSaveSuccess(result) {
            vm.form = {};
            $scope.showAlert()
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }


    }
})();