(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('FriendDetailController', FriendDetailController);

    FriendDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Friend', 'Profile'];

    function FriendDetailController($scope, $rootScope, $stateParams, previousState, entity, Friend, Profile) {
        var vm = this;

        vm.friend = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('mawjoudin2App:friendUpdate', function(event, result) {
            vm.friend = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();