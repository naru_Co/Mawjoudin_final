(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .factory('FriendSearch', FriendSearch);

    FriendSearch.$inject = ['$resource'];

    function FriendSearch($resource) {
        var resourceUrl = 'api/_search/friends/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true }
        });
    }
})();