(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('FriendDialogController', FriendDialogController);

    FriendDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Friend', 'Profile'];

    function FriendDialogController($timeout, $scope, $stateParams, $uibModalInstance, entity, Friend, Profile) {
        var vm = this;

        vm.friend = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.profiles = Profile.query();

        $timeout(function() {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.friend.id !== null) {
                Friend.update(vm.friend, onSaveSuccess, onSaveError);
            } else {
                Friend.save(vm.friend, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('mawjoudin2App:friendUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.creationDate = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();