(function () {
    'use strict';
    angular
        .module('mawjoudin2App')
        .factory('Friend', Friend)
        .factory('FriendsByProfile', FriendsByProfile)
        .factory('Friendship', Friendship)
        .factory('Invitations', Invitations)
        .factory('RequestsBySender', RequestsBySender);




    RequestsBySender.$inject = ['$resource', 'DateUtils'];

    function RequestsBySender($resource, DateUtils) {
        var resourceUrl = 'api/invitationsbysenderAndState/:sender_id/:state';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });
    }




    Invitations.$inject = ['$resource', 'DateUtils'];

    function Invitations($resource, DateUtils) {
        var resourceUrl = 'api/invitationsbyreceiverAndState/:receiver_id/:state';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });
    }

    Friendship.$inject = ['$resource', 'DateUtils'];

    function Friendship($resource, DateUtils) {
        var resourceUrl = 'api/friendshipBetweenTwo/:receiver_id/:sender_id/';

        return $resource(resourceUrl, {}, {
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.birthdate = DateUtils.convertLocalDateFromServer(data.birthdate);
                    }
                    return data;
                }
            }
        });
    }





    FriendsByProfile.$inject = ['$resource', 'DateUtils'];

    function FriendsByProfile($resource, DateUtils) {
        var resourceUrl = 'api/friendsbyreceiverAndState/:receiver_id/:state';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });
    }


    Friend.$inject = ['$resource', 'DateUtils'];

    function Friend($resource, DateUtils) {
        var resourceUrl = 'api/friends/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creationDate = DateUtils.convertDateTimeFromServer(data.creationDate);
                    }
                    return data;
                }
            },
            'update': { method: 'PUT' }
        });
    }
})();