(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('PartnerDialogController', PartnerDialogController);

    PartnerDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Partner', 'Image'];

    function PartnerDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Partner, Image) {
        var vm = this;

        vm.partner = entity;
        vm.clear = clear;
        vm.save = save;
        vm.images = Image.query({filter: 'partner-is-null'});
        $q.all([vm.partner.$promise, vm.images.$promise]).then(function() {
            if (!vm.partner.image || !vm.partner.image.id) {
                return $q.reject();
            }
            return Image.get({id : vm.partner.image.id}).$promise;
        }).then(function(image) {
            vm.images.push(image);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.partner.id !== null) {
                Partner.update(vm.partner, onSaveSuccess, onSaveError);
            } else {
                Partner.save(vm.partner, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('mawjoudin2App:partnerUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
