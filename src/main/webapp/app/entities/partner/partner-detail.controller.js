(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('PartnerDetailController', PartnerDetailController);

    PartnerDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Partner', 'Image'];

    function PartnerDetailController($scope, $rootScope, $stateParams, previousState, entity, Partner, Image) {
        var vm = this;

        vm.partner = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('mawjoudin2App:partnerUpdate', function(event, result) {
            vm.partner = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
