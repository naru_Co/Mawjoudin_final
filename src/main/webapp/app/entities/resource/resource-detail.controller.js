(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('ResourceDetailController', ResourceDetailController);

    ResourceDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Resource'];

    function ResourceDetailController($scope, $rootScope, $stateParams, previousState, entity, Resource) {
        var vm = this;

        vm.resource = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('mawjoudin2App:resourceUpdate', function(event, result) {
            vm.resource = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
