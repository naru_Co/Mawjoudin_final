(function() {
    'use strict';
    angular
        .module('mawjoudin2App')
        .factory('Resource', Resource)
        .factory('ResourceByDesc', ResourceByDesc)



    Resource.$inject = ['$resource']
    ResourceByDesc.$inject = ['$resource', 'DateUtils']

    function ResourceByDesc($resource, DateUtils) {
        var resourceUrl = 'api/resourcesbydesc/:desc';
        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });
    }

    function Resource($resource) {
        var resourceUrl = 'api/resources/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method: 'PUT' }
        });
    }
})();