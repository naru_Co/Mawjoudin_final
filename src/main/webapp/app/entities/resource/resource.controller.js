(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('ResourceController', ResourceController);

    ResourceController.$inject = ['$state', 'ResourceByDesc', '$scope', 'Resource', 'ParseLinks', 'AlertService', 'paginationConstants', 'pagingParams'];

    function ResourceController($state, ResourceByDesc, $scope, Resource, ParseLinks, AlertService, paginationConstants, pagingParams) {

        var vm = this;

        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.resources=[];
        $scope.filePDF = [];
        $scope.fileDoc = [];
        $scope.IsPdf = false;
        $scope.Isdoc = false;
        $scope.searchRes = [];

        loadAll();

        $scope.querySearch = function(query) {
            $scope.title = query;
            return ResourceByDesc.get({
                desc: query.toLowerCase()
            }, function(res) {
                $scope.searchRes = res;
            }).$promise
        }

        function loadAll() {
            Resource.query({
                page: pagingParams.page - 1,
                size: vm.itemsPerPage,
                sort: sort()
            }, onSuccess, onError);

            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.page = pagingParams.page;
                for (var i = 0; i < data.length; i++) {
                    $scope.type = data[i].filePath.split('.')[1];
                    $scope.name=data[i].filePath.split('.')[0]
                    if ($scope.type === "pdf") {
                        data[i].downloadName= $scope.name.split('sources/')[1]+"."+$scope.type
                        data[i].isdoc=false
                        data[i].IsPdf = true;
                    } else if ($scope.type != "pdf") {
                        
                        data[i].downloadName= $scope.name.split('sources/')[1]+"."+$scope.type
                        data[i].isdoc=true
                        data[i].IsPdf = false;
                      
                    }
                    console.log(data[i])
                    vm.resources.push(data[i])

                }
            }

            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadPage(page) {
            vm.page = page;
            vm.transition();
        }

        function transition() {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }
    }
})();