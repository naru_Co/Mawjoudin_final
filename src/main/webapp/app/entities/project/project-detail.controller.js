(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('ProjectDetailController', ProjectDetailController);

    ProjectDetailController.$inject = ['$sce', '$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'Project', 'Image', 'Profile'];

    function ProjectDetailController($sce, $scope, $rootScope, $stateParams, previousState, DataUtils, entity, Project, Image, Profile) {
        var vm = this;

        vm.project = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.trusted = $sce.trustAsHtml(vm.project.description);


        var unsubscribe = $rootScope.$on('mawjoudin2App:projectUpdate', function(event, result) {
            vm.project = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();