(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('ProjectDialogController', ProjectDialogController);

    ProjectDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'DataUtils', 'entity', 'Project', 'Image', 'Profile'];

    function ProjectDialogController($timeout, $scope, $stateParams, $uibModalInstance, $q, DataUtils, entity, Project, Image, Profile) {
        var vm = this;

        vm.project = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;
        $scope.options = {
            focus: true,
            toolbar: [
                ['edit', ['undo', 'redo']],
                ['headline', ['style']],
                ['style', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
                ['fontface', ['fontname']],
                ['textsize', ['fontsize']],
                ['fontclr', ['color']],
                ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video', 'hr']],
            ],
        };
        $scope.imageUpload = function(files) {
            uploadEditorImage(files);
        };
        $scope.pureImage = {}

        function uploadEditorImage(image) {

            DataUtils.toBase64(image[0], function(base64Data) {
                $scope.pureImage.file = base64Data;
                $scope.iscompleted = true
                Image.save($scope.pureImage, function(result) {
                    var file_path = result.normalRes;
                    $scope.iscompleted = false
                    $scope.editor.summernote('insertImage', file_path, 'mawjoudin.jpg');
                }, function() {
                    $scope.iscompleted = false
                });


            });

        }

        vm.images = Image.query({ filter: 'project-is-null' });
        $q.all([vm.project.$promise, vm.images.$promise]).then(function() {
            if (!vm.project.image || !vm.project.image.id) {
                return $q.reject();
            }
            return Image.get({ id: vm.project.image.id }).$promise;
        }).then(function(image) {
            vm.images.push(image);
        });
        vm.profiles = Profile.query();

        $timeout(function() {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.project.id !== null) {
                Project.update(vm.project, onSaveSuccess, onSaveError);
            } else {
                Project.save(vm.project, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('mawjoudin2App:projectUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.creationDate = false;
        vm.datePickerOpenStatus.projectDate = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();