(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .factory('NewsLetterSearch', NewsLetterSearch);

    NewsLetterSearch.$inject = ['$resource'];

    function NewsLetterSearch($resource) {
        var resourceUrl = 'api/_search/news-letters/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true }
        });
    }
})();