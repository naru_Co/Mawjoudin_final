(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('NewsLetterDetailController', NewsLetterDetailController);

    NewsLetterDetailController.$inject = ['$sce', '$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'NewsLetter', 'Image'];

    function NewsLetterDetailController($sce, $scope, $rootScope, $stateParams, previousState, DataUtils, entity, NewsLetter, Image) {
        var vm = this;

        vm.newsLetter = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.trusted = $sce.trustAsHtml(vm.newsLetter.content);


        var unsubscribe = $rootScope.$on('mawjoudin2App:newsLetterUpdate', function(event, result) {
            vm.newsLetter = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();