(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('NewsLetterController', NewsLetterController);

    NewsLetterController.$inject = ['DataUtils', 'NewsLetter', 'NewsLetterSearch', 'SendingNewsLetter', '$scope'];

    function NewsLetterController(DataUtils, NewsLetter, NewsLetterSearch, SendingNewsLetter, $scope) {

        var vm = this;

        vm.newsLetters = [];
        vm.openFile = DataUtils.openFile;
        vm.byteSize = DataUtils.byteSize;
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            NewsLetter.query(function(result) {
                vm.newsLetters = result;
                vm.searchQuery = null;
            });
        }

        $scope.send = function(newletter) {
            
            SendingNewsLetter.send({ id: newletter.id }, function(result) {
                
            })
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            NewsLetterSearch.query({ query: vm.searchQuery }, function(result) {
                vm.newsLetters = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }
    }
})();