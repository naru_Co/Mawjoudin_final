(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('NewsLetterDialogController', NewsLetterDialogController);

    NewsLetterDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'NewsLetter', 'Image'];

    function NewsLetterDialogController($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, NewsLetter, Image) {
        var vm = this;

        vm.newsLetter = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;
        vm.images = Image.query();

        $timeout(function() {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.newsLetter.id !== null) {
                NewsLetter.update(vm.newsLetter, onSaveSuccess, onSaveError);
            } else {
                NewsLetter.save(vm.newsLetter, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('mawjoudin2App:newsLetterUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.creationDate = false;
        vm.datePickerOpenStatus.sendingDate = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();