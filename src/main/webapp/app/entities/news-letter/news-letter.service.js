(function() {
    'use strict';
    angular
        .module('mawjoudin2App')
        .factory('NewsLetter', NewsLetter)
        .factory('SendingNewsLetter', SendingNewsLetter);

    SendingNewsLetter.$inject = ['$resource', 'DateUtils'];

    function SendingNewsLetter($resource, DateUtils) {
        var resourceUrl = 'api/send-news-letters/:id';

        return $resource(resourceUrl, {}, {
            'send': { method: 'GET', isArray: true }
        });
    }
    NewsLetter.$inject = ['$resource', 'DateUtils'];

    function NewsLetter($resource, DateUtils) {
        var resourceUrl = 'api/news-letters/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creationDate = DateUtils.convertDateTimeFromServer(data.creationDate);
                        data.sendingDate = DateUtils.convertDateTimeFromServer(data.sendingDate);
                    }
                    return data;
                }
            },
            'update': { method: 'PUT' }
        });
    }




})();