(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('MessageDetailController', MessageDetailController);

    MessageDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'Message', 'Image', 'Profile'];

    function MessageDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, Message, Image, Profile) {
        var vm = this;

        vm.message = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('mawjoudin2App:messageUpdate', function(event, result) {
            vm.message = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
