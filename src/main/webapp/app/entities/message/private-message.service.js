(function() {
    'use strict';
    angular
        .module('mawjoudin2App')
        .factory('PrivateMessage', PrivateMessage)
        .factory('PrivateUnseenMessage', PrivateUnseenMessage);

    PrivateUnseenMessage.$inject = ['$resource', 'DateUtils'];

    function PrivateUnseenMessage($resource, DateUtils) {
        var resourceUrl = 'api/unseen-private-messages-number/:id';

        return $resource(resourceUrl, {}, {
            'query': {
                method: 'GET',
                isArray: true
            },

        });
    }

    PrivateMessage.$inject = ['$resource', 'DateUtils'];

    function PrivateMessage($resource, DateUtils) {
        var resourceUrl = 'api/private-messages/:uid';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true },

        });
    }
})();