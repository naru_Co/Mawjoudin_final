(function() {
    'use strict';
    angular
        .module('mawjoudin2App')
        .factory('SiteDescription', SiteDescription);

    SiteDescription.$inject = ['$resource'];

    function SiteDescription ($resource) {
        var resourceUrl =  'api/site-descriptions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
