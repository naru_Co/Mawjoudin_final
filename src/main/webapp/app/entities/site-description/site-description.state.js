(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('site-description', {
            parent: 'entity',
            url: '/hello',
            data: {
                pageTitle: 'mawjoudin2App.siteDescription.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-description/site-descriptions.html',
                    controller: 'HomeController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('siteDescription');
                    $translatePartialLoader.addPart('home');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('site-description-detail', {
            parent: 'site-description',
            url: '/site-description/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'mawjoudin2App.siteDescription.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site-description/site-description-detail.html',
                    controller: 'SiteDescriptionDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('siteDescription');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'SiteDescription', function($stateParams, SiteDescription) {
                    return SiteDescription.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'site-description',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('site-description-detail.edit', {
            parent: 'site-description-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-description/site-description-dialog.html',
                    controller: 'SiteDescriptionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteDescription', function(SiteDescription) {
                            return SiteDescription.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-description.new', {
            parent: 'site-description',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-description/site-description-dialog.html',
                    controller: 'SiteDescriptionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                content: null,
                                lang: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('site-description', null, { reload: 'site-description' });
                }, function() {
                    $state.go('site-description');
                });
            }]
        })
        .state('site-description.edit', {
            parent: 'site-description',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-description/site-description-dialog.html',
                    controller: 'SiteDescriptionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SiteDescription', function(SiteDescription) {
                            return SiteDescription.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-description', null, { reload: 'site-description' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('site-description.delete', {
            parent: 'site-description',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site-description/site-description-delete-dialog.html',
                    controller: 'SiteDescriptionDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SiteDescription', function(SiteDescription) {
                            return SiteDescription.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('site-description', null, { reload: 'site-description' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
