(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('SiteDescriptionDeleteController',SiteDescriptionDeleteController);

    SiteDescriptionDeleteController.$inject = ['$uibModalInstance', 'entity', 'SiteDescription'];

    function SiteDescriptionDeleteController($uibModalInstance, entity, SiteDescription) {
        var vm = this;

        vm.siteDescription = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SiteDescription.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
