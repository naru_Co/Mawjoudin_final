(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('SiteDescriptionDialogController', SiteDescriptionDialogController);

    SiteDescriptionDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'SiteDescription'];

    function SiteDescriptionDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, SiteDescription) {
        var vm = this;

        vm.siteDescription = entity;
        vm.clear = clear;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.siteDescription.id !== null) {
                SiteDescription.update(vm.siteDescription, onSaveSuccess, onSaveError);
            } else {
                SiteDescription.save(vm.siteDescription, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('mawjoudin2App:siteDescriptionUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
