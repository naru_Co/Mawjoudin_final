(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('SiteDescriptionDetailController', SiteDescriptionDetailController);

    SiteDescriptionDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'SiteDescription'];

    function SiteDescriptionDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, SiteDescription) {
        var vm = this;

        vm.siteDescription = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('mawjoudin2App:siteDescriptionUpdate', function(event, result) {
            vm.siteDescription = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
