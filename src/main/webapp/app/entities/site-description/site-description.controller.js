(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('SiteDescriptionController', SiteDescriptionController);

    SiteDescriptionController.$inject = ['DataUtils', 'SiteDescription'];

    function SiteDescriptionController(DataUtils, SiteDescription) {

        var vm = this;

        vm.siteDescriptions = [];
        vm.openFile = DataUtils.openFile;
        vm.byteSize = DataUtils.byteSize;

        loadAll();

        function loadAll() {
            SiteDescription.query(function(result) {
                vm.siteDescriptions = result;
                vm.searchQuery = null;
            });
        }
    }
})();
