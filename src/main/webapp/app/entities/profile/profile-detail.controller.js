(function () {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('ProfileDetailController', ProfileDetailController);

    ProfileDetailController.$inject = ['$filter','$scope', '$mdDialog', '$state', '$uibModal', '$rootScope', '$stateParams', 'entity', 'ParseLinks', 'paginationConstants', 'Profile', 'Image', 'Agenda', 'User', 'Principal', 'Friend', 'Publication', 'Blog', 'PubComment', 'PublicationsByProfile', 'CommentByPublication', 'BlogByProfileAndState', 'GalleryByProfileAndType', 'ImageByGallery', 'FriendsByProfile', 'Friendship', 'EventsByProfil', 'Invitations', 'RequestsBySender'];

    function ProfileDetailController($filter,$scope, $mdDialog, $state, $uibModal, $rootScope, $stateParams, entity, ParseLinks, paginationConstants, Profile, Image, Agenda, User, Principal, Friend, Publication, Blog, PubComment, PublicationsByProfile, CommentByPublication, BlogByProfileAndState, GalleryByProfileAndType, ImageByGallery, FriendsByProfile, Friendship, EventsByProfil, Invitations, RequestsBySender) {

        /* -----------------------------------------------------------------------------------------

                           Variables Initialisation
                												 
        --------------------------------------------------------------------------------------------- */
        var vm = this;
        vm.getAccount = getAccount;
        $scope.isAFiend = false;
        $scope.isRequested = false;
        vm.profile = entity;
        $scope.x = 3
        vm.friendship = {};
        getAccount();
        $scope.profileWithImage = {
            profile: vm.profile,
            file: null,
            name: null,
            type: null
        };

 

        /* -----------------------------------------------------------------------------------------

                           Loading Account with all its related entities
                												 
        --------------------------------------------------------------------------------------------- */
        function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
                if (vm.account != null) {
                    if (vm.account.profile.id != vm.profile.id) {
                        Friendship.get({
                            receiver_id: vm.profile.id,
                            sender_id: vm.account.profile.id
                        }, function (result) {
                            vm.friendship = result;
                            if (result != null) {

                                $scope.isAFiend = true;
                                $scope.actualFollow = result
                            }
                        }, function () {

                            vm.friendship = Friendship.get({
                                receiver_id: vm.account.profile.id,
                                sender_id: vm.profile.id
                            }, function (result) {
                                if (result != null) {

                                    $scope.isAFiend = true;
                                    $scope.actualFollow = result
                                }
                            });

                        })
                    }

                    vm.isAuthenticated = Principal.isAuthenticated;
                }
            });
        }


        /* -----------------------------------------------------------------------------------------
        
                               Edit Bio and other Information inside The Profil
                                                                     
            --------------------------------------------------------------------------------------------- */


        $scope.myImage = '';

        $scope.myCroppedImage = '';



        $scope.$watch('myCroppedImage', function () {

            if ($scope.myCroppedImage != '') {
                $scope.profileWithImage.file = $scope.myCroppedImage.split(',')[1];
            }
        });

        var handleFileSelect = function (evt) {
            var file = evt.currentTarget.files[0];

            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function ($scope) {
                    $scope.myImage = evt.target.result;


                });
            };
            reader.readAsDataURL(file);


        };
        angular.element(document.querySelector('#fileInput1')).on('change', handleFileSelect);

        $scope.ChangeImage = function () {
            vm.isSaving = true;
            Profile.update($scope.profileWithImage, onSaveSuccessImage, onSaveError);
        }
        function onSaveSuccessImage(result) {
            $scope.$emit('netbedlooApp:profileUpdate', result);
            vm.isSaving = false;
            vm.profile.image = result.image;
            vm.profilImages.push(result.image)
            $scope.myImage = '';
            $scope.myCroppedImage = '';
            $rootScope.$broadcast('imageChanged', { a: result.image })
        }
        $scope.saveBio = function () {
            vm.isSaving = true;

            Profile.update(vm.profile, onSaveSuccess, onSaveError);

            $scope.edit = 0;
            $scope.editInfo = 0;


        }
        vm.updatedprofile = { profile: {} }
        $scope.saveInfos = function () {
            vm.isSaving = true;
            vm.profile.birthdate = new Date(vm.profile.birthdate);
            vm.updatedprofile.profile = vm.profile;
            Profile.update(vm.updatedprofile, onSaveSuccess, onSaveError);

            vm.profile.birthdate = $scope.birthdate
            $scope.editInfo = 0;


        }

        function onSaveSuccess(result) {
            $scope.$emit('netbedlooApp:profileUpdate', result);
            vm.isSaving = false;
            loadAll()

        }

        function onSaveError() {
            vm.isSaving = false;
        }




        $scope.edit = 0;

        $scope.editBio = function () {
            $scope.edit = 1;
        }
        $scope.cancelBio = function () {
            $scope.edit = 0;

        }
        $scope.cancel = function () {

            $scope.editInfo = 0;


        }
        /* -----------------------------------------------------------------------------------------
    
                                   Working with Publications and Comments
                                                                     
          --------------------------------------------------------------------------------------------- */

        /*--------------------------------Publication Save and Get -------------------------*/
        vm.publication = {};
        vm.publications = [];
        vm.loadPage = loadPage;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.page = 0;
        vm.pubLinks = {
            last: 0
        };
        vm.predicate = 'id';
        vm.reverse = true;
        vm.loadAll = loadAll;
        vm.savePublication = savePublication;

        loadAll()


        function loadAll() {
            

            PublicationsByProfile.get({
                profil_id: vm.profile.id,
                page: vm.page,
                size: 5,
                sort: sort()
            }, onSuccessPub);

            function sort() {
                var result = ['id' + ',' + 'desc'];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }

        }

        function onSuccessPub(data, headers) {
           
            vm.pubLinks = ParseLinks.parse(headers('link'));
            vm.totalItems = headers('X-Total-Count');
            for (var i = 0; i < data.length; i++) {
                loadAllComments(data[i])
                vm.publications.push(data[i]);

            }
        }



        function loadPage(page) {
            vm.page = page;
            loadAll();
        }




        $scope.mySharedImage = '';

        $scope.myCroppedSharedImage = '';
        $scope.rectangleWidth = 100;
        $scope.rectangleHeight = 100;

        $scope.cropper = {
            cropWidth: $scope.rectangleWidth,
            cropHeight: $scope.rectangleHeight
        };
        $scope.$watch('myCroppedSharedImage', function () {

            if ($scope.myCroppedSharedImage != '') {
                vm.publication.file = $scope.myCroppedSharedImage.split(',')[1];
            }
        });

        var handleFileSelect = function (evt) {
            var file = evt.currentTarget.files[0];
            vm.publication.name = file.name;
            vm.publication.type = file.type;
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function ($scope) {
                    $scope.mySharedImage = evt.target.result;


                });
            };
            reader.readAsDataURL(file);


        };
        angular.element(document.querySelector('#fileInput3')).on('change', handleFileSelect);


        function savePublication() {
            vm.isSaving = true;
            vm.publication.publication.profile = vm.account.profile;

            Publication.save(vm.publication, onSaveSuccessPublication, onSaveErrorPublication);;
        }

        function deletePublication(publication) {
            var index = vm.publications.indexOf(publication);
            vm.publications.splice(index, 1);
            Publication.delete({ id: publication.id });
        }

        function deleteComment(comment, publication) {
            var index = publication.comments.indexOf(comment);
            publication.comments.splice(index, 1);
            PubComment.delete({ id: comment.id });
        }

        $scope.x = 3
        
        function onSaveSuccessPublication(result) {
            $scope.$emit('mawjoudin2App:publicationUpdate', result);
            vm.isSaving = false;
           
            vm.publications.splice(0, 0, result);
            vm.publication = {};
            $scope.mySharedImage = '';
            $scope.myCroppedSharedImage = '';
        }

        function onSaveErrorPublication() {
            vm.isSaving = false;
        }
        $scope.editPosted = 0;

        $scope.editPosssst = function (publication) {

            $scope.editPosted = 1;

        }

        $scope.editPost = function (publication) {
            vm.isSaving = true;
            publication.profile = vm.account.profile;
            Publication.update(publication, onSaveSuccess, onSaveError);

            $scope.editPosted = 0;



        }

        /*--------------------------------Comment Save and Get -------------------------*/

        vm.comment = {};
        vm.comments = [];
        vm.saveComment = saveComment;
        vm.loadAllComments = loadAllComments;


        function loadAllComments(publication) {
            var translate = $filter('translate');
            var comments = CommentByPublication.get({
                pub_id: publication.id
            }, function (data) {
                for (var j = 0; j < data.length; j++) {
                    var today = new Date();
                    var Christmas = new Date(data[j].creationDate);
                    var diffMs = (today - Christmas);
                    var diffDays = Math.floor(diffMs / 86400000);
                    var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
                    var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);
                   // minutes
                   if(diffDays===0){
                       if(diffHrs===0){
                           if(diffMins===0){
                            data[j].diff=translate('mawjoudin2App.profile.seconds')   
                           }else if(diffMins<60){
                            data[j].diff=diffMins+' '+translate('mawjoudin2App.profile.minutes')
                           }
                        }else if(diffHrs<24){
                            data[j].diff=diffHrs+' '+translate('mawjoudin2App.profile.hours')
                        }
                       }else if(diffDays<7){
                        data[j].diff=diffDays+' '+translate('mawjoudin2App.profile.days')
                       }else{
                        data[j].diff=0   
                       }
                   }
                   
                

                publication.comments = data
            });

        }


        $scope.loadMore = function (comments) {
            $scope.x = comments.length;
        }
       

        function saveComment(publication) {
           
            vm.isSaving = true;
            vm.comment.content=publication.comment.content
            vm.comment.publication = publication;
            vm.comment.profile = vm.account.profile;
            vm.comment.creationDate = new Date();
            PubComment.save(vm.comment, function (result) {
                $scope.$emit('mawjoudin2App:commentUpdate', result);
                vm.isSaving = false;
                loadAllComments(publication)
                publication.comment.content='';
                 vm.comment={};
            });

        }



        /* -----------------------------------------------------------------------------------------
        
                                       Working with Blog and its Articles
                                                                         
              --------------------------------------------------------------------------------------------- */
        vm.blog = {};
        $scope.myCroppedBlogImage = '';
        $scope.myBlogImage = '';
        vm.saveBlog = saveBlog;
        vm.onHoldBlogs = [];


        $scope.$watch('myCroppedBlogImage', function () {

            if ($scope.myCroppedBlogImage != '') {
                vm.blog.file = $scope.myCroppedBlogImage.split(',')[1];
            }
        });

        var handleFileSelect = function (evt) {
            var file = evt.currentTarget.files[0];
            vm.blog.name = file.name;
            vm.blog.type = file.type;
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function ($scope) {
                    $scope.myBlogImage = evt.target.result;


                });
            };
            reader.readAsDataURL(file);


        };
        angular.element(document.querySelector('#fileInput4')).on('change', handleFileSelect);
        $scope.arab = false;
        $scope.isArabic = function (text) {
            var pattern = /[\u0600-\u06FF\u0750-\u077F]/;
            $scope.arab = pattern.test(text);
            return $scope.arab;
        }

        function saveBlog() {
            vm.isSaving = true;
            vm.blog.blog.profile = vm.account.profile;
            Blog.save(vm.blog, onSaveSuccessBlog, onSaveErrorBlog);

        }

        function onSaveSuccessBlog(result) {
            $scope.$emit('mawjoudin2App:blogUpdate', result);
            vm.onHoldBlogs.push(result);
            vm.isSaving = false;
            vm.blog = {};

        }

        function onSaveErrorBlog() {
            vm.isSaving = false;
        }

        vm.loadPageBlog = loadPageBlog;
        vm.loadPageBlogPublished = loadPageBlogPublished;
        vm.publishedArticles = [];
        vm.blogLinks = {
            last: 0
        };
        vm.onHoldLinks = {
            last: 0
        };
        vm.pageBlog = 0;
        vm.blogPredicatePublish = 'id';
        vm.blogPredicate = 'id';
        vm.pageBlogPublished = 0;
        vm.loadAllOnHoldBlogs = loadAllOnHoldBlogs;
        vm.loadAllPublishedBlogs = loadAllPublishedBlogs;
        loadAllPublishedBlogs();
        loadAllOnHoldBlogs()

        function loadAllPublishedBlogs() {

            BlogByProfileAndState.query({
                profil_id: vm.profile.id,
                state: 1,
                page: vm.pageBlogPublished,
                size: 4,
                sort: sortPublished()
            }, onSuccessPublished, onErrorPublished);





            function sortPublished() {
                var result = ['id' + ',' + 'desc'];
                if (vm.blogPredicatePublish !== 'id') {
                    result.push('id');
                }
                return result;
            }

            function onSuccessPublished(data, headers) {
                vm.blogLinks = ParseLinks.parse(headers('link'));

                vm.totalPublishedItems = headers('X-Total-Count');

                for (var i = 0; i < data.length; i++) {

                    vm.publishedArticles.push(data[i]);

                }
            }

            function onErrorPublished(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadAllOnHoldBlogs() {
            BlogByProfileAndState.get({
                profil_id: vm.profile.id,
                state: 0,
                page: vm.pageBlog,
                size: 4,
                sort: sortOnHold()
            }, onSuccessOnHold, onErrorOnHold);





            function sortOnHold() {
                var result = ['id' + ',' + 'desc'];
                if (vm.blogPredicate !== 'id') {
                    result.push('id');
                }
                return result;
            }

            function onSuccessOnHold(data, headers) {
                vm.onHoldLinks = ParseLinks.parse(headers('link'));
                vm.totalOnHoldItems = headers('X-Total-Count');
                for (var i = 0; i < data.length; i++) {

                    vm.onHoldBlogs.push(data[i]);

                }
            }

            function onErrorOnHold(error) {
                AlertService.error(error.data.message);
            }
        }


        function loadPageBlogPublished() {
            vm.pageBlogPublished = vm.pageBlogPublished + 1;
            loadAllPublishedBlogs();
        }

        function loadPageBlog() {
            vm.pageBlog = vm.pageBlog + 1;
            loadAllOnHoldBlogs();

        }


        /* -----------------------------------------------------------------------------------------
        
                                     Working with Profile Image Gallery
                                                                       
         
        --------------------------------------------------------------------------------------------- */


        vm.profilImages = [];
        vm.sharedImages = [];
        vm.loadPageImageProfile = loadPageImageShared
        vm.loadPageImageShared = loadPageImageShared;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.pageImageProfil = 0;
        vm.pageImageShared = 0;
        vm.profilImagelinks = {
            last: 0
        };
        vm.imageShlinks = {
            last: 0
        };
        vm.predicateImage = 'id';
        vm.predicateImageSh = 'id';

        vm.loadAllSharedImage = loadAllSharedImage;
        vm.loadAllProfileImage = loadAllProfileImage;


        loadAllSharedImage();
        loadAllProfileImage();

        function loadAllProfileImage() {

            GalleryByProfileAndType.get({
                profil_id: vm.profile.id,
                type: 0
            }, function (result) {
                ImageByGallery.query({
                    imageGallery_id: result.id,
                    page: vm.pageImageProfil,
                    size: 12,
                    sort: sortImage()
                }, onSuccessImage, onErrorImage);
            })

            function sortImage() {
                var result = ['id' + ',' + 'desc'];
                if (vm.predicateImage !== 'id') {
                    result.push('id');
                }
                return result;
            }

            function onSuccessImage(data, headers) {
                vm.profilImagelinks = ParseLinks.parse(headers('link'));
                vm.totalProfilImageItems = headers('X-Total-Count');
                for (var i = 0; i < data.length; i++) {

                    vm.profilImages.push(data[i]);

                }

            }

            function onErrorImage(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadAllSharedImage() {
            GalleryByProfileAndType.get({
                profil_id: vm.profile.id,
                type: 1
            }, function (result) {
                ImageByGallery.query({
                    imageGallery_id: result.id,
                    page: vm.pageImageShared,
                    size: 12,
                    sort: sortImageSh()
                }, onSuccessSh, onErrorSh);
            })

            function sortImageSh() {
                var result = ['id' + ',' + 'desc'];
                if (vm.predicateImageSh !== 'id') {
                    result.push('id');
                }
                return result;
            }

            function onSuccessSh(data, headers) {
                vm.imageShlinks = ParseLinks.parse(headers('link'));
                vm.totalShItems = headers('X-Total-Count');
                for (var i = 0; i < data.length; i++) {

                    vm.sharedImages.push(data[i]);

                }
            }

            function onErrorSh(error) {
                AlertService.error(error.data.message);
            }
        }



        function loadPageImageShared(pageImageShared) {
            vm.pageImageShared = pageImageShared;
            loadAllSharedImage();
        }

        function loadPageImageProfile(pageImageProfil) {
            vm.pageImageProfil = pageImageProfil;
            loadAllProfileImage();
        }


        /* -----------------------------------------------------------------------------------------
         
                                         Working with Profile Friends And Requested Invitations
                                                                           
            
        --------------------------------------------------------------------------------------------- */

        vm.friends = [];
        vm.invitations = [];
        vm.requests = [];
        vm.loadAllInvitations = loadAllInvitations;
        vm.loadAllFriends = loadAllFriends;
        vm.loadAllRequests = loadAllRequests
        loadAllFriends();
        loadAllInvitations()
        loadAllRequests();

        function loadAllFriends() {
            vm.friends = FriendsByProfile.get({
                receiver_id: vm.profile.id,
                state: 1,
            });

        }


        function loadAllInvitations() {
            vm.invitations = Invitations.get({
                receiver_id: vm.profile.id,
                state: 0,
            });

        }

        function loadAllRequests() {
            vm.requests = RequestsBySender.get({
                sender_id: vm.profile.id,
                state: 0,
            });

        }


        $scope.acceptInvitation = function (invitation) {
            invitation.state = 1;
            Friend.update(invitation, onSaveFriendSuccess, onSaveFriendError);
            location.reload();
        }

        function onSaveFriendSuccess(result) {
            $scope.$emit('mawjoudin2App:friendUpdate', result);
            vm.isSaving = false;
            loadAllFriends();

            $scope.isAFiend = true;
        }

        function onSaveFriendError() {
            vm.isSaving = false;
        }
        $scope.showConfirm4 = function (ev, comment, publication) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Effacer Mon Commentaire')
                .textContent('Êtes-vous sûr ?')
                .ariaLabel('Je Supprime!')
                .targetEvent(ev)
                .ok('Oui!')
                .cancel('Non');

            $mdDialog.show(confirm).then(function () {
                deleteComment(comment, publication)

            }, function () { });
        };
        $scope.showConfirm3 = function (ev, publication) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Effacer Ma Publication')
                .textContent('Êtes-vous sûr ?')
                .ariaLabel('Je Supprime!')
                .targetEvent(ev)
                .ok('Oui!')
                .cancel('Non');

            $mdDialog.show(confirm).then(function () {
                deletePublication(publication)

            }, function () { });
        };

        $scope.showConfirm = function (ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Suivre ' + vm.profile.firstName + ' ' + vm.profile.lastName + '?')
                .textContent('Voulez vous rester en contact avec ' + vm.profile.firstName + ' ' + vm.profile.lastName + ' et suivre ses activités ?')
                .ariaLabel('Follow!')
                .targetEvent(ev)
                .ok('Oui!')
                .cancel('Non');

            $mdDialog.show(confirm).then(function () {
                followProfile()

            }, function () { });
        };



        $scope.showConfirm2 = function (ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Arreter de suivre ' + vm.profile.firstName + ' ' + vm.profile.lastName + '?')
                .textContent('Voulez vous quitter ' + vm.profile.firstName + ' ' + vm.profile.lastName + ' et ne plus suivre ses activités ?')
                .ariaLabel('Follow!')
                .targetEvent(ev)
                .ok('Oui oui!')
                .cancel('Je lui donne une autre chance');

            $mdDialog.show(confirm).then(function () {
                StopfollowProfile()

            }, function () { });
        };

        function followProfile() {
            var friend = {};
            friend.sender = vm.account.profile;
            friend.receiver = vm.profile;
            Friend.save(friend, function () {
                $rootScope.$broadcast('followoperation');
            })
            $scope.isAFiend = true;
            loadAllInvitations();

        }

        function StopfollowProfile() {

            Friend.delete($scope.actualFollow, function () {
                $rootScope.$broadcast('followoperation');
            })
            $scope.isAFiend = false;
            loadAllFriends();
            loadAllInvitations();

        }



        /* -----------------------------------------------------------------------------------------
           
                                                Get All Profil Events
                                                                                  
                   
            --------------------------------------------------------------------------------------------- */


        // calender stuff
        vm.calendarView = 'month';
        vm.viewDate = new Date();
        vm.eventers = EventsByProfil.get({
            profil_id: vm.profile.id
        }, function (result) {
            for (var i = 0; i < result.length; i++) {
                result[i].startsAt = new Date(result[i].startsAt);
                result[i].endsAt = new Date(result[i].endsAt);
            }
            vm.events = result
        })


        vm.cellIsOpen = true;




        //dialog page for agenda


        $scope.showAdvanced = function (args) {
            $mdDialog.show({
                controller: 'eventdialogdetailController',
                templateUrl: 'app/admin/dashboard/eventdialog.html',
                locals: {
                    items: args
                },
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
                .then(function (answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function () {
                    $scope.status = 'You cancelled the dialog.';
                });
        };



        vm.eventClicked = function (event) {
            $scope.showAdvanced(event)

        };



        vm.eventTimesChanged = function (event) {
            alert.show('Dropped or resized', event);
        };

        vm.toggle = function ($event, field, event) {
            $event.preventDefault();
            $event.stopPropagation();
            event[field] = !event[field];
        };

        vm.timespanClicked = function (date, cell) {

            if (vm.calendarView === 'month') {
                if ((vm.cellIsOpen && moment(date).startOf('day').isSame(moment(vm.viewDate).startOf('day'))) || cell.events.length === 0 || !cell.inMonth) {
                    vm.cellIsOpen = false;
                } else {
                    vm.cellIsOpen = true;
                    vm.viewDate = date;
                }
            } else if (vm.calendarView === 'year') {
                if ((vm.cellIsOpen && moment(date).startOf('month').isSame(moment(vm.viewDate).startOf('month'))) || cell.events.length === 0) {
                    vm.cellIsOpen = false;
                } else {
                    vm.cellIsOpen = true;
                    vm.viewDate = date;
                }
            }
        }






        var unsubscribe = $rootScope.$on('mawjoudin2App:profileUpdate', function (event, result) {
            vm.profile = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();