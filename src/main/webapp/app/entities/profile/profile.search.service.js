(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .factory('ProfileSearch', ProfileSearch);

    ProfileSearch.$inject = ['$resource'];

    function ProfileSearch($resource) {
        var resourceUrl = 'api/_search/profiles/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true }
        });
    }
})();