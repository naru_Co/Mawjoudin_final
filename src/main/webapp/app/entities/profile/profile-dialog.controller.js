(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('ProfileDialogController', ProfileDialogController);

    ProfileDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Profile', 'Image', 'Agenda', 'User'];

    function ProfileDialogController($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Profile, Image, Agenda, User) {
        var vm = this;

        vm.profile = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.images = Image.query({ filter: 'profile-is-null' });
        $q.all([vm.profile.$promise, vm.images.$promise]).then(function() {
            if (!vm.profile.image || !vm.profile.image.id) {
                return $q.reject();
            }
            return Image.get({ id: vm.profile.image.id }).$promise;
        }).then(function(image) {
            vm.images.push(image);
        });
        vm.agenda = Agenda.query();
        vm.users = User.query();

        $timeout(function() {
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            if (vm.profile.id !== null) {
                Profile.update(vm.profile, onSaveSuccess, onSaveError);
            } else {
                Profile.save(vm.profile, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $scope.$emit('mawjoudin2App:profileUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.birthdate = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();