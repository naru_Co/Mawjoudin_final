(function() {
    'use strict';
    angular
        .module('mawjoudin2App')
        .factory('PubComment', PubComment)
        .factory('CommentByPublication', CommentByPublication)
        .factory('CommentByForum', CommentByForum);

    CommentByForum.$inject = ['$resource', 'DateUtils'];

    function CommentByForum($resource, DateUtils) {
        var resourceUrl = 'api/commentsbyforum/:subject_id';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });
    }

    CommentByPublication.$inject = ['$resource', 'DateUtils'];

    function CommentByPublication($resource, DateUtils) {
        var resourceUrl = 'api/commentsbypublication/:pub_id';

        return $resource(resourceUrl, {}, {
            'get': { method: 'GET', isArray: true }
        });
    }
    PubComment.$inject = ['$resource', 'DateUtils'];

    function PubComment($resource, DateUtils) {
        var resourceUrl = 'api/comments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function(data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.creationDate = DateUtils.convertDateTimeFromServer(data.creationDate);
                    }
                    return data;
                }
            },
            'update': { method: 'PUT' }
            
        });
    }
})();