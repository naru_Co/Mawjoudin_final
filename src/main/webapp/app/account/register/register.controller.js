(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('RegisterController', RegisterController);


    RegisterController.$inject = ['$scope', '$translate', '$timeout', 'Auth', 'LoginService', 'Profile'];

    function RegisterController($scope, $translate, $timeout, Auth, LoginService, Profile) {
        var vm = this;
        $scope.accept = false;
        vm.doNotMatch = null;
        vm.error = null;
        vm.errorUserExists = null;
        vm.login = LoginService.open;
        vm.register = register;
        vm.registerAccount = {};
        vm.success = null;
        vm.profile = {};
        $scope.myImage = '';
        $scope.image='app/home/user.jpg'
        $scope.myCroppedImage = '';
        $scope.$watch('myCroppedImage', function() {

            if ($scope.myCroppedImage != '') {
                vm.profile.file = $scope.myCroppedImage.split(',')[1];   
            }
        });

        var handleFileSelect = function(evt) {
            var file = evt.currentTarget.files[0];
            vm.profile.name = file.name;
            vm.profile.type = file.type;
            var reader = new FileReader();
            reader.onload = function(evt) {
                $scope.$apply(function($scope) {
                    $scope.myImage = evt.target.result;


                });
            };
            reader.readAsDataURL(file);


        };


        angular.element(document.querySelector('#fileInput2')).on('change', handleFileSelect);



        $timeout(function() { angular.element('#login').focus(); });




        function register() {
            if (vm.registerAccount.password !== vm.confirmPassword) {
                vm.doNotMatch = 'ERROR';
            } else {
                vm.registerAccount.langKey = $translate.use();
                vm.registerAccount.firstName = vm.profile.profile.firstName;
                vm.registerAccount.lastName = vm.profile.profile.lastName;
                vm.profile.profile.nickname = vm.registerAccount.login;
                vm.doNotMatch = null;
                vm.error = null;
                vm.errorUserExists = null;
                vm.errorEmailExists = null;

                if (vm.profile.profile.membership) {
                    vm.profile.profile.motivation = "Déjà un membre de l'organisation Mawjoudin";
                }

                function onSaveSuccess(result) {
                    $scope.$emit('netbedlooApp:profileUpdate', result);
                    $scope.enfinz = result;
                    vm.isSaving = false;
                }

                function onSaveError() {
                    vm.isSaving = false;
                }



                Profile.save(vm.profile, function(enfinz) {

                    vm.registerAccount.profile = enfinz;
                    Auth.createAccount(vm.registerAccount).then(function() {
                        vm.success = 'OK';

                    }).catch(function(response) {
                        vm.success = null;

                        if (response.status === 400 && response.data === 'login already in use') {
                            vm.errorUserExists = 'ERROR';
                        } else if (response.status === 400 && response.data === 'email address already in use') {
                            vm.errorEmailExists = 'ERROR';
                        } else {
                            vm.error = 'ERROR';
                        }
                    })
                });






            }
        }
    }
})();