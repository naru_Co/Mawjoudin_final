(function () {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['Profile', 'Slider', '$sce', 'About', '$cookies', '$rootScope', 'Partner', 'AlertService', 'Project', 'News', '$scope', 'Principal', 'LoginService', '$state', 'SiteDescription', '$translate','$timeout'];

    function HomeController(Profile, Slider, $sce, About, $cookies, $rootScope, Partner, AlertService, Project, News, $scope, Principal, LoginService, $state, SiteDescription, $translate,$timeout) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.register = register;
        $scope.recentProjects = [];
        $scope.recentArticles = [];
        $scope.descriptions = SiteDescription.query();
        $scope.myInterval = 5000;
        $scope.noWrapSlides = false;
        $scope.active = 0;
        vm.profile = {
            profile: {}
        };
        var slides = $scope.slides = [];
        var currIndex = 0;
        $scope.allSliders = Slider.query();

        $scope.subscribeNewletter = function () {
            vm.profile.profile.email = $scope.subsemail;
            Profile.save(vm.profile)
        }

        Partner.query(function (res) {
            $scope.allPartners = res;

        })
        $scope.lang = $translate.use()
        if ($scope.lang === 'en') {
            $scope.about = About.get({
                id: 17
            }, function (res) {
                vm.trusted = $sce.trustAsHtml(res.content);
            });
            $scope.description = SiteDescription.get({
                id: 15
            })
        } else if ($scope.lang === 'ar-ly') {
            $scope.about = About.get({
                id: 18
            }, function (res) {
                vm.trusted = $sce.trustAsHtml(res.content);
            });
            $scope.description = SiteDescription.get({
                id: 17
            })

        } else if ($scope.lang === 'fr') {
            $scope.about = About.get({
                id: 19
            }, function (res) {
                vm.trusted = $sce.trustAsHtml(res.content);
            });
            $scope.description = SiteDescription.get({
                id: 16
            })


        }
        //detect language changement
        $rootScope.$on('$translateChangeSuccess', function (event, current, previous) {
            $scope.lang = current.language
            if (current.language === 'en') {
                $scope.about = About.get({
                    id: 17
                }, function (res) {
                    vm.trusted = $sce.trustAsHtml(res.content);
                });
                $scope.description = SiteDescription.get({
                    id: 15
                })

            } else if (current.language === 'ar-ly') {
                $scope.about = About.get({
                    id: 18
                }, function (res) {
                    vm.trusted = $sce.trustAsHtml(res.content);
                });
                $scope.description = SiteDescription.get({
                    id: 17
                })

            } else {
                $scope.about = About.get({
                    id: 19
                }, function (res) {
                    vm.trusted = $sce.trustAsHtml(res.content);
                });
                $scope.description = SiteDescription.get({
                    id: 16
                })


            }
            //
        });


        $scope.addSlide = function () {
            var newWidth = 600 + slides.length + 1;
            slides.push({
                image: '//unsplash.it/' + newWidth + '/300',
                text: ['Nice image', 'Awesome photograph', 'That is so cool', 'I love that'][slides.length % 4],
                id: currIndex++
            });
        };

        $scope.randomize = function () {
            var indexes = generateIndexesArray();
            assignNewIndexesToSlides(indexes);
        };

        for (var i = 0; i < 4; i++) {
            $scope.addSlide();
        }

        // Randomize logic below

        function assignNewIndexesToSlides(indexes) {
            for (var i = 0, l = slides.length; i < l; i++) {
                slides[i].id = indexes.pop();
            }
        }

        function generateIndexesArray() {
            var indexes = [];
            for (var i = 0; i < currIndex; ++i) {
                indexes[i] = i;
            }
            return shuffle(indexes);
        }

        // http://stackoverflow.com/questions/962802#962890
        function shuffle(array) {
            var tmp, current, top = array.length;

            if (top) {
                while (--top) {
                    current = Math.floor(Math.random() * (top + 1));
                    tmp = array[current];
                    array[current] = array[top];
                    array[top] = tmp;
                }
            }

            return array;
        }
        $scope.$on('authenticationSuccess', function () {
            getAccount();
        });

        getAccount();

        function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }

        function register() {
            $state.go('register');
        }
        
        $scope.delayRegister=function(){
            $timeout( function(){
                $state.go('about');
            }, 1300 );
           
        }

        $scope.delayMaw=function(){
            $timeout( function(){
                $state.go('site-description');
            }, 1300 );
           
        }
        $scope.delayNews=function(){
            $timeout( function(){
                $state.go('news');
            }, 1300 );
           
        }
        $scope.delayProj=function(){
            $timeout( function(){
                $state.go('project');
            }, 1300 );
           
        }

        $scope.delayPart=function(){
            $timeout( function(){
                $state.go('partner');
            }, 1300 );
           
        }

        loadAll()

        function loadAll() {

            News.query({
                page: 0,
                size: 4,
                sort: sort()
            }, onSuccess, onError);

            Project.query({
                page: 0,
                size: 4,
                sort: sort()
            }, function (data, headers) {
                $scope.recentProjects = data;
            }, onError);


            function sort() {
                var result = ['creationDate' + ',' + 'desc'];
                return result;
            }

            function onSuccess(data, headers) {
                $scope.recentArticles = data;
            }



            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

    }
})();



