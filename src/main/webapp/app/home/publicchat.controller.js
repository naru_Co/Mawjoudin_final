(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('ChatController', ChatController);

    ChatController.$inject = ['Principal', '$scope', 'ChatService', '$translate', 'ChatControl'];

    function ChatController(Principal, $scope, ChatService, $translate, ChatControl) {
        var vm = this;
      
        $scope.messages = [];
        $scope.message = '';
        ChatService.disconnect();
        getAccount();
        $scope.chatControl = ChatControl.get({ id: 1 }, function(res) {
            $scope.startTime = moment(res.startDate);
            $scope.endTime = moment(res.endDate);
            if ((new Date() - $scope.startTime) > 0 && (new Date() - $scope.endTime) < 0) {
                $scope.between = 1;
            } else {
                $scope.between = 0;
            }
        })

        function getAccount() {

            ChatService.connect();
            ChatService.receive().then(null, null, function(message) {
                $scope.messages.push(message);
             
            });
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }
        $scope.sendMessage = function(message) {
           
            if (message.length === 0) {
                return;
            }
            ChatService.sendMessage(message);
            $scope.message = '';
        }


    }
})();