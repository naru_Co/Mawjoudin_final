(function () {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('NavbarController', NavbarController);

    NavbarController.$inject = ['Invitations', 'PrivateUnseenMessage', 'PrivateMessage', 'PrivateChatService', '$scope', '$state', 'Auth', 'Principal', 'ProfileService', 'LoginService', 'FriendsByProfile', 'ChatNotificationService', 'Message'];

    function NavbarController(Invitations, PrivateUnseenMessage, PrivateMessage, PrivateChatService, $scope, $state, Auth, Principal, ProfileService, LoginService, FriendsByProfile, ChatNotificationService, Message) {
        var vm = this;
        $scope.isVerified = false;
        vm.isNavbarCollapsed = true;
        vm.isAuthenticated = Principal.isAuthenticated;
        $scope.isConnected = false;
        getAccount();

        function loadAllFriends(currentProfile) {
            if (currentProfile != null) {
                vm.friends = FriendsByProfile.query({
                    receiver_id: currentProfile.id,
                    state: 1
                }, function (res) {
                    loadAllInvitations(currentProfile, res)

                })
            }
        }

        if ($state.includes('verification')) {
            $scope.chatInfos = [];

        }
        $scope.$on('verified', function (event, args) {
            getAccount()
            vm.theImage = 1;
        });

        $scope.$on('authenticationSuccess', function (event, args) {
            getAccount();
            $scope.chatInfos = [];
            vm.theImage = 1;
        });
        $scope.$on('followoperation', function (event, args) {
            getAccount();
        });
        $scope.$on('imageChanged', function (event, opt) {
            $scope.profileChanged = opt.a;
            vm.account.profile.image = $scope.profileChanged;
        });

        ProfileService.getProfileInfo().then(function (response) {
            vm.inProduction = response.inProduction;
            vm.swaggerEnabled = response.swaggerEnabled;
        });

        vm.login = login;
        vm.logout = logout;
        vm.toggleNavbar = toggleNavbar;
        vm.collapseNavbar = collapseNavbar;
        vm.$state = $state;
        $scope.account = null;
        vm.account = null;





        function login() {
            collapseNavbar();
            $scope.chatInfos = [];
            LoginService.open();
        }

        function logout() {
            collapseNavbar();
            $scope.chatInfos = [];
            $scope.user = false;
            vm.account = null
            Auth.logout();
            $scope.isVerified = false;
            $state.go('home');
        }

        function toggleNavbar() {
            vm.isNavbarCollapsed = !vm.isNavbarCollapsed;
        }

        function collapseNavbar() {
            vm.isNavbarCollapsed = true;
        }

        function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
                if (vm.account != null) {
                    $scope.user = true;

                    if (vm.account.authorities.length != 1) {
                        $scope.isVerified = true;
                    }
                    loadAllFriends(account.profile);
                    vm.isAuthenticated = Principal.isAuthenticated;
                    if (vm.account.profile != null) {
                        $scope.unseenMessages = PrivateUnseenMessage.query({
                            id: vm.account.profile.id
                        });

                        ChatNotificationService.connect(vm.account.profile.id);
                        ChatNotificationService.receive().then(null, null, function (pr) {
                            $scope.playNotifAudio();
                            if ($scope.chosedProfileId != pr.sender.id) {
                                $scope.unseenMessages.push(pr);

                                //vm.profiles to be replaced by friendsList
                                var index = vm.profiles.map(function (e) {
                                    return e.id;
                                }).indexOf(pr.sender.id);
                                vm.profiles[index].notif = vm.profiles[index].notif + 1;

                                $mdToast.show(
                                    $mdToast.simple()
                                    .textContent(pr.sender.firstName + " " + pr.sender.lastName + " vous a envoyé un message")
                                    .position('bottom left')
                                    .hideDelay(10000)
                                );
                            }



                        });
                    }
                } else {
                    $scope.user = false;
                }
            })
        }

        $scope.playNotifAudio = function () {
            var audio = new Audio('content/audio/notif.mp3');
            audio.play();
        };

        $scope.chatInfos = [];
        $scope.chosedProfileId = null;
        $scope.talkTo = function (profile) {

            $scope.chosedProfileId = profile.id;
            PrivateChatService.unsubscribe()
            $scope.messages = [];
            vm.message = '';
            var objDiv = document.getElementsByClassName(".chat-messages");
            objDiv.scrollTop = objDiv.scrollHeight;

            profile.notif = 0;
            //messaging part logic
            var id1 = 0;
            var id2 = 0;

            if (vm.account.profile.id > profile.id) {
                id1 = vm.account.profile.id;
                id2 = profile.id;
            } else {
                id1 = profile.id;
                id2 = vm.account.profile.id;
            }


            $scope.messages = PrivateMessage.get({
                uid: id1 + "naru" + id2
            }, function (result) {

                //update the notification number by seeing the unseen
                var tobeDeleted = 0;
                for (var i = 0; i < $scope.unseenMessages.length; i++) {
                    if ($scope.unseenMessages[i].sender.id === profile.id) {
                        tobeDeleted++;
                    }
                }
                $scope.unseenMessages.splice(-tobeDeleted, tobeDeleted)
                //end of service
                for (i = 1; i <= tobeDeleted; i++) {
                    Message.update(result[result.length - i], function (result2) {
                        result[result.length - i] = result2;
                    });
                }

            })





            PrivateChatService.connect(id1, id2);
            PrivateChatService.receive().then(null, null, function (message) {
                $scope.messages.push(message);
                //addMessages to the seen
                if (message.receiver.id === vm.account.profile.id) {
                    Message.update(message, function (result) {
                        message = result;
                    });
                }

            });

            var chatInfo = {
                receiver: profile,
                id1: id1,
                id2: id2,
                tabPos: 1
            }
            if ($scope.chatInfos.length === 4) {
                $scope.chatInfos.splice(0, 1);
                $scope.chatInfos.push(chatInfo);
            } else {
                $scope.chatInfos.push(chatInfo);
            }
            $scope.seenNotifs = function () {


            }

        }

        //send message function

        $scope.sendMessage = function (chatInfo) {

            if (vm.message.length === 0) {
                return;
            }
            var message = {};
            message.sender = vm.account.profile;
            message.receiver = chatInfo.receiver;
            message.content = vm.message;
            message.uid = chatInfo.id1 + "naru" + chatInfo.id2
            PrivateChatService.sendMessage(message, chatInfo.id1, chatInfo.id2);
            vm.message = '';

        }

        $scope.endtalk = function (item) {
            var index = $scope.chatInfos.indexOf(item);
            $scope.chatInfos.splice(index, 1);
        }

        vm.invitations = [];

        function loadAllInvitations(profile, friends) {
            vm.invitations = Invitations.get({
                receiver_id: profile.id,
                state: 0,
            }, function (res) {
                for (var i = 0; i < res.length; i++) {


                    var index = friends.map(function (e) {
                        return e.id;
                    }).indexOf(res[i].sender.id);
                    if (index > -1) {
                        vm.friends[index].isf = "0"

                    }
                }
            });

        }
    }
})();
