(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('verification', {
                parent: 'entity',
                url: '/verification',
                data: {
                   
                },
                views: {
                    'content@': {
                        templateUrl: 'app/components/verification/verification.html',
                        controller: 'VerificationController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('verification');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
    }

})();