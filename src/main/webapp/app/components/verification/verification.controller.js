(function () {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('VerificationController', VerificationController);

    VerificationController.$inject = ['$rootScope','$state', 'Auth', 'Principal', 'User'];

    function VerificationController($rootScope,$state, Auth, Principal, User) {
        var vm = this;

        vm.verifyPassword = verifyPassword;
        vm.doNotMatch = null;
        vm.error = null;
        vm.success = null;
        vm.password = '';
        setTimeout(function () {
            document.getElementById('div1').style.visibility = "visible";
        }, 5000);

        function verifyPassword() {
            vm.doNotMatch = null;
            Auth.verifyPassword(vm.password).then(function () {
                vm.error = null;
                vm.success = 'OK';
                $rootScope.$broadcast('verified');
                Principal.identity(true).then(function (account) {
                    // After the login the language will be changed to
                    // the language selected by the user during his registration  

                });
                
                $state.go('membershome');
                

            }).catch(function () {
                vm.success = null;
                vm.error = 'ERROR';
            });
        }
    }

})();