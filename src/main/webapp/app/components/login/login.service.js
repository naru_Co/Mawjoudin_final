(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .factory('LoginService', LoginService);

    LoginService.$inject = ['$uibModal', '$state'];

    function LoginService($uibModal, $state) {
        var service = {
            open: open
        };



        return service;

        function open() {
            $state.go('login', null, { reload: 'login' });
        }
    }
})();