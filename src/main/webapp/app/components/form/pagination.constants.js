(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
