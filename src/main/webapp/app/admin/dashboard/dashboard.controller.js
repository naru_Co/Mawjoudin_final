(function () {
    'use strict';

    angular
        .module('mawjoudin2App')


        .controller('dashboardController', dashboardController);

    dashboardController.$inject = ['Agenda', 'calendarConfig', 'moment', 'Partner', 'NewsLetter', '$mdDialog', '$q', 'DataUtils', 'Project', 'Image', 'News', '$scope', 'Principal', 'Glossaire', 'ProfileByName', 'BlogByState', 'Blog', 'ChatControl', 'JhiLanguageService', 'About', 'SiteDescription', 'Slider', 'Resource'];

    function dashboardController(Agenda, calendarConfig, moment, Partner, NewsLetter, $mdDialog, $q, DataUtils, Project, Image, News, $scope, Principal, Glossaire, ProfileByName, BlogByState, Blog, ChatControl, JhiLanguageService, About, SiteDescription, Slider, Resource) {
        var vm = this;
        vm.account = null;
        vm.languages = null;
        loadAll()
        vm.newevent = { id: null }
        vm.events = Agenda.query(function (result) {
            result.forEach(function (e) {
                e.color = calendarConfig.colorTypes.warning;
                e.actions = actions
            });
        })
        $scope.iscompleted = false


        JhiLanguageService.getAll().then(function (languages) {
            vm.languages = languages;
        });

        // calender stuff
        vm.calendarView = 'month';
        vm.viewDate = new Date();
        var actions = [{
            label: '<i class=\'glyphicon glyphicon-pencil\'></i>',
            onClick: function (args) {
                vm.newevent = args.calendarEvent;
                $scope.selectedProfiles = vm.newevent.profiles

                delete vm.element.actions;
                delete vm.element.calendarEventId;
                delete vm.element.color;
            }
        }, {
            label: '<i class=\'glyphicon glyphicon-remove\'></i>',
            onClick: function (args) {
                $scope.deleteEvent(args.calendarEvent)

            }
        }];


        vm.cellIsOpen = true;

        vm.addEvent = function (newevent, indc) {
            if (indc === 0) {
                newevent.id === null;
            }
            vm.newevent = { id: null }
            newevent.author = vm.account.profile;
            newevent.profiles = $scope.selectedProfiles;
            Agenda.save(newevent, function (res) {
                res.color = calendarConfig.colorTypes.important;
                res.actions = actions
                vm.events.push(
                    res

                );
            })


        };

        $scope.deleteEvent = function (ev) {
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete the event ' + ev.title + '?')
                .textContent('All the related informations will be deleted.')
                .ok('Yes')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function () {
                Agenda.delete({ id: ev.id }, function () {
                    vm.events = Agenda.query(function (result) {
                        result.forEach(function (e) {
                            e.color = calendarConfig.colorTypes.warning;
                            e.actions = actions
                        });
                    })
                })
            }, function () { });
        };

        //autocomplete for agenda
        $scope.transformChip = function (chip) {
            // If it is an object, it's already a known chip
            if (angular.isObject(chip)) {
                return chip;
            }

            // Otherwise, create a new one
            return { name: chip, type: 'new' }
        }

        $scope.selectedProfiles = [];

        $scope.querySearch = function (query) {

            return ProfileByName.get({ name: query.toLowerCase() }).$promise
        }

        // end of autocomplete for agenda

        //dialog page for agenda


        $scope.showAdvanced = function (args) {
            $mdDialog.show({
                controller: 'eventdialogdetailController',
                templateUrl: 'app/admin/dashboard/eventdialog.html',
                locals: {
                    items: args
                },
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
                .then(function (answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function () {
                    $scope.status = 'You cancelled the dialog.';
                });
        };



        vm.eventClicked = function (event) {
            $scope.showAdvanced(event)

        };

        vm.eventEdited = function (event) {
            alert.show('Edited', event);
        };

        vm.eventDeleted = function (event) {
            alert.show('Deleted', event);
        };

        vm.eventTimesChanged = function (event) {
            alert.show('Dropped or resized', event);
        };

        vm.toggle = function ($event, field, event) {
            $event.preventDefault();
            $event.stopPropagation();
            event[field] = !event[field];
        };

        vm.timespanClicked = function (date, cell) {

            if (vm.calendarView === 'month') {
                if ((vm.cellIsOpen && moment(date).startOf('day').isSame(moment(vm.viewDate).startOf('day'))) || cell.events.length === 0 || !cell.inMonth) {
                    vm.cellIsOpen = false;
                } else {
                    vm.cellIsOpen = true;
                    vm.viewDate = date;
                }
            } else if (vm.calendarView === 'year') {
                if ((vm.cellIsOpen && moment(date).startOf('month').isSame(moment(vm.viewDate).startOf('month'))) || cell.events.length === 0) {
                    vm.cellIsOpen = false;
                } else {
                    vm.cellIsOpen = true;
                    vm.viewDate = date;
                }
            }
        }

        // end of calender stuff

        vm.openFile = DataUtils.openFile;
        vm.setFile = function ($file, profile) {
            if ($file) {
                DataUtils.toBase64($file, function (base64Data) {

                    vm.newpartner.file = base64Data;


                });
            }
        };


        $scope.imageUpload = function (files) {

        }
        vm.choice = 0;
        vm.news = {};
        vm.project = {};
        vm.newletter = {};
        vm.newpartner = {}
        vm.isSaving = false;


        vm.save = save;
        vm.saveProject = saveProject;
        vm.saveNewsLetter = saveNewsLetter;

        $scope.pureImage = {};
        vm.newglossaire = {}
        $scope.myImage = '';
        $scope.myCroppedImage = '';
        $scope.details = -1;
        $scope.$watch('myCroppedImage', function () {

            if ($scope.myCroppedImage != '') {
                switch (vm.choice) {
                    case 1:
                        vm.news.file = $scope.myCroppedImage.split(',')[1];
                        break;
                    case 2:
                        vm.newletter.file = $scope.myCroppedImage.split(',')[1];
                        break;
                    case 3:
                        vm.project.file = $scope.myCroppedImage.split(',')[1];
                        break;
                    case 6:
                        vm.newpartner.file = $scope.myCroppedImage.split(',')[1];
                }


            }



        });
        $scope.switchdetails = function () {
            $scope.details = $scope.details * -1;
        }

        var handleFileSelect = function (evt) {
            var file = evt.currentTarget.files[0];
            vm.news.name = file.name;
            vm.news.type = file.type;
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function ($scope) {
                    $scope.myImage = evt.target.result;


                });
            };
            reader.readAsDataURL(file);


        };


        angular.element(document.querySelector('#fileInput2')).on('change', handleFileSelect);




        $scope.$on('authenticationSuccess', function () {
            getAccount();
        });

        getAccount();

        function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }
        $scope.options = {
            focus: true,
            toolbar: [
                ['edit', ['undo', 'redo']],
                ['headline', ['style']],
                ['style', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
                ['fontface', ['fontname']],
                ['textsize', ['fontsize']],
                ['fontclr', ['color']],
                ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video', 'hr']],
            ],
        };
        $scope.imageUpload = function (files) {
            uploadEditorImage(files);
        };
        $scope.imageUpload2 = function (files) {
            uploadEditorImage2(files);
        };

        function uploadEditorImage(image) {
            DataUtils.toBase64(image[0], function (base64Data) {
                $scope.pureImage.file = base64Data;
                $scope.iscompleted = true
                Image.save($scope.pureImage, function (result) {
                    var file_path = result.normalRes;
                    $scope.iscompleted = false
                    $scope.editor.summernote('insertImage', file_path, 'mawjoudin.jpg');
                }, function () {
                    $scope.iscompleted = false
                });


            });

        }

        function uploadEditorImage2(image) {
            DataUtils.toBase64(image[0], function (base64Data) {
                $scope.pureImage.file = base64Data;
                $scope.iscompleted = true

                Image.save($scope.pureImage, function (result) {
                    var file_path = result.normalRes;
                    $scope.iscompleted = false
                    $scope.editor2.summernote('insertImage', file_path, 'mawjoudin.jpg');
                }, function () {
                    $scope.iscompleted = false
                });


            });

        }

        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.images = Image.query();







        function save() {
            vm.isSaving = true;
            $scope.iscompleted = true

            vm.news.news.author = vm.account.profile;
            News.save(vm.news, onSaveSuccess, onSaveError);

        }

        function saveNewsLetter() {
            $scope.iscompleted = true

            vm.isSaving = true;
            vm.newsletter.status = 0;
            NewsLetter.save(vm.newsletter, onSaveSuccess, onSaveError)
        }

        function saveProject() {
            $scope.iscompleted = true

            vm.isSaving = true;

            vm.project.project.author = vm.account.profile;
            Project.save(vm.project, onSaveSuccess, onSaveError);

        }

        function onSaveSuccess(result) {
            $scope.iscompleted = false

            vm.isSaving = false;
            vm.news = {};
            vm.project = {};
            vm.newsletter = {};
            $scope.showConfirm()
            loadAll();

        }





        function onSaveError() {
            $scope.iscompleted = false

            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.creationDate = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }


        $scope.showConfirm = function () {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Votre Article à été publié avec succés')
                .ariaLabel('yeah')
                .ok('Okay')
                .cancel('Voir mes autres Articles');

            $mdDialog.show(confirm).then(function () {
                $scope.status = '';
            }, function () {
                $scope.status = '';
            });
        };

        //loading old news part

        function loadAll() {
            NewsLetter.query(function (result) {
                $scope.recentNewsLetters = result;
            });
            Project.query({
                page: 0,
                size: 4,
                sort: sort()
            }, ProjectonSuccess, onError);


            News.query({
                page: 0,
                size: 4,
                sort: sort()
            }, onSuccess, onError);

            Partner.query(function (res) {
                $scope.allPartners = res;
            })


            function sort() {
                var result = ['creationDate' + ',' + 'desc'];
                return result;
            }

            function onSuccess(data, headers) {

                $scope.recentArticles = data;
            }

            function ProjectonSuccess(data, headers) {

                $scope.recentProjects = data;

            }

            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function reset() {
            vm.page = 0;
            vm.news = [];
            loadAll();
        }

        function loadPage(page) {
            vm.page = page;
            loadAll();
        }

        function clear() {
            vm.news = [];
            vm.links = {
                last: 0
            };
            vm.page = 0;
            vm.predicate = 'id';
            vm.reverse = true;
            vm.searchQuery = null;
            vm.currentSearch = null;
            vm.loadAll();
        }

        function search(searchQuery) {
            if (!searchQuery) {
                return vm.clear();
            }
            vm.news = [];
            vm.links = {
                last: 0
            };
            vm.page = 0;
            vm.predicate = '_score';
            vm.reverse = false;
            vm.currentSearch = searchQuery;
            vm.loadAll();
        }
        $scope.Glossairepart = function () {
            vm.choice = 5;
            $scope.allGlossaire = Glossaire.query(function (res) { })


        }
        $scope.editglossary = function (glossaire) {
            glossaire.edit = 1;
        }
        $scope.saveglossary = function (glossaire) {
            $scope.iscompleted = true

            Glossaire.save(glossaire, function () {
                $scope.iscompleted = false
            }, function () {
                $scope.iscompleted = false

            })
            glossaire.edit = undefined;
        }
        $scope.saveNewglossary = function () {
            $scope.iscompleted = true

            Glossaire.save(vm.newglossaire, function () {
                $scope.iscompleted = false
                $scope.allGlossaire.push(vm.newglossaire);
                vm.newglossaire = {}

            }, function () {
                $scope.iscompleted = false
            });
        }
        $scope.deleteglossaire = function (glossaire) {
            Glossaire.delete({ id: glossaire.id },
                function () {
                    $scope.allGlossaire = Glossaire.query()
                });
        }
        $scope.deletepartner = function (partner) {
            Partner.delete({ id: partner.id },
                function () {
                    $scope.allPartners = Partner.query()
                });
        }
        $scope.savePartner = function () {
            $scope.iscompleted = true

            Partner.save(vm.newpartner, function () {
                $scope.iscompleted = false
                $scope.allPartners = Partner.query()
            }, function () {
                $scope.iscompleted = false
            });


        }

        /* Blog part */
        $scope.allWaitingBlogs = [];
        $scope.Blogpart = function () {
            vm.choice = 7;
            $scope.allWaitingBlogs = BlogByState.query({ state: 0 })


        }
        $scope.publishBlog = function (blog) {
            blog.state = 1;

            Blog.update(blog);
            $scope.showBlogConfirm()
        }

        $scope.deleteBlog = function (blog) {
            blog.state = 1;

            Blog.delete(blog.id);

        }

        $scope.showBlogConfirm = function () {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Le Blog a été mis en ligne')
                .ariaLabel('yeah')
                .ok('Okay')

            $mdDialog.show(confirm).then(function () {
                $scope.status = '';
            }, function () {
                $scope.status = '';
            });
        };

        /* End of blog part */

        /* chat control part */

        vm.chatControl = ChatControl.get({ id: 1 })

        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.datePickerOpenStatus.startDate = false;
        vm.datePickerOpenStatus.endDate = false;

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }
        $scope.disableChat = function () {
            vm.chatControl.status = 0;
            ChatControl.update(vm.chatControl, ChatonSuccess, ChatonError);

        }
        $scope.enableChat = function () {
            vm.chatControl.status = 1;
            ChatControl.update(vm.chatControl, ChatonSuccess, ChatonError);

        }

        $scope.updateChat = function () {
            ChatControl.update(vm.chatControl, ChatonSuccess, ChatonError);

        }

        function ChatonSuccess(result) {
            vm.chatControl = ChatControl.get({ id: 1 })

        }

        function ChatonError() { }

        vm.chatControl = ChatControl.get({ id: 1 })


        /* About Us part */

        $scope.showAboutConfirm = function () {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Votre Modfication à été publié avec succés')
                .ariaLabel('yeah')
                .ok('Okay')

            $mdDialog.show(confirm).then(function () {
                $scope.status = '';
            }, function () {
                $scope.status = '';
            });
        };
        $scope.enAbout = About.get({ id: 17 }, function (res) {

        });
        $scope.arAbout = About.get({ id: 18 }, function (res) {

        });
        $scope.frAbout = About.get({ id: 19 }, function (res) {

        });
        vm.about = {};
        $scope.saveAbout = function () {
            About.update($scope.enAbout);
            About.update($scope.arAbout);
            About.update($scope.frAbout);
            $scope.showAboutConfirm()

        };


        $scope.descriptions = SiteDescription.query();

        $scope.saveDescription = function (description) {
            SiteDescription.update(description)
            description.edit = undefined;
        }

        $scope.editDescription = function (description) {
            description.edit = 1;
        }
        /* Slider part */
        vm.saveSlider = saveSlider;
        vm.slider = {};
        $scope.myCroppedSlideImage = '';
        $scope.mySlideImage = '';
        $scope.allSliders = Slider.query();


        $scope.$watch('myCroppedSlideImage', function () {

            if ($scope.myCroppedSlideImage != '') {
                vm.slider.file = $scope.myCroppedSlideImage.split(',')[1];
            }
        });

        var handleFileSelect = function (evt) {
            var file = evt.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function ($scope) {
                    $scope.mySlideImage = evt.target.result;


                });
            };
            reader.readAsDataURL(file);


        };


        angular.element(document.querySelector('#fileInput4')).on('change', handleFileSelect);

        function saveSlider() {
            $scope.iscompleted = true

            vm.isSaving = true;

            Slider.save(vm.slider, onSaveSliderSuccess, onSaveSliderError);

        }

        function onSaveSliderSuccess(result) {
            $scope.$emit('mawjoudin2App:sliderUpdate', result);
            vm.isSaving = false;
            $scope.iscompleted = false
            $scope.allSliders.push(result)
            vm.slider = {};
            $scope.myCroppedSlideImage = '';
        }

        function onSaveSliderError() {
            vm.isSaving = false;
            $scope.iscompleted = false
        }
        $scope.showDelete = function (ev, slider) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Effacer Ce Slider . ')
                .textContent('Êtes-vous sûr ?')
                .ariaLabel('Je Supprime!')
                .targetEvent(ev)
                .ok('Oui!')
                .cancel('Non');

            $mdDialog.show(confirm).then(function () {
                deleteSlider(slider)

            }, function () { });
        };

        function deleteSlider(slider) {
            var index=$scope.allSliders.indexOf(slider);
            $scope.allSliders.splice(index, 1);
            Slider.delete({ id: slider.id });
        }


        /*   Resources */
        vm.resource = {};
        vm.allResources = [];
        $scope.type = '';
        $scope.filePDF=[];
        $scope.fileDoc=[];
        $scope.IsPdf=false;
        $scope.Isdoc=false;
        vm.saveResource = saveResource;
        $scope.resource = function () {
            vm.choice = 12
            vm.allResources = Resource.query({

            }, function () {
                for (var i = 0; i < vm.allResources.length; i++) {
                    $scope.type = vm.allResources[i].filePath.split('.')[1];
                    if($scope.type==="pdf"){
                       $scope.IsPdf=true; 
                       $scope.filePDF.push($scope.IsPdf)
                        $scope.Isdoc=false;
                         $scope.fileDoc.push($scope.Isdoc)
                    }else if($scope.type!="pdf"){
                         $scope.Isdoc=true;
                         $scope.fileDoc.push($scope.Isdoc)
                          $scope.IsPdf=false; 
                           $scope.filePDF.push($scope.IsPdf)
                    }
                    
                }
            });


        }



        vm.setResource = function ($file, resource) {
            if ($file) {
                DataUtils.toBase64($file, function (base64Data) {
                    $scope.$apply(function () {
                        resource.file = base64Data;
                        resource.type = $file.type;
                        resource.name = $file.name;
                    });


                })
            }
        }
         $scope.showDeleteResource = function (ev, resource) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Effacer cette ressource? . ')
                .textContent('Êtes-vous sûr ?')
                .ariaLabel('Je Supprime!')
                .targetEvent(ev)
                .ok('Oui!')
                .cancel('Non');

            $mdDialog.show(confirm).then(function () {
                deleteResource(resource)

            }, function () { });
        };

        function deleteResource(resource) {
            var disResource = vm.allResources.indexOf(resource)
            vm.allResources.splice(disResource, 1);
            Resource.delete({ id: resource.id });
        }



        function saveResource() {
            vm.isSaving = true;
            Resource.save(vm.resource, onSaveResourceSuccess, onSaveResourceError);

        }


        function onSaveResourceSuccess(result) {
            $scope.$emit('mawjoudin2App:resourceUpdate', result);
            vm.allResources.push(result);
            vm.isSaving = false;
            vm.resource = {};
        }

        function onSaveResourceError() {
            vm.isSaving = false;
        }

    }

})();