(function() {
    'use strict';

    angular
        .module('mawjoudin2App')


    .controller('eventdialogdetailController', eventdialogdetailController);

    eventdialogdetailController.$inject = ['Agenda', 'calendarConfig', 'moment', '$mdDialog', '$q', 'DataUtils', '$scope', 'Principal', 'items'];

    function eventdialogdetailController(Agenda, calendarConfig, moment, $mdDialog, $q, DataUtils, $scope, Principal, items) {
        var vm = this;
        vm.account = null;
        $scope.items = items;

        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        getAccount();
        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }
    };
})();