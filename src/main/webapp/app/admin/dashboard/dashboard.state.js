(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('dashboard', {
                parent: 'admin',
                url: '/dashboard',
                data: {
                    authorities: ['ROLE_ADMIN'],

                    pageTitle: 'admin dasboard'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/admin/dashboard/dashboard.html',
                        controller: 'dashboardController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('dashboard');
                        return $translate.refresh();
                    }]
                }
            }),
            $stateProvider.state('dashboard2', {
                parent: 'admin',
                url: '/dashboardCalender',
                data: {
                    authorities: ['ROLE_ADMIN'],

                    pageTitle: 'admin dasboard'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/admin/dashboard/test.html',
                        controller: 'dashboardController',
                        controllerAs: 'vm'
                    }
                },
               resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('dashboard');

                        return $translate.refresh();
                    }]
                }
            });
    }
})();