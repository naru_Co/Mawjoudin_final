(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .controller('UserManagementDetailController', UserManagementDetailController);

    UserManagementDetailController.$inject = ['$stateParams', 'User'];

    function UserManagementDetailController($stateParams, User) {
        var vm = this;

        vm.load = load;
        vm.setActive=setActive;
        vm.user = {};

        vm.load($stateParams.login);
        function setActive (isActivated) {
            vm.user.activated = isActivated;
            User.update(vm.user, function () {
            });
        }

        function load(login) {
            User.get({login: login}, function(result) {
                vm.user = result;
                vm.name=result.firstName+' '+result.lastName;
                vm.birthdate=new Date(vm.user.profile.birthdate);
            });
        }
    }
})();
