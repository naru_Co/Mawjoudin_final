(function() {
    'use strict';

    angular
        .module('mawjoudin2App')
        .factory('Password', Password)
        .factory('VerifyPassword',VerifyPassword);
        VerifyPassword.$inject = ['$resource'];

    function VerifyPassword($resource) {
        var service = $resource('api/account/verification', {}, {});

        return service;
    }

    Password.$inject = ['$resource'];

    function Password($resource) {
        var service = $resource('api/account/change_password', {}, {});

        return service;
    }
})();
