package  org.naru.mawjoudin.web.websocket;

import org.naru.mawjoudin.security.SecurityUtils;
import org.naru.mawjoudin.service.AdvancedEncryptionStandard;
import org.naru.mawjoudin.web.websocket.dto.MessageDTO;
import org.naru.mawjoudin.repository.MessageRepository;
import org.naru.mawjoudin.domain.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.handler.annotation.DestinationVariable;

import org.springframework.stereotype.Controller;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.security.Principal;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static org.naru.mawjoudin.config.WebsocketConfiguration.IP_ADDRESS;

@Controller
public class ChatService implements ApplicationListener<SessionDisconnectEvent> {

    private static final Logger log = LoggerFactory.getLogger(ChatService.class);

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private final SimpMessageSendingOperations messagingTemplate;
        private final MessageRepository messageRepository;


    public ChatService(SimpMessageSendingOperations messagingTemplate,MessageRepository messageRepository) {
        this.messagingTemplate = messagingTemplate;
                this.messageRepository=messageRepository;

    }


     @SubscribeMapping("/privatechat/private/{id1}/{id2}")
    public void subscribeToPrivate(@DestinationVariable String id1, @DestinationVariable String id2,StompHeaderAccessor stompHeaderAccessor, Principal principal) {
        String login = SecurityUtils.getCurrentUserLogin();
        String ipAddress = stompHeaderAccessor.getSessionAttributes().get(IP_ADDRESS).toString();
        log.debug("User {} subscribed to private Chat from IP {}", login, ipAddress);
       
    }


     @MessageMapping("/privatechat/{id1}/{id2}")
    @SendTo("/privatechat/private/{id1}/{id2}")
    public Message sendPrivateChat(@Payload Message message,@DestinationVariable String id1, @DestinationVariable String id2, StompHeaderAccessor stompHeaderAccessor, Principal principal) {
        log.debug("User sent a private message ");

        return setupMessageDTO2(message, stompHeaderAccessor, principal);
    }




    @SubscribeMapping("/chat/public")
    public void subscribe(@Payload StompHeaderAccessor stompHeaderAccessor, Principal principal) {
        String login = principal.getName();

        String ipAddress = stompHeaderAccessor.getSessionAttributes().get(IP_ADDRESS).toString();
        log.debug("User {} subscribed to Chat from IP {}", login, ipAddress);
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setUserLogin("System");
        messageDTO.setTime(dateTimeFormatter.format(ZonedDateTime.now()));
        messageDTO.setMessage(login + " a rejoint la discussion");
        messagingTemplate.convertAndSend("/chat/public", messageDTO);
    }

    @MessageMapping("/chat")
    @SendTo("/chat/public")
    public MessageDTO sendChat(@Payload MessageDTO messageDTO, StompHeaderAccessor stompHeaderAccessor, Principal principal) {
        messageDTO.setUserLogin(principal.getName());
        return setupMessageDTO(messageDTO, stompHeaderAccessor, principal);
    }

    @Override
    public void onApplicationEvent(SessionDisconnectEvent event) {
        // when the user disconnects, send a message saying that hey are leaving
        log.info("{} disconnected from the chat websockets", event.getUser().getName());
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setUserLogin("System");
        messageDTO.setTime(dateTimeFormatter.format(ZonedDateTime.now()));
        messageDTO.setMessage(event.getUser().getName() + " a quitté la discussion");
        messagingTemplate.convertAndSend("/chat/public", messageDTO);
    }

    private MessageDTO setupMessageDTO (MessageDTO messageDTO, StompHeaderAccessor stompHeaderAccessor, Principal principal) {
        messageDTO.setTime(dateTimeFormatter.format(ZonedDateTime.now()));
        log.debug("Sending user chat data {}", messageDTO);
        return messageDTO;
    }

        private Message setupMessageDTO2 (Message message, StompHeaderAccessor stompHeaderAccessor, Principal principal) {

        message.setCreationDate(ZonedDateTime.now());
        log.debug("Sending and saving user chat data {}", message);
        Message encMessages =message;
                        String secretKey= "544522881553881187753000540543603";
        String originalString = message.getContent();
        String encryptedString = AdvancedEncryptionStandard.encrypt(originalString, secretKey) ;
        encMessages.setContent(encryptedString);
        messageRepository.save(encMessages);
        //trying to send a notification
        System.out.println(message);
        message.setContent(AdvancedEncryptionStandard.decrypt(encryptedString, secretKey));
        messagingTemplate.convertAndSend("/notification/notif/"+message.getReceiver().getId(), message);

        return message;
    }
}
