package org.naru.mawjoudin.web.websocket;

import org.naru.mawjoudin.security.SecurityUtils;
import org.naru.mawjoudin.web.websocket.dto.MessageDTO;
import org.naru.mawjoudin.domain.Message;
import org.naru.mawjoudin.domain.Profile;
import org.naru.mawjoudin.repository.MessageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.messaging.handler.annotation.DestinationVariable;

import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.security.Principal;
import java.util.Calendar;

import static org.naru.mawjoudin.config.WebsocketConfiguration.IP_ADDRESS;

@Controller
public class NotificationService implements ApplicationListener<SessionDisconnectEvent> {

    private static final Logger log = LoggerFactory.getLogger(NotificationService.class);

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private final SimpMessageSendingOperations messagingTemplate;
    private final MessageRepository messageRepository;

    public NotificationService(SimpMessageSendingOperations messagingTemplate,MessageRepository messageRepository) {
        this.messagingTemplate = messagingTemplate;
        this.messageRepository=messageRepository;
    }
/*
         @SubscribeMapping("notification/notif/{id}")
    public void subscribeToNotif(@DestinationVariable String id,StompHeaderAccessor stompHeaderAccessor, Principal principal) {
        String login = SecurityUtils.getCurrentUserLogin();
        String ipAddress = stompHeaderAccessor.getSessionAttributes().get(IP_ADDRESS).toString();
        log.debug("User {} subscribed to private Chat from IP {}", login, ipAddress);
       
    }
 @MessageMapping("/notification/{id}")*/
    @SendTo("/notification/notif/{id}")
    public Message sendNotif(@Payload Message message,@DestinationVariable String id,StompHeaderAccessor stompHeaderAccessor, Principal principal) {
        log.debug("User sent a notification message ");

        return message;
    }

    @Override
    public void onApplicationEvent(SessionDisconnectEvent event) {
        // when the user disconnects, send a message saying that hey are leaving
        log.info("{} disconnected from the chat websockets", event.getUser().getName());
      
    }


}
