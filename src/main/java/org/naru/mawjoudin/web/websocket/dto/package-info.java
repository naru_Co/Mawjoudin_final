/**
 * Data Access Objects used by WebSocket services.
 */
package org.naru.mawjoudin.web.websocket.dto;
