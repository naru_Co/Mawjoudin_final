package org.naru.mawjoudin.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.naru.mawjoudin.domain.Publication;
import org.naru.mawjoudin.domain.PublicationImage;
import org.naru.mawjoudin.domain.Comment;
import org.naru.mawjoudin.domain.Image;
import org.naru.mawjoudin.domain.ImageGallery;
import org.naru.mawjoudin.domain.Profile;
import org.naru.mawjoudin.service.ImageUploadService;
import org.naru.mawjoudin.repository.CommentRepository;
import org.naru.mawjoudin.repository.ImageGalleryRepository;
import org.naru.mawjoudin.repository.ImageRepository;
import org.naru.mawjoudin.repository.PublicationRepository;
import org.naru.mawjoudin.web.rest.util.HeaderUtil;
import org.naru.mawjoudin.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


/**
 * REST controller for managing Publication.
 */
@RestController
@RequestMapping("/api")
public class PublicationResource {

    private final Logger log = LoggerFactory.getLogger(PublicationResource.class);

    private static final String ENTITY_NAME = "publication";

    private final PublicationRepository publicationRepository;


    private final ImageUploadService imageUploadService;

    private final ImageGalleryRepository imageGalleryRepository;

    private final ImageRepository imageRepository;

    private final CommentRepository commentRepository;

    public PublicationResource(PublicationRepository publicationRepository,ImageUploadService imageUploadService,ImageGalleryRepository imageGalleryRepository,ImageRepository imageRepository,CommentRepository commentRepository) {
        this.publicationRepository = publicationRepository;
        this.imageUploadService= imageUploadService;
        this.imageGalleryRepository = imageGalleryRepository;
        this.imageRepository=imageRepository;
        this.commentRepository=commentRepository;

    }

    /**
     * POST  /publications : Create a new publication.
     *
     * @param publication the publication to create
     * @return the ResponseEntity with status 201 (Created) and with body the new publication, or with status 400 (Bad Request) if the publication has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/publications")
    @Timed
    public ResponseEntity<Publication> createPublication(@RequestBody PublicationImage publicationImage) throws URISyntaxException {
        log.debug("REST request to save Publication : {}", publicationImage.getPublication());
        Publication publication = new Publication();
        Image image = new Image();
        Profile profile=publicationImage.getPublication().getProfile();
        String desc = publicationImage.getPublication().getContent();
        ImageGallery gallery = imageGalleryRepository.findOneByProfile_idAndType(profile.getId(), 1);
        if (publicationImage.getFile() == null) {
            
        } else{
      Image res =imageUploadService.imageUploadPublication(publicationImage.getFile(), image);  
      publication.setImage(res);  

      gallery.setDescription("Images Publiées");
      imageGalleryRepository.save(gallery);
       Image affectTogallery =  imageRepository.findOne(res.getId());
         affectTogallery.setImageGallery(gallery);
         imageRepository.save(affectTogallery);
    }

    Publication pubimg = publicationImage.getPublication();
    publication.setProfile(pubimg.getProfile());
    publication.setContent(pubimg.getContent());
    publication.setCreationDate(ZonedDateTime.now());

        Publication result = publicationRepository.save(publication);
        return ResponseEntity.created(new URI("/api/publications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /publications : Updates an existing publication.
     *
     * @param publication the publication to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated publication,
     * or with status 400 (Bad Request) if the publication is not valid,
     * or with status 500 (Internal Server Error) if the publication couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/publications")
    @Timed
    public ResponseEntity<Publication> updatePublication(@RequestBody Publication publication) throws URISyntaxException {
        log.debug("REST request to update Publication : {}", publication);
        if (publication.getId() == null) {
            PublicationImage publicationImage = new PublicationImage();
            return createPublication(publicationImage);
        }
        Publication result = publicationRepository.save(publication);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, publication.getId().toString()))
            .body(result);
    }

    /**
     * GET  /publications : get all the publications.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of publications in body
     */
    @GetMapping("/publications")
    @Timed
    public ResponseEntity<List<Publication>> getAllPublications(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Publications");
        Page<Publication> page = publicationRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/publications");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
 /**
     * GET  /publicationsbyprofil/:profil_id : get the "id" of the profil.
     *
     * @param id the id of the profil to retrieve all his publications
     * @return the ResponseEntity with status 200 (OK) and with body the publication, or with status 404 (Not Found)
     */
    @GetMapping("/publicationsbyprofil/{profil_id}")
    @Timed
    public ResponseEntity<List<Publication>> getAllPublications(@PathVariable Long profil_id,@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Publications");
        Page<Publication> page = publicationRepository.findAllByProfile_id(profil_id,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/publicationsbyprofil/{profil_id}");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    @GetMapping("/publicationsbyprofilandtime/{profil_id}")
    @Timed
    public List<Publication> getAllPublicationsByProfileAndTime(@PathVariable Long profil_id,@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Publications");
        ZonedDateTime yesterday = ZonedDateTime.now().minusDays(1);
        List<Publication> publications = publicationRepository.findAllByProfile_idAndCreationDateAfter(profil_id,yesterday);
        return publications;
    }
   
   
   
   
    /**
     * GET  /publications/:id : get the "id" publication.
     *
     * @param id the id of the publication to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the publication, or with status 404 (Not Found)
     */
    @GetMapping("/publications/{id}")
    @Timed
    public ResponseEntity<Publication> getPublication(@PathVariable Long id) {
        log.debug("REST request to get Publication : {}", id);
        Publication publication = publicationRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(publication));
    }

    /**
     * DELETE  /publications/:id : delete the "id" publication.
     *
     * @param id the id of the publication to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/publications/{id}")
    @Timed
    public ResponseEntity<Void> deletePublication(@PathVariable Long id) {
        log.debug("REST request to delete Publication : {}", id);
        List<Comment> comments= commentRepository.findAllByPublication_id(id);
        for(Comment comment : comments){
            commentRepository.delete(comment.getId());
        }
        publicationRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
