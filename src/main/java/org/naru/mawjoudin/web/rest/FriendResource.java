package org.naru.mawjoudin.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.google.common.util.concurrent.Service.State;

import org.naru.mawjoudin.domain.Friend;
import org.naru.mawjoudin.domain.Profile;
import org.naru.mawjoudin.repository.FriendRepository;
import org.naru.mawjoudin.web.rest.util.HeaderUtil;
import org.naru.mawjoudin.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


/**
 * REST controller for managing Friend.
 */
@RestController
@RequestMapping("/api")
public class FriendResource {

    private final Logger log = LoggerFactory.getLogger(FriendResource.class);

    private static final String ENTITY_NAME = "friend";

    private final FriendRepository friendRepository;

    public FriendResource(FriendRepository friendRepository) {
        this.friendRepository = friendRepository;
    }

    /**
     * POST  /friends : Create a new friend.
     *
     * @param friend the friend to create
     * @return the ResponseEntity with status 201 (Created) and with body the new friend, or with status 400 (Bad Request) if the friend has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/friends")
    @Timed
    public ResponseEntity<Friend> createFriend(@RequestBody Friend friend) throws URISyntaxException {
        log.debug("REST request to save Friend : {}", friend);
        if (friend.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new friend cannot already have an ID")).body(null);
        }
        Long state = (long) 0;
        friend.setState(state);
        friend.setCreationDate(ZonedDateTime.now());
        Friend result = friendRepository.save(friend);
        return ResponseEntity.created(new URI("/api/friends/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /friends : Updates an existing friend.
     *
     * @param friend the friend to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated friend,
     * or with status 400 (Bad Request) if the friend is not valid,
     * or with status 500 (Internal Server Error) if the friend couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/friends")
    @Timed
    public ResponseEntity<Friend> updateFriend(@RequestBody Friend friend) throws URISyntaxException {
        log.debug("REST request to update Friend : {}", friend);
        if (friend.getId() == null) {
            return createFriend(friend);
        }
        Friend result = friendRepository.save(friend);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, friend.getId().toString()))
            .body(result);
    }

    /**
     * GET  /friends : get all the friends.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of friends in body
     */
    @GetMapping("/friends")
    @Timed
    public ResponseEntity<List<Friend>> getAllFriends(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Friends");
        Page<Friend> page = friendRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/friends");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
   /**
     * GET  /friendsbyreceiverAndState/:receiver_id/:state : get all the friends.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of friends in body
     */
    @GetMapping("/friendsbyreceiverAndState/{receiver_id}/{state}")
    @Timed
    public List<Profile> getAllProfileFriends(@PathVariable Long receiver_id,@PathVariable Long state) {
        log.debug("REST request to get a page of Friends");
        List<Friend> friends = friendRepository.findAllByReceiver_idOrSender_idAndState(receiver_id,receiver_id,state);
        List<Profile> friendsProfile =new ArrayList<Profile>();
        for (int i=0;i<friends.size();i++){
            if((friends.get(i).getReceiver().getId()).equals(receiver_id)){
                System.out.println("we are here");
            friendsProfile.add(friends.get(i).getSender());
            }else{
            friendsProfile.add(friends.get(i).getReceiver());

            }
        }
        return friendsProfile;
    }
    /**
     * GET  /friendsbyreceiverAndState/:receiver_id/:state : get all the friends.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of friends in body
     */
    @GetMapping("/invitationsbyreceiverAndState/{receiver_id}/{state}")
    @Timed
    public List<Friend> getAllRequests(@PathVariable Long receiver_id,@PathVariable Long state) {
        log.debug("REST request to get a page of Friends");
        List<Friend> friends = friendRepository.findAllByReceiver_idAndState(receiver_id,state);
        
        return friends;
    }
     /**
     * GET  /friendsbyreceiverAndState/:receiver_id/:state : get all the friends.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of friends in body
     */
    @GetMapping("/invitationsbysenderAndState/{sender_id}/{state}")
    @Timed
    public List<Friend> getAllRequestsBySender(@PathVariable Long sender_id,@PathVariable Long state) {
        log.debug("REST request to get a page of Friends");
        List<Friend> friends = friendRepository.findAllBySender_idAndState(sender_id,state);
        
        return friends;
    }

    
    /**
     * GET  /friendshipbetweentow/:receiver_id/:sender_id : get the "id" friend.
     *
     * @param id the id of the friend to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the friend, or with status 404 (Not Found)
     */
    @GetMapping("/friendshipBetweenTwo/{receiver_id}/{sender_id}")
    @Timed
    public ResponseEntity<Friend> getFriendship(@PathVariable Long receiver_id,@PathVariable Long sender_id) {
        log.debug("REST request to get Friendship : {}");
        Friend friend = friendRepository.findOneByReceiver_idAndSender_id(receiver_id,sender_id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(friend));
    }

    /**
     * DELETE  /friends/:id : delete the "id" friend.
     *
     * @param id the id of the friend to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/friends/{id}")
    @Timed
    public ResponseEntity<Void> deleteFriend(@PathVariable Long id) {
        log.debug("REST request to delete Friend : {}", id);
        friendRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
