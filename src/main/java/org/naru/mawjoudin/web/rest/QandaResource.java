package org.naru.mawjoudin.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.naru.mawjoudin.domain.Qanda;

import org.naru.mawjoudin.repository.QandaRepository;
import org.naru.mawjoudin.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Qanda.
 */
@RestController
@RequestMapping("/api")
public class QandaResource {

    private final Logger log = LoggerFactory.getLogger(QandaResource.class);

    private static final String ENTITY_NAME = "qanda";

    private final QandaRepository qandaRepository;

    public QandaResource(QandaRepository qandaRepository) {
        this.qandaRepository = qandaRepository;
    }

    /**
     * POST  /qandas : Create a new qanda.
     *
     * @param qanda the qanda to create
     * @return the ResponseEntity with status 201 (Created) and with body the new qanda, or with status 400 (Bad Request) if the qanda has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/qandas")
    @Timed
    public ResponseEntity<Qanda> createQanda(@RequestBody Qanda qanda) throws URISyntaxException {
        log.debug("REST request to save Qanda : {}", qanda);
        if (qanda.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new qanda cannot already have an ID")).body(null);
        }
        Qanda result = qandaRepository.save(qanda);
        return ResponseEntity.created(new URI("/api/qandas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /qandas : Updates an existing qanda.
     *
     * @param qanda the qanda to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated qanda,
     * or with status 400 (Bad Request) if the qanda is not valid,
     * or with status 500 (Internal Server Error) if the qanda couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/qandas")
    @Timed
    public ResponseEntity<Qanda> updateQanda(@RequestBody Qanda qanda) throws URISyntaxException {
        log.debug("REST request to update Qanda : {}", qanda);
        if (qanda.getId() == null) {
            return createQanda(qanda);
        }
        Qanda result = qandaRepository.save(qanda);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, qanda.getId().toString()))
            .body(result);
    }

    /**
     * GET  /qandas : get all the qandas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of qandas in body
     */
    @GetMapping("/qandas")
    @Timed
    public List<Qanda> getAllQandas() {
        log.debug("REST request to get all Qandas");
        return qandaRepository.findAll();
    }

    /**
     * GET  /qandas/:id : get the "id" qanda.
     *
     * @param id the id of the qanda to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the qanda, or with status 404 (Not Found)
     */
    @GetMapping("/qandas/{id}")
    @Timed
    public ResponseEntity<Qanda> getQanda(@PathVariable Long id) {
        log.debug("REST request to get Qanda : {}", id);
        Qanda qanda = qandaRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(qanda));
    }

    /**
     * DELETE  /qandas/:id : delete the "id" qanda.
     *
     * @param id the id of the qanda to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/qandas/{id}")
    @Timed
    public ResponseEntity<Void> deleteQanda(@PathVariable Long id) {
        log.debug("REST request to delete Qanda : {}", id);
        qandaRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
