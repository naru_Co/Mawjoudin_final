package org.naru.mawjoudin.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.naru.mawjoudin.domain.SiteDescription;

import org.naru.mawjoudin.repository.SiteDescriptionRepository;
import org.naru.mawjoudin.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SiteDescription.
 */
@RestController
@RequestMapping("/api")
public class SiteDescriptionResource {

    private final Logger log = LoggerFactory.getLogger(SiteDescriptionResource.class);

    private static final String ENTITY_NAME = "siteDescription";

    private final SiteDescriptionRepository siteDescriptionRepository;

    public SiteDescriptionResource(SiteDescriptionRepository siteDescriptionRepository) {
        this.siteDescriptionRepository = siteDescriptionRepository;
    }

    /**
     * POST  /site-descriptions : Create a new siteDescription.
     *
     * @param siteDescription the siteDescription to create
     * @return the ResponseEntity with status 201 (Created) and with body the new siteDescription, or with status 400 (Bad Request) if the siteDescription has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/site-descriptions")
    @Timed
    public ResponseEntity<SiteDescription> createSiteDescription(@RequestBody SiteDescription siteDescription) throws URISyntaxException {
        log.debug("REST request to save SiteDescription : {}", siteDescription);
        if (siteDescription.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new siteDescription cannot already have an ID")).body(null);
        }
        SiteDescription result = siteDescriptionRepository.save(siteDescription);
        return ResponseEntity.created(new URI("/api/site-descriptions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /site-descriptions : Updates an existing siteDescription.
     *
     * @param siteDescription the siteDescription to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated siteDescription,
     * or with status 400 (Bad Request) if the siteDescription is not valid,
     * or with status 500 (Internal Server Error) if the siteDescription couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/site-descriptions")
    @Timed
    public ResponseEntity<SiteDescription> updateSiteDescription(@RequestBody SiteDescription siteDescription) throws URISyntaxException {
        log.debug("REST request to update SiteDescription : {}", siteDescription);
        if (siteDescription.getId() == null) {
            return createSiteDescription(siteDescription);
        }
        SiteDescription result = siteDescriptionRepository.save(siteDescription);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, siteDescription.getId().toString()))
            .body(result);
    }

    /**
     * GET  /site-descriptions : get all the siteDescriptions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of siteDescriptions in body
     */
    @GetMapping("/site-descriptions")
    @Timed
    public List<SiteDescription> getAllSiteDescriptions() {
        log.debug("REST request to get all SiteDescriptions");
        return siteDescriptionRepository.findAll();
    }

    /**
     * GET  /site-descriptions/:id : get the "id" siteDescription.
     *
     * @param id the id of the siteDescription to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the siteDescription, or with status 404 (Not Found)
     */
    @GetMapping("/site-descriptions/{id}")
    @Timed
    public ResponseEntity<SiteDescription> getSiteDescription(@PathVariable Long id) {
        log.debug("REST request to get SiteDescription : {}", id);
        SiteDescription siteDescription = siteDescriptionRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(siteDescription));
    }

    /**
     * DELETE  /site-descriptions/:id : delete the "id" siteDescription.
     *
     * @param id the id of the siteDescription to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/site-descriptions/{id}")
    @Timed
    public ResponseEntity<Void> deleteSiteDescription(@PathVariable Long id) {
        log.debug("REST request to delete SiteDescription : {}", id);
        siteDescriptionRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
