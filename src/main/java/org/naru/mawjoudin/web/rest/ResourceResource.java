package org.naru.mawjoudin.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.naru.mawjoudin.domain.Resource;
import org.naru.mawjoudin.domain.ResourceFile;
import org.naru.mawjoudin.repository.ResourceRepository;
import org.naru.mawjoudin.web.rest.util.HeaderUtil;
import org.naru.mawjoudin.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.*;
import java.net.URISyntaxException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import java.io.IOException;

import com.codahale.metrics.annotation.Timed;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import javax.servlet.*;
import javax.servlet.http.*;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import java.util.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Resource.
 */
@RestController
@RequestMapping("/api")
public class ResourceResource {

    private final Logger log = LoggerFactory.getLogger(ResourceResource.class);

    private static final String ENTITY_NAME = "resource";

    private final ResourceRepository resourceRepository;
    @Autowired
    private HttpServletRequest request;

    public ResourceResource(ResourceRepository resourceRepository) {
        this.resourceRepository = resourceRepository;
    }

    /**
     * POST  /resources : Create a new resource.
     *
     * @param resource the resource to create
     * @return the ResponseEntity with status 201 (Created) and with body the new resource, or with status 400 (Bad Request) if the resource has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/resources")
    @Timed
    public ResponseEntity<Resource> createResource(@RequestBody ResourceFile resourceFile) throws URISyntaxException {
        log.debug("REST request to save Resource : {}", resourceFile.getResource());
        Resource resource = new Resource();
        if (resourceFile.getResource().getId() != null) {
            return ResponseEntity.badRequest().headers(
                    HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new resource cannot already have an ID"))
                    .body(null);
        }
        byte[] document = resourceFile.getFile();
        String uploadsDir = "/pdfresources/";
        String orgName = "";
        String realPathtoUpload = request.getServletContext().getRealPath(uploadsDir);

        if (!new File(realPathtoUpload).exists()) {
            new File(realPathtoUpload).mkdir();
        }
        if (resourceFile.getType().equals("application/msword")) {
            orgName = (new SimpleDateFormat("yyyyMMddHHmmssSSSSSS").format(new Date()))
                    + resourceFile.getResource().getTitle() + ".doc";
        } else if (resourceFile.getType().equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
            orgName = (new SimpleDateFormat("yyyyMMddHHmmssSSSSSS").format(new Date()))
                    + resourceFile.getResource().getTitle() + ".docx";
        } else if(resourceFile.getType().equals("application/pdf")){
              orgName = (new SimpleDateFormat("yyyyMMddHHmmssSSSSSS").format(new Date()))
                    + resourceFile.getResource().getTitle() + ".pdf";

        }
        String filePath = realPathtoUpload + "/" + orgName;
        File dest = new File(filePath);
        resource.setFilePath(uploadsDir + orgName);
        try {
            new FileOutputStream(dest).write(document);
        } catch (Exception e) {
            System.out.println(e);
        }
        Resource rsc = resourceFile.getResource();
        resource.setTitle(rsc.getTitle());
        resource.setDescription(rsc.getDescription());

        Resource result = resourceRepository.save(resource);
        return ResponseEntity.created(new URI("/api/resources/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT  /resources : Updates an existing resource.
     *
     * @param resource the resource to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated resource,
     * or with status 400 (Bad Request) if the resource is not valid,
     * or with status 500 (Internal Server Error) if the resource couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/resources")
    @Timed
    public ResponseEntity<Resource> updateResource(@RequestBody Resource resource) throws URISyntaxException {
        log.debug("REST request to update Resource : {}", resource);
        if (resource.getId() == null) {
            ResourceFile rsf = new ResourceFile();
            return createResource(rsf);
        }
        Resource result = resourceRepository.save(resource);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, resource.getId().toString()))
                .body(result);
    }

    /**
     * GET  /resources : get all the resources.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of resources in body
     */
    @GetMapping("/resources")
    @Timed
    public ResponseEntity<List<Resource>> getAllResources(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Resources");
        Page<Resource> page = resourceRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/resources");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /resources/:id : get the "id" resource.
     *
     * @param id the id of the resource to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the resource, or with status 404 (Not Found)
     */
    @GetMapping("/resources/{id}")
    @Timed
    public ResponseEntity<Resource> getResource(@PathVariable Long id) {
        log.debug("REST request to get Resource : {}", id);
        Resource resource = resourceRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(resource));
    }

       @GetMapping("/resourcesbydesc/{desc}")
    @Timed
    public List<Resource> getAllResourcesByDesc(@PathVariable String desc){
        List<Resource> resources = resourceRepository.findAllByDescriptionContainingIgnoreCaseOrTitleContainingIgnoreCase(desc,desc);
        return resources;
    }

    /**
     * DELETE  /resources/:id : delete the "id" resource.
     *
     * @param id the id of the resource to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/resources/{id}")
    @Timed
    public ResponseEntity<Void> deleteResource(@PathVariable Long id) {
        log.debug("REST request to delete Resource : {}", id);
        resourceRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
