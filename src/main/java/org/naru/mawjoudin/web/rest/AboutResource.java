package org.naru.mawjoudin.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.naru.mawjoudin.domain.About;

import org.naru.mawjoudin.repository.AboutRepository;
import org.naru.mawjoudin.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing About.
 */
@RestController
@RequestMapping("/api")
public class AboutResource {

    private final Logger log = LoggerFactory.getLogger(AboutResource.class);

    private static final String ENTITY_NAME = "about";

    private final AboutRepository aboutRepository;

    public AboutResource(AboutRepository aboutRepository) {
        this.aboutRepository = aboutRepository;
    }

    /**
     * POST  /abouts : Create a new about.
     *
     * @param about the about to create
     * @return the ResponseEntity with status 201 (Created) and with body the new about, or with status 400 (Bad Request) if the about has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/abouts")
    @Timed
    public ResponseEntity<About> createAbout(@RequestBody About about) throws URISyntaxException {
        if (about.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new about cannot already have an ID")).body(null);
        }
        About result = aboutRepository.save(about);
        return ResponseEntity.created(new URI("/api/abouts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /abouts : Updates an existing about.
     *
     * @param about the about to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated about,
     * or with status 400 (Bad Request) if the about is not valid,
     * or with status 500 (Internal Server Error) if the about couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/abouts")
    @Timed
    public ResponseEntity<About> updateAbout(@RequestBody About about) throws URISyntaxException {
        if (about.getId() == null) {
            return createAbout(about);
        }
        About result = aboutRepository.save(about);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, about.getId().toString()))
            .body(result);
    }

    /**
     * GET  /abouts : get all the abouts.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of abouts in body
     */
    @GetMapping("/abouts")
    @Timed
    public List<About> getAllAbouts() {
        return aboutRepository.findAll();
    }

    /**
     * GET  /abouts/:id : get the "id" about.
     *
     * @param id the id of the about to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the about, or with status 404 (Not Found)
     */
    @GetMapping("/abouts/{id}")
    @Timed
    public ResponseEntity<About> getAbout(@PathVariable Long id) {
        About about = aboutRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(about));
    }

    /**
     * DELETE  /abouts/:id : delete the "id" about.
     *
     * @param id the id of the about to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/abouts/{id}")
    @Timed
    public ResponseEntity<Void> deleteAbout(@PathVariable Long id) {
        aboutRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
