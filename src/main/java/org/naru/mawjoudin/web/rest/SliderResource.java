package org.naru.mawjoudin.web.rest;

import com.codahale.metrics.annotation.Timed;

import org.naru.mawjoudin.domain.Image;
import org.naru.mawjoudin.domain.Slider;
import org.naru.mawjoudin.domain.SliderImage;
import org.naru.mawjoudin.repository.SliderRepository;
import org.naru.mawjoudin.service.ImageUploadService;
import org.naru.mawjoudin.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Slider.
 */
@RestController
@RequestMapping("/api")
public class SliderResource {

    private final Logger log = LoggerFactory.getLogger(SliderResource.class);

    private static final String ENTITY_NAME = "slider";

    private final SliderRepository sliderRepository;

    private final ImageUploadService imageUploadService;

    public SliderResource(SliderRepository sliderRepository,ImageUploadService imageUploadService) {
        this.sliderRepository = sliderRepository;
        this.imageUploadService = imageUploadService;
    }

    /**
     * POST  /sliders : Create a new slider.
     *
     * @param slider the slider to create
     * @return the ResponseEntity with status 201 (Created) and with body the new slider, or with status 400 (Bad Request) if the slider has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sliders")
    @Timed
    public ResponseEntity<Slider> createSlider(@RequestBody SliderImage sliderImage) throws URISyntaxException {
        log.debug("REST request to save Slider : {}", sliderImage.getSlider());
        if (sliderImage.getSlider().getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new slider cannot already have an ID")).body(null);
        }
        Slider slider = new Slider();
        Image image = new Image();
        if (sliderImage.getFile() == null) {
            
        } else {
           Image res= imageUploadService.imageUploadSlider(sliderImage.getFile(), image);
            slider.setImage(res);

        }
        
        Slider sldimg = sliderImage.getSlider();
        slider.setHeadlline(sldimg.getHeadlline());
        slider.setHeadlineFr(sldimg.getHeadlineFr());  
        slider.setHeadlineAr(sldimg.getHeadlineAr());
        slider.setSimpleText(sldimg.getSimpleText());   
        slider.setSimpleTextFr(sldimg.getSimpleTextFr());
        slider.setSimpleTextAr(sldimg.getSimpleTextAr());
        Slider result = sliderRepository.save(slider);
        return ResponseEntity.created(new URI("/api/sliders/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sliders : Updates an existing slider.
     *
     * @param slider the slider to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated slider,
     * or with status 400 (Bad Request) if the slider is not valid,
     * or with status 500 (Internal Server Error) if the slider couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sliders")
    @Timed
    public ResponseEntity<Slider> updateSlider(@RequestBody SliderImage sliderImage) throws URISyntaxException {
        log.debug("REST request to update Slider : {}", sliderImage.getSlider());
        if (sliderImage.getSlider().getId() == null) {
            return createSlider(sliderImage);
        }
        Image image = new Image();
        Slider slider = sliderImage.getSlider();
        if (sliderImage.getFile() == null) {
            
        } else {
           Image res= imageUploadService.imageUploadBlog(sliderImage.getFile(), image);
            slider.setImage(res);

        }
        Slider result = sliderRepository.save(slider);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, slider.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sliders : get all the sliders.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of sliders in body
     */
    @GetMapping("/sliders")
    @Timed
    public List<Slider> getAllSliders() {
        log.debug("REST request to get all Sliders");
        return sliderRepository.findAll();
    }

    /**
     * GET  /sliders/:id : get the "id" slider.
     *
     * @param id the id of the slider to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the slider, or with status 404 (Not Found)
     */
    @GetMapping("/sliders/{id}")
    @Timed
    public ResponseEntity<Slider> getSlider(@PathVariable Long id) {
        log.debug("REST request to get Slider : {}", id);
        Slider slider = sliderRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(slider));
    }

    /**
     * DELETE  /sliders/:id : delete the "id" slider.
     *
     * @param id the id of the slider to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sliders/{id}")
    @Timed
    public ResponseEntity<Void> deleteSlider(@PathVariable Long id) {
        log.debug("REST request to delete Slider : {}", id);
        sliderRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
