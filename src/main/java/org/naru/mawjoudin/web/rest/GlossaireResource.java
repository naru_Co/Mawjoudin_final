package org.naru.mawjoudin.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.naru.mawjoudin.domain.Glossaire;

import org.naru.mawjoudin.repository.GlossaireRepository;
import org.naru.mawjoudin.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


/**
 * REST controller for managing Glossaire.
 */
@RestController
@RequestMapping("/api")
public class GlossaireResource {

    private final Logger log = LoggerFactory.getLogger(GlossaireResource.class);

    private static final String ENTITY_NAME = "glossaire";

    private final GlossaireRepository glossaireRepository;

    public GlossaireResource(GlossaireRepository glossaireRepository) {
        this.glossaireRepository = glossaireRepository;
    }

    /**
     * POST  /glossaires : Create a new glossaire.
     *
     * @param glossaire the glossaire to create
     * @return the ResponseEntity with status 201 (Created) and with body the new glossaire, or with status 400 (Bad Request) if the glossaire has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/glossaires")
    @Timed
    public ResponseEntity<Glossaire> createGlossaire(@RequestBody Glossaire glossaire) throws URISyntaxException {
        log.debug("REST request to save Glossaire : {}", glossaire);
        if (glossaire.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new glossaire cannot already have an ID")).body(null);
        }
        Glossaire result = glossaireRepository.save(glossaire);
        return ResponseEntity.created(new URI("/api/glossaires/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /glossaires : Updates an existing glossaire.
     *
     * @param glossaire the glossaire to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated glossaire,
     * or with status 400 (Bad Request) if the glossaire is not valid,
     * or with status 500 (Internal Server Error) if the glossaire couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/glossaires")
    @Timed
    public ResponseEntity<Glossaire> updateGlossaire(@RequestBody Glossaire glossaire) throws URISyntaxException {
        log.debug("REST request to update Glossaire : {}", glossaire);
        if (glossaire.getId() == null) {
            return createGlossaire(glossaire);
        }
        Glossaire result = glossaireRepository.save(glossaire);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, glossaire.getId().toString()))
            .body(result);
    }

    /**
     * GET  /glossaires : get all the glossaires.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of glossaires in body
     */
    @GetMapping("/glossaires")
    @Timed
    public List<Glossaire> getAllGlossaires() {
        log.debug("REST request to get all Glossaires");
        return glossaireRepository.findAll();
    }

    /**
     * GET  /glossaires/:id : get the "id" glossaire.
     *
     * @param id the id of the glossaire to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the glossaire, or with status 404 (Not Found)
     */
    @GetMapping("/glossaires/{id}")
    @Timed
    public ResponseEntity<Glossaire> getGlossaire(@PathVariable Long id) {
        log.debug("REST request to get Glossaire : {}", id);
        Glossaire glossaire = glossaireRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(glossaire));
    }

    /**
     * DELETE  /glossaires/:id : delete the "id" glossaire.
     *
     * @param id the id of the glossaire to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/glossaires/{id}")
    @Timed
    public ResponseEntity<Void> deleteGlossaire(@PathVariable Long id) {
        log.debug("REST request to delete Glossaire : {}", id);
        glossaireRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
