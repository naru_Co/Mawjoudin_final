package org.naru.mawjoudin.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.naru.mawjoudin.domain.Blog;
import org.naru.mawjoudin.domain.BlogImage;
import org.naru.mawjoudin.domain.Image;
import org.naru.mawjoudin.service.ImageUploadService;

import org.naru.mawjoudin.repository.BlogRepository;
import org.naru.mawjoudin.web.rest.util.HeaderUtil;
import org.naru.mawjoudin.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


/**
 * REST controller for managing Blog.
 */
@RestController
@RequestMapping("/api")
public class BlogResource {

    private final Logger log = LoggerFactory.getLogger(BlogResource.class);

    private static final String ENTITY_NAME = "blog";

    private final BlogRepository blogRepository;

    private final ImageUploadService imageUploadService;

    public BlogResource(BlogRepository blogRepository,ImageUploadService imageUploadService) {
        this.blogRepository = blogRepository;
        this.imageUploadService = imageUploadService;
    }

    /**
     * POST  /blogs : Create a new blog.
     *
     * @param blog the blog to create
     * @return the ResponseEntity with status 201 (Created) and with body the new blog, or with status 400 (Bad Request) if the blog has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/blogs")
    @Timed
    public ResponseEntity<Blog> createBlog(@RequestBody BlogImage blogImage) throws URISyntaxException {
        log.debug("REST request to save Blog : {}", blogImage);
        Blog blog = new Blog();
        Image image = new Image();
        if (blogImage.getFile() == null) {
            
        } else {
           Image res= imageUploadService.imageUploadBlog(blogImage.getFile(), image);
            blog.setImage(res);

        }
        long state = (long) 0;
        Blog blogimg = blogImage.getBlog();
        blog.setTitle(blogimg.getTitle());
        blog.setProfile(blogimg.getProfile());
        blog.setContent(blogimg.getContent());
        blog.setCreationDate(ZonedDateTime.now());
        blog.setState(state);

        Blog result = blogRepository.save(blog);
        return ResponseEntity.created(new URI("/api/blogs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /blogs : Updates an existing blog.
     *
     * @param blog the blog to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated blog,
     * or with status 400 (Bad Request) if the blog is not valid,
     * or with status 500 (Internal Server Error) if the blog couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/blogs")
    @Timed
    public ResponseEntity<Blog> updateBlog(@RequestBody Blog blog) throws URISyntaxException {
        log.debug("REST request to update Blog : {}", blog);
        if (blog.getId() == null) {
            BlogImage blogImage= new BlogImage();
            return createBlog(blogImage);
        }
                blog.setCreationDate(ZonedDateTime.now());

        Blog result = blogRepository.save(blog);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, blog.getId().toString()))
            .body(result);
    }

    /**
     * GET  /blogs : get all the blogs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of blogs in body
     */
    @GetMapping("/blogs")
    @Timed
    public ResponseEntity<List<Blog>> getAllBlogs(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Blogs");
        Page<Blog> page = blogRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/blogs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
 /**
     * GET  /blogs : get all the blogs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of blogs in body
     */
    @GetMapping("/blogsbyProfilAndState/{profil_id}/{state}")
    @Timed
    public ResponseEntity<List<Blog>> getAllBlogsByProfileAndState(@PathVariable Long profil_id,@PathVariable Long state, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Blogs");
        Page<Blog> page = blogRepository.findAllByProfile_idAndState(profil_id,state,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/blogsbyProfilAndState");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

     @GetMapping("/blogsbyProfilAndStateAndTime/{profil_id}")
    @Timed
    public List<Blog> getAllBlogsByProfileAndStateAndTime(@PathVariable Long profil_id) {
        log.debug("REST request to get a page of Blogs");
                ZonedDateTime yesterday = ZonedDateTime.now().minusDays(1);
Long i = new Long(1);
        List<Blog> blogs = blogRepository.findAllByProfile_idAndCreationDateAfterAndState(profil_id,yesterday,i);
        return blogs;
    }

    @GetMapping("/blogsbyState/{state}")
    @Timed
    public List<Blog> getAllBlogsByState(@PathVariable Long state){
                log.debug("REST request to get Blogs By State");

        List<Blog> blogs = blogRepository.findAllByState(state);
        return blogs;
    }


    /**
     * GET  /blogs/:id : get the "id" blog.
     *
     * @param id the id of the blog to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the blog, or with status 404 (Not Found)
     */
    @GetMapping("/blogs/{id}")
    @Timed
    public ResponseEntity<Blog> getBlog(@PathVariable Long id) {
        log.debug("REST request to get Blog : {}", id);
        Blog blog = blogRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(blog));
    }

    /**
     * DELETE  /blogs/:id : delete the "id" blog.
     *
     * @param id the id of the blog to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/blogs/{id}")
    @Timed
    public ResponseEntity<Void> deleteBlog(@PathVariable Long id) {
        log.debug("REST request to delete Blog : {}", id);
        blogRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
