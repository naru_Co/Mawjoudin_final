package org.naru.mawjoudin.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.naru.mawjoudin.domain.Profile;
import org.naru.mawjoudin.domain.ProfileImage;
import org.naru.mawjoudin.domain.Image;
import org.naru.mawjoudin.domain.ImageGallery;
import org.naru.mawjoudin.service.ImageUploadService;
import org.naru.mawjoudin.repository.ImageGalleryRepository;
import org.naru.mawjoudin.repository.ImageRepository;
import org.naru.mawjoudin.repository.ProfileRepository;
import org.naru.mawjoudin.web.rest.util.HeaderUtil;
import org.naru.mawjoudin.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


/**
 * REST controller for managing Profile.
 */
@RestController
@RequestMapping("/api")
public class ProfileResource {

    private final Logger log = LoggerFactory.getLogger(ProfileResource.class);

    private static final String ENTITY_NAME = "profile";

    private final ProfileRepository profileRepository;


    private final ImageUploadService imageUploadService;

    private final ImageGalleryRepository imageGalleryRepository;

    private final ImageRepository imageRepository;



    public ProfileResource(ProfileRepository profileRepository,ImageUploadService imageUploadService,ImageGalleryRepository imageGalleryRepository, ImageRepository imageRepository) {
        this.profileRepository = profileRepository;
        this.imageUploadService=imageUploadService;
        this.imageGalleryRepository = imageGalleryRepository;
        this.imageRepository = imageRepository;
    }

    /**
     * POST  /profiles : Create a new profile.
     *
     * @param profile the profile to create
     * @return the ResponseEntity with status 201 (Created) and with body the new profile, or with status 400 (Bad Request) if the profile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/profiles")
    @Timed
    public ResponseEntity<Profile> createProfile(@RequestBody ProfileImage profileImage) throws URISyntaxException {
        log.debug("REST request to save Profile : {}", profileImage.getProfile());
        Image image = new Image();
       Image resImage = new Image();
        
       
    
        Profile profile = new Profile();
        if (profileImage.getFile() == null) {
           
        } else{
            resImage= imageUploadService.imageUploadProfile(profileImage.getFile(), image);
            profile.setImage(resImage);
             
            
        }
        Profile profileimg = profileImage.getProfile();
        if(profileimg.getId()!=null){
                profile.setId(profileimg.getId());
           }
       
          
        
        profile.setFirstName(profileimg.getFirstName());
        profile.setLastName(profileimg.getLastName());
        profile.setBirthdate(profileimg.getBirthdate());
        profile.setNickname(profileimg.getNickname());
        profile.setAdress(profileimg.getAdress());
        profile.setBio(profileimg.getBio());
        profile.setMembership(profileimg.isMembership());
        profile.setMotivation(profileimg.getMotivation());
        Profile result = profileRepository.save(profile);
        ImageGallery gallery = new ImageGallery();
        ImageGallery gallery2 = new ImageGallery();
          gallery.setCreationDate(ZonedDateTime.now());
        gallery2.setCreationDate(ZonedDateTime.now());
            gallery.setDescription("Photos de Profil de "+""+profileImage.getProfile().getFirstName()+""+profileImage.getProfile().getLastName());
            gallery.setType(0);
            gallery2.setType(1);
            gallery.setProfile(result);
            gallery2.setProfile(result);
            ImageGallery resgallery=imageGalleryRepository.save(gallery);
            imageGalleryRepository.save(gallery2);

            if(resImage!=null){
        resImage.setImageGallery(resgallery);
        System.out.println(resgallery.getId());
        imageRepository.save(resImage);}
        
        
        return ResponseEntity.created(new URI("/api/profiles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /profiles : Updates an existing profile.
     *
     * @param profile the profile to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated profile,
     * or with status 400 (Bad Request) if the profile is not valid,
     * or with status 500 (Internal Server Error) if the profile couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/profiles")
    @Timed
    public ResponseEntity<Profile> updateProfile(@RequestBody ProfileImage profileImage) throws URISyntaxException {
        log.debug("REST request to update Profile : {}", profileImage.getProfile().getId());
        Image image = new Image();
        System.out.println("//***************************************************//");
        System.out.println(profileImage.getFile());
         System.out.println("//***************************************************//");
        if (profileImage.getProfile().getId() == null) {
            return createProfile(profileImage);
        }
        ImageGallery gallery = imageGalleryRepository.findOneByProfile_idAndType(profileImage.getProfile().getId(), 0);
        
        Profile profile = profileRepository.findOne(profileImage.getProfile().getId());
        

      if (profileImage.getFile() == null) {
           
        } else{
        Image res= imageUploadService.imageUploadProfile(profileImage.getFile(), image);
            profile.setImage(res);  

         Image affectTogallery =  imageRepository.findOne(res.getId());
         affectTogallery.setImageGallery(gallery);
         imageRepository.save(affectTogallery);

    }
        
        Profile profileimg = profileImage.getProfile();
        if(profileimg.getFirstName()!=null){
        profile.setFirstName(profileimg.getFirstName());}
        if(profileimg.getLastName()!=null){
        profile.setLastName(profileimg.getLastName());}
    if(profileimg.getBirthdate()!=null){
        profile.setBirthdate(profileimg.getBirthdate());}
    if(profileimg.getAdress()!=null){
        profile.setAdress(profileimg.getAdress());}
       if(profileimg.getBio()!=null){ 
        profile.setBio(profileimg.getBio());}
        Profile result = profileRepository.save(profile);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * GET  /profiles : get all the profiles.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of profiles in body
     */
    @GetMapping("/profiles")
    @Timed
    public ResponseEntity<List<Profile>> getAllProfiles(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Profiles");
        Page<Profile> page = profileRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/profiles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /profiles/:id : get the "id" profile.
     *
     * @param id the id of the profile to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the profile, or with status 404 (Not Found)
     */
    @GetMapping("/profiles/{id}")
    @Timed
    public ResponseEntity<Profile> getProfile(@PathVariable Long id) {
        log.debug("REST request to get Profile : {}", id);
        Profile profile = profileRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(profile));
    }

    @GetMapping("/profilesbyname/{name}")
    @Timed
    public List<Profile> getAllprofilesByName(@PathVariable String name){
        List<Profile> profiles = profileRepository.findAllByFirstNameContainingIgnoreCase(name);
        return profiles;
    }

    /**
     * DELETE  /profiles/:id : delete the "id" profile.
     *
     * @param id the id of the profile to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/profiles/{id}")
    @Timed
    public ResponseEntity<Void> deleteProfile(@PathVariable Long id) {
        log.debug("REST request to delete Profile : {}", id);
        profileRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
