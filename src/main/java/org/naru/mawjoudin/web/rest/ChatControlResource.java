package org.naru.mawjoudin.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.naru.mawjoudin.domain.ChatControl;

import org.naru.mawjoudin.repository.ChatControlRepository;
import org.naru.mawjoudin.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ChatControl.
 */
@RestController
@RequestMapping("/api")
public class ChatControlResource {

    private final Logger log = LoggerFactory.getLogger(ChatControlResource.class);

    private static final String ENTITY_NAME = "chatControl";

    private final ChatControlRepository chatControlRepository;

    public ChatControlResource(ChatControlRepository chatControlRepository) {
        this.chatControlRepository = chatControlRepository;
    }

    /**
     * POST  /chat-controls : Create a new chatControl.
     *
     * @param chatControl the chatControl to create
     * @return the ResponseEntity with status 201 (Created) and with body the new chatControl, or with status 400 (Bad Request) if the chatControl has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/chat-controls")
    @Timed
    public ResponseEntity<ChatControl> createChatControl(@RequestBody ChatControl chatControl) throws URISyntaxException {
        log.debug("REST request to save ChatControl : {}", chatControl);
        if (chatControl.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new chatControl cannot already have an ID")).body(null);
        }
        ChatControl result = chatControlRepository.save(chatControl);
        return ResponseEntity.created(new URI("/api/chat-controls/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /chat-controls : Updates an existing chatControl.
     *
     * @param chatControl the chatControl to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated chatControl,
     * or with status 400 (Bad Request) if the chatControl is not valid,
     * or with status 500 (Internal Server Error) if the chatControl couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/chat-controls")
    @Timed
    public ResponseEntity<ChatControl> updateChatControl(@RequestBody ChatControl chatControl) throws URISyntaxException {
        log.debug("REST request to update ChatControl : {}", chatControl);
        if (chatControl.getId() == null) {
            return createChatControl(chatControl);
        }
        ChatControl result = chatControlRepository.save(chatControl);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, chatControl.getId().toString()))
            .body(result);
    }

    /**
     * GET  /chat-controls : get all the chatControls.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of chatControls in body
     */
    @GetMapping("/chat-controls")
    @Timed
    public List<ChatControl> getAllChatControls() {
        log.debug("REST request to get all ChatControls");
        return chatControlRepository.findAll();
    }

    /**
     * GET  /chat-controls/:id : get the "id" chatControl.
     *
     * @param id the id of the chatControl to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the chatControl, or with status 404 (Not Found)
     */
    @GetMapping("/chat-controls/{id}")
    @Timed
    public ResponseEntity<ChatControl> getChatControl(@PathVariable Long id) {
        log.debug("REST request to get ChatControl : {}", id);
        ChatControl chatControl = chatControlRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(chatControl));
    }

    /**
     * DELETE  /chat-controls/:id : delete the "id" chatControl.
     *
     * @param id the id of the chatControl to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/chat-controls/{id}")
    @Timed
    public ResponseEntity<Void> deleteChatControl(@PathVariable Long id) {
        log.debug("REST request to delete ChatControl : {}", id);
        chatControlRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
