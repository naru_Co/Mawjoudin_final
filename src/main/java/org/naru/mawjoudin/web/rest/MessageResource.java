package org.naru.mawjoudin.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.naru.mawjoudin.domain.Message;

import org.naru.mawjoudin.repository.MessageRepository;
import org.naru.mawjoudin.service.AdvancedEncryptionStandard;
import org.naru.mawjoudin.web.rest.util.HeaderUtil;
import org.naru.mawjoudin.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


/**
 * REST controller for managing Message.
 */
@RestController
@RequestMapping("/api")
public class MessageResource {

    private final Logger log = LoggerFactory.getLogger(MessageResource.class);

    private static final String ENTITY_NAME = "message";

    private final MessageRepository messageRepository;

    public MessageResource(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    /**
     * POST  /messages : Create a new message.
     *
     * @param message the message to create
     * @return the ResponseEntity with status 201 (Created) and with body the new message, or with status 400 (Bad Request) if the message has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/messages")
    @Timed
    public ResponseEntity<Message> createMessage(@RequestBody Message message) throws URISyntaxException {
        log.debug("REST request to save Message : {}", message);
        String secretKey= "544522881553881187753000540543603";
        if (message.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new message cannot already have an ID")).body(null);
        }

        String originalString = message.getContent();
    String encryptedString = AdvancedEncryptionStandard.encrypt(originalString, secretKey) ;
    message.setContent(encryptedString);
     
           

        Message result = messageRepository.save(message);
        return ResponseEntity.created(new URI("/api/messages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /messages : Updates an existing message.
     *
     * @param message the message to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated message,
     * or with status 400 (Bad Request) if the message is not valid,
     * or with status 500 (Internal Server Error) if the message couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/messages")
    @Timed
    public ResponseEntity<Message> updateMessage(@RequestBody Message message) throws URISyntaxException {
        log.debug("REST request to update Message : {}", message);
                String secretKey= "544522881553881187753000540543603";

        if (message.getId() == null) {
            return createMessage(message);
        }
        message.setSeenDate(ZonedDateTime.now());
        String originalString = message.getContent();
            String encryptedString = AdvancedEncryptionStandard.encrypt(originalString, secretKey) ;
    message.setContent(encryptedString);

        Message result = messageRepository.save(message);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, message.getId().toString()))
            .body(result);
    }

    /**
     * GET  /messages : get all the messages.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of messages in body
     */
    @GetMapping("/messages")
    @Timed
    public ResponseEntity<List<Message>> getAllMessages(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Messages");
        Page<Message> page = messageRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/messages");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /messages/:id : get the "id" message.
     *
     * @param id the id of the message to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the message, or with status 404 (Not Found)
     */
    @GetMapping("/messages/{id}")
    @Timed
    public ResponseEntity<Message> getMessage(@PathVariable Long id) {
        log.debug("REST request to get Message : {}", id);
        Message message = messageRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(message));
    }

     @GetMapping("/private-messages/{uid}")
    @Timed
    public List<Message> getAllMessagesByUid(@PathVariable String uid) {
                String secretKey= "544522881553881187753000540543603";

        log.debug("REST request to get all Messages");
        List<Message> finalmessages = new ArrayList<Message>();
        List<Message> messages = messageRepository.findByUid(uid);
        for (int i=0;i<messages.size();i++){
            Message temp;
            temp=messages.get(i);
            temp.setContent(AdvancedEncryptionStandard.decrypt(messages.get(i).getContent(), secretKey));
            finalmessages.add(temp);
        }
        return finalmessages;
    }

    @GetMapping("/unseen-private-messages-number/{id}")
    @Timed

    public List<Message> getNumberOfUnseenMessagesByUid(@PathVariable Long id) {
        log.debug("REST request number of unseen messages");
                            String secretKey= "544522881553881187753000540543603";

        
        List<Message> unseenMessages = messageRepository.findBySeenDateIsNullAndReceiver_Id(id);
        List<Message> finalmessages = new ArrayList<Message>();

for (int i=0;i<unseenMessages.size();i++){
            Message temp;
            temp=unseenMessages.get(i);
            temp.setContent(AdvancedEncryptionStandard.decrypt(unseenMessages.get(i).getContent(), secretKey));
            finalmessages.add(temp);
        }
        return unseenMessages;
    }

    /**
     * DELETE  /messages/:id : delete the "id" message.
     *
     * @param id the id of the message to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/messages/{id}")
    @Timed
    public ResponseEntity<Void> deleteMessage(@PathVariable Long id) {
        log.debug("REST request to delete Message : {}", id);
        messageRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
