package org.naru.mawjoudin.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.naru.mawjoudin.domain.ImageGallery;

import org.naru.mawjoudin.repository.ImageGalleryRepository;
import org.naru.mawjoudin.web.rest.util.HeaderUtil;
import org.naru.mawjoudin.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ImageGallery.
 */
@RestController
@RequestMapping("/api")
public class ImageGalleryResource {

    private final Logger log = LoggerFactory.getLogger(ImageGalleryResource.class);

    private static final String ENTITY_NAME = "imageGallery";

    private final ImageGalleryRepository imageGalleryRepository;

    public ImageGalleryResource(ImageGalleryRepository imageGalleryRepository) {
        this.imageGalleryRepository = imageGalleryRepository;
    }

    /**
     * POST  /image-galleries : Create a new imageGallery.
     *
     * @param imageGallery the imageGallery to create
     * @return the ResponseEntity with status 201 (Created) and with body the new imageGallery, or with status 400 (Bad Request) if the imageGallery has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/image-galleries")
    @Timed
    public ResponseEntity<ImageGallery> createImageGallery(@RequestBody ImageGallery imageGallery) throws URISyntaxException {
        log.debug("REST request to save ImageGallery : {}", imageGallery);
        if (imageGallery.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new imageGallery cannot already have an ID")).body(null);
        }
        ImageGallery result = imageGalleryRepository.save(imageGallery);
        return ResponseEntity.created(new URI("/api/image-galleries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /image-galleries : Updates an existing imageGallery.
     *
     * @param imageGallery the imageGallery to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated imageGallery,
     * or with status 400 (Bad Request) if the imageGallery is not valid,
     * or with status 500 (Internal Server Error) if the imageGallery couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/image-galleries")
    @Timed
    public ResponseEntity<ImageGallery> updateImageGallery(@RequestBody ImageGallery imageGallery) throws URISyntaxException {
        log.debug("REST request to update ImageGallery : {}", imageGallery);
        if (imageGallery.getId() == null) {
            return createImageGallery(imageGallery);
        }
        ImageGallery result = imageGalleryRepository.save(imageGallery);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, imageGallery.getId().toString()))
            .body(result);
    }

    /**
     * GET  /image-galleries : get all the imageGalleries.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of imageGalleries in body
     */
    @GetMapping("/image-galleries")
    @Timed
    public ResponseEntity<List<ImageGallery>> getAllImageGalleries(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ImageGalleries");
        Page<ImageGallery> page = imageGalleryRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/image-galleries");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /image-galleries/:id : get the "id" imageGallery.
     *
     * @param id the id of the imageGallery to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the imageGallery, or with status 404 (Not Found)
     */
    @GetMapping("/image-galleries/{id}")
    @Timed
    public ResponseEntity<ImageGallery> getImageGallery(@PathVariable Long id) {
        log.debug("REST request to get ImageGallery : {}", id);
        ImageGallery imageGallery = imageGalleryRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(imageGallery));
    }
  /**
     * GET  /imagegallery/profile:id/type : get the imageGallery by profile and type.
     *
     * @param id the id of the imageGallery to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the imageGallery, or with status 404 (Not Found)
     */
    @GetMapping("/gallerybyprofileandtype/{profil_id}/{type}")
    @Timed
    public ResponseEntity<ImageGallery> getImageGallery(@PathVariable Long profil_id,@PathVariable Integer type) {
        log.debug("REST request to get ImageGallery : {}", profil_id);
        ImageGallery imageGallery = imageGalleryRepository.findOneByProfile_idAndType(profil_id,type);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(imageGallery));
    }
    /**
     * DELETE  /image-galleries/:id : delete the "id" imageGallery.
     *
     * @param id the id of the imageGallery to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/image-galleries/{id}")
    @Timed
    public ResponseEntity<Void> deleteImageGallery(@PathVariable Long id) {
        log.debug("REST request to delete ImageGallery : {}", id);
        imageGalleryRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
