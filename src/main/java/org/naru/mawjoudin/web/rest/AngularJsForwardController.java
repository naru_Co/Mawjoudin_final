package org.naru.mawjoudin.web.rest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
public class AngularJsForwardController {
    // problem in this line
@RequestMapping(value = {"/{[path:[^\\.]*}","news/news/*","project/project/*","blog/blog/*","resource/*","/profile/profile/*","/questionnaire/quest","forum/forum/*"})
    public String redirect() {
        return "forward:/";
    }

}