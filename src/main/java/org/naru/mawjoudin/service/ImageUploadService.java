package org.naru.mawjoudin.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import org.imgscalr.Scalr;
import org.naru.mawjoudin.domain.Image;
import org.naru.mawjoudin.domain.ImageGallery;
import org.naru.mawjoudin.repository.ImageGalleryRepository;
import org.naru.mawjoudin.repository.ImageRepository;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.io.File;
import java.io.FileOutputStream;
import javax.servlet.*;
import javax.servlet.http.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;

import java.io.*;

@Service
public class ImageUploadService {
  private final Logger log = LoggerFactory.getLogger(ImageUploadService.class);

  private final ImageResizeService imageResizeService;

  private final ImageRepository imageRepository;

  private final ImageGalleryRepository imageGalleryRepository;

  @Autowired
  private HttpServletRequest request;

  public ImageUploadService(ImageResizeService imageResizeService, ImageRepository imageRepository,
      ImageGalleryRepository imageGalleryRepository) {
    this.imageRepository = imageRepository;
    this.imageResizeService = imageResizeService;
    this.imageGalleryRepository = imageGalleryRepository;
  }

  public Image imageUploadProfile(byte[] file, Image image) {

    String uploadsDir = "/profiles_photos/";
    String realPathtoUploads = request.getServletContext().getRealPath(uploadsDir);
    if (!new File(realPathtoUploads).exists()) {
      new File(realPathtoUploads).mkdir();
    }

    String orgName = Math.round(Math.random() * 100000) + ""
        + (new SimpleDateFormat("yyyyMMddHHmmssSSSSSS").format(new Date())) + "" + ".jpeg";
    String filePath = realPathtoUploads + "/" + orgName;
    String tmblPath = realPathtoUploads + "/" + "tmbl_" + orgName;
    String mdPath = realPathtoUploads + "/" + "md_" + orgName;
    String nrPath = realPathtoUploads + "/" + "nr_" + orgName;
    File dest = new File(filePath);
    image.setHighRes("/profiles_photos/" + orgName);

    try {
      new FileOutputStream(dest).write(file);
      imageResizeService.thumbailSizeforSquareImage(filePath, tmblPath);
      image.setThumbnail("/profiles_photos/tmbl_" + orgName);
      imageResizeService.mediumSizeforSquareImage(filePath, mdPath);
      image.setMediumRes("/profiles_photos/md_" + orgName);
      imageResizeService.normalSizeforSquareImage(filePath, nrPath);
      image.setNormalRes("/profiles_photos/nr_" + orgName);

    } catch (Exception e) {
      System.out.println(e);
    }
    Image res = imageRepository.save(image);
    return res;

  }

  public Image imageUploadNews(byte[] file, Image image) {

    String uploadsDir = "/news_images/";
    String realPathtoUploads = request.getServletContext().getRealPath(uploadsDir);
    if (!new File(realPathtoUploads).exists()) {
      new File(realPathtoUploads).mkdir();
    }

    String orgName = Math.round(Math.random() * 100000) + ""
        + (new SimpleDateFormat("yyyyMMddHHmmssSSSSSS").format(new Date())) + "" + ".jpeg";
    String filePath = realPathtoUploads + "/" + orgName;
    String tmblPath = realPathtoUploads + "/" + "tmbl_" + orgName;
    String mdPath = realPathtoUploads + "/" + "md_" + orgName;
    String nrPath = realPathtoUploads + "/" + "nr_" + orgName;
    File dest = new File(filePath);
    image.setHighRes("/news_images/" + orgName);

    try {
      new FileOutputStream(dest).write(file);
      BufferedImage bimg = ImageIO.read(dest);
      int width = bimg.getWidth();
      int height = bimg.getHeight();
      if (width == height) {
        imageResizeService.thumbailSizeforSquareImage(filePath, tmblPath);
        image.setThumbnail("/news_images/tmbl_" + orgName);
        imageResizeService.mediumSizeforSquareImage(filePath, mdPath);
        image.setMediumRes("/news_images/md_" + orgName);
        imageResizeService.normalSizeforSquareImage(filePath, nrPath);
        image.setNormalRes("/news_images/nr_" + orgName);
      } else if (width < height) {
        double ratio = (double) width / (double) height;
        imageResizeService.thumbailSizeforRectangleImage(filePath, tmblPath, ratio);
        image.setThumbnail("/news_images/tmbl_" + orgName);
        imageResizeService.mediumSizeforRectangleImage(filePath, mdPath, ratio);
        image.setMediumRes("/news_images/md_" + orgName);
        imageResizeService.normalSizeforRectangleImage(filePath, nrPath, ratio);
        image.setNormalRes("/news_images/nr_" + orgName);

      } else {
        double ratio = (double) height / (double) width;
        imageResizeService.thumbailSizeforRevRectangleImage(filePath, tmblPath, ratio);
        image.setThumbnail("/news_images/tmbl_" + orgName);
        imageResizeService.mediumSizeforRevRectangleImage(filePath, mdPath, ratio);
        image.setMediumRes("/news_images/md_" + orgName);
        imageResizeService.normalSizeforRevRectangleImage(filePath, nrPath, ratio);
        image.setNormalRes("/news_images/nr_" + orgName);

      }

    } catch (Exception e) {
      System.out.println(e);
    }
    Image res = imageRepository.save(image);
    return res;

  }

  public Image imageUploadProject(byte[] file, Image image) {

    String uploadsDir = "/project_images/";
    String realPathtoUploads = request.getServletContext().getRealPath(uploadsDir);
    if (!new File(realPathtoUploads).exists()) {
      new File(realPathtoUploads).mkdir();
    }

    String orgName = Math.round(Math.random() * 100000) + ""
        + (new SimpleDateFormat("yyyyMMddHHmmssSSSSSS").format(new Date())) + "" + ".jpeg";
    String filePath = realPathtoUploads + "/" + orgName;
    String tmblPath = realPathtoUploads + "/" + "tmbl_" + orgName;
    String mdPath = realPathtoUploads + "/" + "md_" + orgName;
    String nrPath = realPathtoUploads + "/" + "nr_" + orgName;
    File dest = new File(filePath);
    image.setHighRes("/news_images/" + orgName);

    try {
      new FileOutputStream(dest).write(file);
      BufferedImage bimg = ImageIO.read(dest);
      int width = bimg.getWidth();
      int height = bimg.getHeight();
      if (width == height) {
        imageResizeService.thumbailSizeforSquareImage(filePath, tmblPath);
        image.setThumbnail("/project_images/tmbl_" + orgName);
        imageResizeService.mediumSizeforSquareImage(filePath, mdPath);
        image.setMediumRes("/project_images/md_" + orgName);
        imageResizeService.normalSizeforSquareImage(filePath, nrPath);
        image.setNormalRes("/project_images/nr_" + orgName);
      } else if (width < height) {
        double ratio = (double) width / (double) height;
        imageResizeService.thumbailSizeforRectangleImage(filePath, tmblPath, ratio);
        image.setThumbnail("/project_images/tmbl_" + orgName);
        imageResizeService.mediumSizeforRectangleImage(filePath, mdPath, ratio);
        image.setMediumRes("/project_images/md_" + orgName);
        imageResizeService.normalSizeforRectangleImage(filePath, nrPath, ratio);
        image.setNormalRes("/project_images/nr_" + orgName);

      } else {
        double ratio = (double) height / (double) width;
        imageResizeService.thumbailSizeforRevRectangleImage(filePath, tmblPath, ratio);
        image.setThumbnail("/project_images/tmbl_" + orgName);
        imageResizeService.mediumSizeforRevRectangleImage(filePath, mdPath, ratio);
        image.setMediumRes("/project_images/md_" + orgName);
        imageResizeService.normalSizeforRevRectangleImage(filePath, nrPath, ratio);
        image.setNormalRes("/project_images/nr_" + orgName);

      }

    } catch (Exception e) {
      System.out.println(e);
    }
    Image res = imageRepository.save(image);
    return res;

  }

  public Image imageUploadBlog(byte[] file, Image image) {

    String uploadsDir = "/blog_images/";
    String realPathtoUploads = request.getServletContext().getRealPath(uploadsDir);
    if (!new File(realPathtoUploads).exists()) {
      new File(realPathtoUploads).mkdir();
    }

    String orgName = Math.round(Math.random() * 100000) + ""
        + (new SimpleDateFormat("yyyyMMddHHmmssSSSSSS").format(new Date())) + "" + ".jpeg";
    String filePath = realPathtoUploads + "/" + orgName;
    String tmblPath = realPathtoUploads + "/" + "tmbl_" + orgName;
    String mdPath = realPathtoUploads + "/" + "md_" + orgName;
    String nrPath = realPathtoUploads + "/" + "nr_" + orgName;
    File dest = new File(filePath);
    image.setHighRes("/blog_images/" + orgName);

    try {
      new FileOutputStream(dest).write(file);
      BufferedImage bimg = ImageIO.read(dest);
      int width = bimg.getWidth();
      int height = bimg.getHeight();
      if (width == height) {
        imageResizeService.thumbailSizeforSquareImage(filePath, tmblPath);
        image.setThumbnail("/blog_images/tmbl_" + orgName);
        imageResizeService.mediumSizeforSquareImage(filePath, mdPath);
        image.setMediumRes("/blog_images/md_" + orgName);
        imageResizeService.normalSizeforSquareImage(filePath, nrPath);
        image.setNormalRes("/blog_images/nr_" + orgName);
      } else if (width < height) {
        double ratio = (double) width / (double) height;
        imageResizeService.thumbailSizeforRectangleImage(filePath, tmblPath, ratio);
        image.setThumbnail("/blog_images/tmbl_" + orgName);
        imageResizeService.mediumSizeforRectangleImage(filePath, mdPath, ratio);
        image.setMediumRes("/blog_images/md_" + orgName);
        imageResizeService.normalSizeforRectangleImage(filePath, nrPath, ratio);
        image.setNormalRes("/blog_images/nr_" + orgName);

      } else {
        double ratio = (double) height / (double) width;
        imageResizeService.thumbailSizeforRevRectangleImage(filePath, tmblPath, ratio);
        image.setThumbnail("/blog_images/tmbl_" + orgName);
        imageResizeService.mediumSizeforRevRectangleImage(filePath, mdPath, ratio);
        image.setMediumRes("/blog_images/md_" + orgName);
        imageResizeService.normalSizeforRevRectangleImage(filePath, nrPath, ratio);
        image.setNormalRes("/blog_images/nr_" + orgName);

      }

    } catch (Exception e) {
      System.out.println(e);
    }
    Image res = imageRepository.save(image);
    return res;

  }

  public Image imageUploadPartner(byte[] file, Image image) {

    String uploadsDir = "/partners_logo/";
    String realPathtoUploads = request.getServletContext().getRealPath(uploadsDir);
    if (!new File(realPathtoUploads).exists()) {
      new File(realPathtoUploads).mkdir();
    }

    String orgName = Math.round(Math.random() * 100000) + ""
        + (new SimpleDateFormat("yyyyMMddHHmmssSSSSSS").format(new Date())) + "" + ".jpeg";
    String filePath = realPathtoUploads + "/" + orgName;
    String tmblPath = realPathtoUploads + "/" + "tmbl_" + orgName;
    String mdPath = realPathtoUploads + "/" + "md_" + orgName;
    String nrPath = realPathtoUploads + "/" + "nr_" + orgName;
    File dest = new File(filePath);
    image.setHighRes("/partners_logo/" + orgName);

    try {
      new FileOutputStream(dest).write(file);
      BufferedImage bimg = ImageIO.read(dest);
      int width = bimg.getWidth();
      int height = bimg.getHeight();
      if (width == height) {
        imageResizeService.thumbailSizeforSquareImage(filePath, tmblPath);
        image.setThumbnail("/partners_logo/tmbl_" + orgName);
        imageResizeService.mediumSizeforSquareImage(filePath, mdPath);
        image.setMediumRes("/partners_logo/md_" + orgName);
        imageResizeService.normalSizeforSquareImage(filePath, nrPath);
        image.setNormalRes("/partners_logo/nr_" + orgName);
      } else if (width < height) {
        double ratio = (double) width / (double) height;
        imageResizeService.thumbailSizeforRectangleImage(filePath, tmblPath, ratio);
        image.setThumbnail("/partners_logo/tmbl_" + orgName);
        imageResizeService.mediumSizeforRectangleImage(filePath, mdPath, ratio);
        image.setMediumRes("/partners_logo/md_" + orgName);
        imageResizeService.normalSizeforRectangleImage(filePath, nrPath, ratio);
        image.setNormalRes("/partners_logo/nr_" + orgName);

      } else {
        double ratio = (double) height / (double) width;
        imageResizeService.thumbailSizeforRevRectangleImage(filePath, tmblPath, ratio);
        image.setThumbnail("/partners_logo/tmbl_" + orgName);
        imageResizeService.mediumSizeforRevRectangleImage(filePath, mdPath, ratio);
        image.setMediumRes("/partners_logo/md_" + orgName);
        imageResizeService.normalSizeforRevRectangleImage(filePath, nrPath, ratio);
        image.setNormalRes("/partners_logo/nr_" + orgName);

      }

    } catch (Exception e) {
      System.out.println(e);
    }

    Image res = imageRepository.save(image);
    return res;
  }

  public Image imageUploadPublication(byte[] file, Image image) {

    String uploadsDir = "/publication_images/";
    String realPathtoUploads = request.getServletContext().getRealPath(uploadsDir);
    if (!new File(realPathtoUploads).exists()) {
      new File(realPathtoUploads).mkdir();
    }

    String orgName = Math.round(Math.random() * 100000) + ""
        + (new SimpleDateFormat("yyyyMMddHHmmssSSSSSS").format(new Date())) + "" + ".jpeg";
    String filePath = realPathtoUploads + "/" + orgName;
    String tmblPath = realPathtoUploads + "/" + "tmbl_" + orgName;
    String mdPath = realPathtoUploads + "/" + "md_" + orgName;
    String nrPath = realPathtoUploads + "/" + "nr_" + orgName;
    File dest = new File(filePath);
    image.setHighRes("/publication_images/" + orgName);

    try {
      new FileOutputStream(dest).write(file);
      BufferedImage bimg = ImageIO.read(dest);
      int width = bimg.getWidth();
      int height = bimg.getHeight();
      if (width == height) {
        imageResizeService.thumbailSizeforSquareImage(filePath, tmblPath);
        image.setThumbnail("/publication_images/tmbl_" + orgName);
        imageResizeService.mediumSizeforSquareImage(filePath, mdPath);
        image.setMediumRes("/publication_images/md_" + orgName);
        imageResizeService.normalSizeforSquareImage(filePath, nrPath);
        image.setNormalRes("/publication_images/nr_" + orgName);
      } else if (width < height) {
        double ratio = (double) width / (double) height;
        imageResizeService.thumbailSizeforRectangleImage(filePath, tmblPath, ratio);
        image.setThumbnail("/publication_images/tmbl_" + orgName);
        imageResizeService.mediumSizeforRectangleImage(filePath, mdPath, ratio);
        image.setMediumRes("/publication_images/md_" + orgName);
        imageResizeService.normalSizeforRectangleImage(filePath, nrPath, ratio);
        image.setNormalRes("/publication_images/nr_" + orgName);

      } else {
        double ratio = (double) height / (double) width;
        imageResizeService.thumbailSizeforRevRectangleImage(filePath, tmblPath, ratio);
        image.setThumbnail("/publication_images/tmbl_" + orgName);
        imageResizeService.mediumSizeforRevRectangleImage(filePath, mdPath, ratio);
        image.setMediumRes("/publication_images/md_" + orgName);
        imageResizeService.normalSizeforRevRectangleImage(filePath, nrPath, ratio);
        image.setNormalRes("/publication_images/nr_" + orgName);

      }

    }

    catch (Exception e) {
      System.out.println(e);
    }
    Image res = imageRepository.save(image);
    return res;

  }

  public Image imageUploadSlider(byte[] file, Image image) {

    String uploadsDir = "/slider_images/";
    String realPathtoUploads = request.getServletContext().getRealPath(uploadsDir);
    if (!new File(realPathtoUploads).exists()) {
      new File(realPathtoUploads).mkdir();
    }

    String orgName = Math.round(Math.random() * 100000) + ""
        + (new SimpleDateFormat("yyyyMMddHHmmssSSSSSS").format(new Date())) + "" + ".jpeg";
    String filePath = realPathtoUploads + "/" + orgName;
    String tmblPath = realPathtoUploads + "/" + "tmbl_" + orgName;
    String mdPath = realPathtoUploads + "/" + "md_" + orgName;
    String nrPath = realPathtoUploads + "/" + "nr_" + orgName;
    File dest = new File(filePath);
    image.setHighRes("/slider_images/" + orgName);

    try {
      new FileOutputStream(dest).write(file);
      BufferedImage bimg = ImageIO.read(dest);
      int width = bimg.getWidth();
      int height = bimg.getHeight();
      if (width == height) {

        imageResizeService.thumbailSizeforSquareImage(filePath, tmblPath);
        image.setThumbnail("/slider_images/tmbl_" + orgName);
        imageResizeService.mediumSizeforSquareImage(filePath, mdPath);
        image.setMediumRes("/slider_images/md_" + orgName);
        imageResizeService.normalSizeforSquareImage(filePath, nrPath);
        image.setNormalRes("/slider_images/nr_" + orgName);
      } else if (width < height) {
        double ratio = (double) width / (double) height;
        imageResizeService.thumbailSizeforRectangleImage(filePath, tmblPath, ratio);
        image.setThumbnail("/slider_images/tmbl_" + orgName);
        imageResizeService.mediumSizeforRectangleImage(filePath, mdPath, ratio);
        image.setMediumRes("/slider_images/md_" + orgName);
        imageResizeService.normalSizeforRectangleImage(filePath, nrPath, ratio);
        image.setNormalRes("/slider_images/nr_" + orgName);

      } else {
        double ratio = (double) height / (double) width;
        imageResizeService.thumbailSizeforRevRectangleImage(filePath, tmblPath, ratio);
        image.setThumbnail("/slider_images/tmbl_" + orgName);
        imageResizeService.mediumSizeforRevRectangleImage(filePath, mdPath, ratio);
        image.setMediumRes("/slider_images/md_" + orgName);
        imageResizeService.normalSizeforRevRectangleImage(filePath, nrPath, ratio);
        image.setNormalRes("/slider_images/nr_" + orgName);

      }

    }

    catch (Exception e) {
      System.out.println(e);
    }
    Image res = imageRepository.save(image);
    return res;

  }

  public Image imageUploadMessage(byte[] file, Image image) {

    String uploadsDir = "/messages_images/";
    String realPathtoUploads = request.getServletContext().getRealPath(uploadsDir);
    if (!new File(realPathtoUploads).exists()) {
      new File(realPathtoUploads).mkdir();
    }

    String orgName = Math.round(Math.random() * 100000) + ""
        + (new SimpleDateFormat("yyyyMMddHHmmssSSSSSS").format(new Date())) + "" + ".jpeg";
    String filePath = realPathtoUploads + "/" + orgName;
    String tmblPath = realPathtoUploads + "/" + "tmbl_" + orgName;
    String mdPath = realPathtoUploads + "/" + "md_" + orgName;
    String nrPath = realPathtoUploads + "/" + "nr_" + orgName;
    File dest = new File(filePath);
    image.setHighRes("/messages_images/" + orgName);

    try {
      new FileOutputStream(dest).write(file);
      BufferedImage bimg = ImageIO.read(dest);
      int width = bimg.getWidth();
      int height = bimg.getHeight();
      if (width == height) {

        imageResizeService.thumbailSizeforSquareImage(filePath, tmblPath);
        image.setThumbnail("/messages_images/tmbl_" + orgName);
        imageResizeService.mediumSizeforSquareImage(filePath, mdPath);
        image.setMediumRes("/messages_images/md_" + orgName);
        imageResizeService.normalSizeforSquareImage(filePath, nrPath);
        image.setNormalRes("/messages_images/nr_" + orgName);
      } else if (width < height) {
        double ratio = (double) width / (double) height;
        imageResizeService.thumbailSizeforRectangleImage(filePath, tmblPath, ratio);
        image.setThumbnail("/messages_images/tmbl_" + orgName);
        imageResizeService.mediumSizeforRectangleImage(filePath, mdPath, ratio);
        image.setMediumRes("/messages_images/md_" + orgName);
        imageResizeService.normalSizeforRectangleImage(filePath, nrPath, ratio);
        image.setNormalRes("/messages_images/nr_" + orgName);

      } else {
        double ratio = (double) height / (double) width;
        imageResizeService.thumbailSizeforRevRectangleImage(filePath, tmblPath, ratio);
        image.setThumbnail("/messages_images/tmbl_" + orgName);
        imageResizeService.mediumSizeforRevRectangleImage(filePath, mdPath, ratio);
        image.setMediumRes("/messages_images/md_" + orgName);
        imageResizeService.normalSizeforRevRectangleImage(filePath, nrPath, ratio);
        image.setNormalRes("/messages_images/nr_" + orgName);

      }

    }

    catch (Exception e) {
      System.out.println(e);
    }
    Image res = imageRepository.save(image);
    return res;

  }

}