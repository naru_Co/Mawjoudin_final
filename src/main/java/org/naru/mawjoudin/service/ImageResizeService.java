package org.naru.mawjoudin.service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import org.imgscalr.Scalr;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

import java.io.File;
import java.io.FileOutputStream;  
import java.io.FileInputStream; 
import java.io.InputStream;
import java.awt.*;
import java.awt.image.*;

import java.io.*;

@Service
public class ImageResizeService {

    private final Logger log = LoggerFactory.getLogger(ImageResizeService.class);
    
         private static final int[] RGB_MASKS = {0xFF0000, 0xFF00, 0xFF};

private static final ColorModel RGB_OPAQUE = new DirectColorModel(32, RGB_MASKS[0], RGB_MASKS[1], RGB_MASKS[2]);

    
    
    

    public void thumbailSizeforSquareImage(String filePath,String outputPath){
        try{
             Image img = Toolkit.getDefaultToolkit().createImage(filePath);

              PixelGrabber pg = new PixelGrabber(img, 0, 0, -1, -1, true);
              
                  pg.grabPixels();
              int width = pg.getWidth(), height = pg.getHeight();

        DataBuffer buffer = new DataBufferInt((int[]) pg.getPixels(), pg.getWidth() * pg.getHeight());
        WritableRaster raster = Raster.createPackedRaster(buffer, width, height, width, RGB_MASKS, null);
        BufferedImage bi = new BufferedImage(RGB_OPAQUE, raster, false, null);

                 BufferedImage    scaledImage = Scalr.resize(bi, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 100, 100, null);
                     
                     System.out.println("the scaled Image "+ scaledImage);
                     File outputfile = new File(outputPath);
                     ImageIO.write(scaledImage, "jpeg", outputfile);
                     
                    }
                     catch(Exception e){}

    }
    public void mediumSizeforSquareImage(String filePath,String outputPath){
         
         try{
             Image img = Toolkit.getDefaultToolkit().createImage(filePath);

PixelGrabber pg = new PixelGrabber(img, 0, 0, -1, -1, true);
pg.grabPixels();
int width = pg.getWidth(), height = pg.getHeight();

DataBuffer buffer = new DataBufferInt((int[]) pg.getPixels(), pg.getWidth() * pg.getHeight());
WritableRaster raster = Raster.createPackedRaster(buffer, width, height, width, RGB_MASKS, null);
BufferedImage bi = new BufferedImage(RGB_OPAQUE, raster, false, null);

                 BufferedImage    scaledImage = Scalr.resize(bi, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 300, 300, null);
                     
                     
                     File outputfile = new File(outputPath);
                     ImageIO.write(scaledImage, "jpeg", outputfile);
                      }
                     catch(Exception e){}

    }
    public void normalSizeforSquareImage(String filePath,String outputPath){
        try{
         Image img = Toolkit.getDefaultToolkit().createImage(filePath);

PixelGrabber pg = new PixelGrabber(img, 0, 0, -1, -1, true);
pg.grabPixels();
int width = pg.getWidth(), height = pg.getHeight();

DataBuffer buffer = new DataBufferInt((int[]) pg.getPixels(), pg.getWidth() * pg.getHeight());
WritableRaster raster = Raster.createPackedRaster(buffer, width, height, width, RGB_MASKS, null);
BufferedImage bi = new BufferedImage(RGB_OPAQUE, raster, false, null);

                 BufferedImage    scaledImage = Scalr.resize(bi, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 1000, 1000, null);
                     File outputfile = new File(outputPath);
                     ImageIO.write(scaledImage, "jpeg", outputfile);
                      }
                     catch(Exception e){}

    }
     public void thumbailSizeforRectangleImage(String filePath,String outputPath, double ratio){
        try{ 
            Image img = Toolkit.getDefaultToolkit().createImage(filePath);

PixelGrabber pg = new PixelGrabber(img, 0, 0, -1, -1, true);
pg.grabPixels();
int width = pg.getWidth(), height = pg.getHeight();
double widthResized = ratio*100;
int y = (int) widthResized;



DataBuffer buffer = new DataBufferInt((int[]) pg.getPixels(), pg.getWidth() * pg.getHeight());
WritableRaster raster = Raster.createPackedRaster(buffer, width, height, width, RGB_MASKS, null);
BufferedImage bi = new BufferedImage(RGB_OPAQUE, raster, false, null);

                 BufferedImage    scaledImage = Scalr.resize(bi, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT,y,100, null);
                     
                     System.out.println("the scaled Image "+ scaledImage);
                     File outputfile = new File(outputPath);
                     ImageIO.write(scaledImage, "jpeg", outputfile);
                      }
                     catch(Exception e){}

    }
    public void mediumSizeforRectangleImage(String filePath,String outputPath,double ratio){
         try{
             Image img = Toolkit.getDefaultToolkit().createImage(filePath);

PixelGrabber pg = new PixelGrabber(img, 0, 0, -1, -1, true);
pg.grabPixels();
int width = pg.getWidth(), height = pg.getHeight();
double widthResized = ratio*300;
int y = (int) widthResized;

DataBuffer buffer = new DataBufferInt((int[]) pg.getPixels(), pg.getWidth() * pg.getHeight());
WritableRaster raster = Raster.createPackedRaster(buffer, width, height, width, RGB_MASKS, null);
BufferedImage bi = new BufferedImage(RGB_OPAQUE, raster, false, null);

                 BufferedImage    scaledImage = Scalr.resize(bi, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT,  y,300, null);
                     
                     System.out.println("the scaled Image "+ scaledImage);
                     File outputfile = new File(outputPath );
                     ImageIO.write(scaledImage, "jpeg", outputfile);
                      }
                     catch(Exception e){}

    }
    public void normalSizeforRectangleImage(String filePath,String outputPath,double ratio){
        try{
        Image img = Toolkit.getDefaultToolkit().createImage(filePath);

PixelGrabber pg = new PixelGrabber(img, 0, 0, -1, -1, true);
pg.grabPixels();
int width = pg.getWidth(), height = pg.getHeight();
double widthResized = ratio*1000;
int y = (int) widthResized;

DataBuffer buffer = new DataBufferInt((int[]) pg.getPixels(), pg.getWidth() * pg.getHeight());
WritableRaster raster = Raster.createPackedRaster(buffer, width, height, width, RGB_MASKS, null);
BufferedImage bi = new BufferedImage(RGB_OPAQUE, raster, false, null);

                 BufferedImage    scaledImage = Scalr.resize(bi, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, y, 1000, null);
                     
                     System.out.println("the scaled Image "+ scaledImage);
                     File outputfile = new File(outputPath);
                     ImageIO.write(scaledImage, "jpeg", outputfile);
                   
                      }
                     catch(Exception e){}

    }
 public void thumbailSizeforRevRectangleImage(String filePath,String outputPath,double ratio){
        try{ 
            Image img = Toolkit.getDefaultToolkit().createImage(filePath);

PixelGrabber pg = new PixelGrabber(img, 0, 0, -1, -1, true);
pg.grabPixels();
int width = pg.getWidth(), height = pg.getHeight();
double widthResized = ratio*100;
int y = (int) widthResized;

DataBuffer buffer = new DataBufferInt((int[]) pg.getPixels(), pg.getWidth() * pg.getHeight());
WritableRaster raster = Raster.createPackedRaster(buffer, width, height, width, RGB_MASKS, null);
BufferedImage bi = new BufferedImage(RGB_OPAQUE, raster, false, null);

                 BufferedImage    scaledImage = Scalr.resize(bi, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT,100,y,  null);
                     
                     System.out.println("the scaled Image "+ scaledImage);
                     File outputfile = new File(outputPath);
                     ImageIO.write(scaledImage, "jpeg", outputfile);
                      }
                     catch(Exception e){}

    }
    public void mediumSizeforRevRectangleImage(String filePath,String outputPath,double ratio){
         try{
             Image img = Toolkit.getDefaultToolkit().createImage(filePath);

PixelGrabber pg = new PixelGrabber(img, 0, 0, -1, -1, true);
pg.grabPixels();
int width = pg.getWidth(), height = pg.getHeight();
double widthResized = ratio*300;
int y = (int) widthResized;
DataBuffer buffer = new DataBufferInt((int[]) pg.getPixels(), pg.getWidth() * pg.getHeight());
WritableRaster raster = Raster.createPackedRaster(buffer, width, height, width, RGB_MASKS, null);
BufferedImage bi = new BufferedImage(RGB_OPAQUE, raster, false, null);

                 BufferedImage    scaledImage = Scalr.resize(bi, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT,300,y,   null);
                     
                     System.out.println("the scaled Image "+ scaledImage);
                     File outputfile = new File(outputPath );
                     ImageIO.write(scaledImage, "jpeg", outputfile);
                      }
                     catch(Exception e){}

    }
    public void normalSizeforRevRectangleImage(String filePath,String outputPath,double ratio){
        try{
        Image img = Toolkit.getDefaultToolkit().createImage(filePath);

PixelGrabber pg = new PixelGrabber(img, 0, 0, -1, -1, true);
pg.grabPixels();
int width = pg.getWidth(), height = pg.getHeight();

double widthResized = ratio*1000;
int y = (int) widthResized;

DataBuffer buffer = new DataBufferInt((int[]) pg.getPixels(), pg.getWidth() * pg.getHeight());
WritableRaster raster = Raster.createPackedRaster(buffer, width, height, width, RGB_MASKS, null);
BufferedImage bi = new BufferedImage(RGB_OPAQUE, raster, false, null);

                 BufferedImage    scaledImage = Scalr.resize(bi, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT,1000, y,  null);
                     
                     System.out.println("the scaled Image "+ scaledImage);
                     File outputfile = new File(outputPath);
                     ImageIO.write(scaledImage, "jpeg", outputfile);
                   
                      }
                     catch(Exception e){}

    }









}