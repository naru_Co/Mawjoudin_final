package org.naru.mawjoudin.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Slider.
 */
@Entity
@Table(name = "slider")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Slider implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "headlline")
    private String headlline;

    @Column(name = "simple_text")
    private String simpleText;

    @Column(name = "headline_fr")
    private String headlineFr;

    @Column(name = "simple_text_fr")
    private String simpleTextFr;

    @Column(name = "headline_ar")
    private String headlineAr;

    @Column(name = "simple_text_ar")
    private String simpleTextAr;

    @OneToOne
    @JoinColumn(unique = true)
    private Image image;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHeadlline() {
        return headlline;
    }

    public Slider headlline(String headlline) {
        this.headlline = headlline;
        return this;
    }

    public void setHeadlline(String headlline) {
        this.headlline = headlline;
    }

    public String getSimpleText() {
        return simpleText;
    }

    public Slider simpleText(String simpleText) {
        this.simpleText = simpleText;
        return this;
    }

    public void setSimpleText(String simpleText) {
        this.simpleText = simpleText;
    }

    public String getHeadlineFr() {
        return headlineFr;
    }

    public Slider headlineFr(String headlineFr) {
        this.headlineFr = headlineFr;
        return this;
    }

    public void setHeadlineFr(String headlineFr) {
        this.headlineFr = headlineFr;
    }

    public String getSimpleTextFr() {
        return simpleTextFr;
    }

    public Slider simpleTextFr(String simpleTextFr) {
        this.simpleTextFr = simpleTextFr;
        return this;
    }

    public void setSimpleTextFr(String simpleTextFr) {
        this.simpleTextFr = simpleTextFr;
    }

    public String getHeadlineAr() {
        return headlineAr;
    }

    public Slider headlineAr(String headlineAr) {
        this.headlineAr = headlineAr;
        return this;
    }

    public void setHeadlineAr(String headlineAr) {
        this.headlineAr = headlineAr;
    }

    public String getSimpleTextAr() {
        return simpleTextAr;
    }

    public Slider simpleTextAr(String simpleTextAr) {
        this.simpleTextAr = simpleTextAr;
        return this;
    }

    public void setSimpleTextAr(String simpleTextAr) {
        this.simpleTextAr = simpleTextAr;
    }

    public Image getImage() {
        return image;
    }

    public Slider image(Image image) {
        this.image = image;
        return this;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Slider slider = (Slider) o;
        if (slider.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), slider.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Slider{" +
            "id=" + getId() +
            ", headlline='" + getHeadlline() + "'" +
            ", simpleText='" + getSimpleText() + "'" +
            ", headlineFr='" + getHeadlineFr() + "'" +
            ", simpleTextFr='" + getSimpleTextFr() + "'" +
            ", headlineAr='" + getHeadlineAr() + "'" +
            ", simpleTextAr='" + getSimpleTextAr() + "'" +
            "}";
    }
}
