package org.naru.mawjoudin.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Image.
 */
@Entity
@Table(name = "image")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Image implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "high_res")
    private String highRes;

    @Column(name = "normal_res")
    private String normalRes;

    @Column(name = "medium_res")
    private String mediumRes;

    @Column(name = "thumbnail")
    private String thumbnail;

    @ManyToOne
    private NewsLetter newsLetter;

    @ManyToOne
    @JsonIgnore
    private ImageGallery imageGallery;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHighRes() {
        return highRes;
    }

    public Image highRes(String highRes) {
        this.highRes = highRes;
        return this;
    }

    public void setHighRes(String highRes) {
        this.highRes = highRes;
    }

    public String getNormalRes() {
        return normalRes;
    }

    public Image normalRes(String normalRes) {
        this.normalRes = normalRes;
        return this;
    }

    public void setNormalRes(String normalRes) {
        this.normalRes = normalRes;
    }

    public String getMediumRes() {
        return mediumRes;
    }

    public Image mediumRes(String mediumRes) {
        this.mediumRes = mediumRes;
        return this;
    }

    public void setMediumRes(String mediumRes) {
        this.mediumRes = mediumRes;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public Image thumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
        return this;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public NewsLetter getNewsLetter() {
        return newsLetter;
    }

    public Image newsLetter(NewsLetter newsLetter) {
        this.newsLetter = newsLetter;
        return this;
    }

    public void setNewsLetter(NewsLetter newsLetter) {
        this.newsLetter = newsLetter;
    }

    public ImageGallery getImageGallery() {
        return imageGallery;
    }

    public Image imageGallery(ImageGallery imageGallery) {
        this.imageGallery = imageGallery;
        return this;
    }

    public void setImageGallery(ImageGallery imageGallery) {
        this.imageGallery = imageGallery;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Image image = (Image) o;
        if (image.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), image.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Image{" +
            "id=" + getId() +
            ", highRes='" + getHighRes() + "'" +
            ", normalRes='" + getNormalRes() + "'" +
            ", mediumRes='" + getMediumRes() + "'" +
            ", thumbnail='" + getThumbnail() + "'" +
            "}";
    }
}
