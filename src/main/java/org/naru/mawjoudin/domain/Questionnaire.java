package org.naru.mawjoudin.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Questionnaire.
 */
@Entity
@Table(name = "questionnaire")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Questionnaire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "biological_sex")
    private Integer biologicalSex;

    @Column(name = "gender_identity")
    private Integer genderIdentity;

    @Column(name = "sexual_orientation")
    private Integer sexualOrientation;

    @Column(name = "age")
    private Long age;

    @Column(name = "adress")
    private String adress;

    @Column(name = "live_in")
    private Integer liveIn;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "nationality")
    private String nationality;

    @Column(name = "connection_with_lgbt")
    private Integer connectionWithLgbt;

    @Column(name = "jhi_extent")
    private Integer extent;

    @Column(name = "true_date_of_incident")
    private ZonedDateTime trueDateOfIncident;

    @Column(name = "adress_incident")
    private String adressIncident;

    @Column(name = "place_incident")
    private Integer placeIncident;

    @Column(name = "type_of_incident")
    private String typeOfIncident;

    
    @Column(name = "incident_description")
    private String incidentDescription;

    @Column(name = "occurence_incident")
    private Integer occurenceIncident;

    @Column(name = "number_perpetrators")
    private Integer numberPerpetrators;

    
    @Column(name = "threat_description")
    private String threatDescription;

    @Column(name = "identityperpetrator")
    private Integer identityperpetrator;

    
    @Column(name = "identifier_perpetrator")
    private String identifierPerpetrator;

    @Column(name = "age_perpetrator")
    private String agePerpetrator;

    
    @Column(name = "witness_reaction")
    private String witnessReaction;

    
    @Column(name = "witness_detail")
    private String witnessDetail;

    @Column(name = "incident_link")
    private Integer incidentLink;

    @Column(name = "incident_link_indic")
    private Integer incidentLinkIndic;

    @Column(name = "impact")
    private Integer impact;

    
    @Column(name = "impact_detail")
    private String impactDetail;

    @Column(name = "medical_care")
    private Integer medicalCare;

    
    @Column(name = "medical_staff")
    private String medicalStaff;

    
    @Column(name = "psy_imact")
    private String psyImact;

    @Column(name = "support_kind")
    private Integer supportKind;

    @Column(name = "police")
    private Integer police;

    
    @Column(name = "police_desc")
    private String policeDesc;

    @Column(name = "forcibly_police")
    private String forciblyPolice;

    @Column(name = "arrest_period")
    private String arrestPeriod;

    @Column(name = "arrest_motif")
    private Integer arrestMotif;

    
    @Column(name = "arrest_detail")
    private String arrestDetail;

    @Column(name = "incident_report")
    private Integer incidentReport;

    
    @Column(name = "anything_to_add")
    private String anythingToAdd;

    @Column(name = "creation_date")
    private ZonedDateTime creationDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public Questionnaire fullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getBiologicalSex() {
        return biologicalSex;
    }

    public Questionnaire biologicalSex(Integer biologicalSex) {
        this.biologicalSex = biologicalSex;
        return this;
    }

    public void setBiologicalSex(Integer biologicalSex) {
        this.biologicalSex = biologicalSex;
    }

    public Integer getGenderIdentity() {
        return genderIdentity;
    }

    public Questionnaire genderIdentity(Integer genderIdentity) {
        this.genderIdentity = genderIdentity;
        return this;
    }

    public void setGenderIdentity(Integer genderIdentity) {
        this.genderIdentity = genderIdentity;
    }

    public Integer getSexualOrientation() {
        return sexualOrientation;
    }

    public Questionnaire sexualOrientation(Integer sexualOrientation) {
        this.sexualOrientation = sexualOrientation;
        return this;
    }

    public void setSexualOrientation(Integer sexualOrientation) {
        this.sexualOrientation = sexualOrientation;
    }

    public Long getAge() {
        return age;
    }

    public Questionnaire age(Long age) {
        this.age = age;
        return this;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public String getAdress() {
        return adress;
    }

    public Questionnaire adress(String adress) {
        this.adress = adress;
        return this;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Integer getLiveIn() {
        return liveIn;
    }

    public Questionnaire liveIn(Integer liveIn) {
        this.liveIn = liveIn;
        return this;
    }

    public void setLiveIn(Integer liveIn) {
        this.liveIn = liveIn;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Questionnaire phoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public Questionnaire email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNationality() {
        return nationality;
    }

    public Questionnaire nationality(String nationality) {
        this.nationality = nationality;
        return this;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public Integer getConnectionWithLgbt() {
        return connectionWithLgbt;
    }

    public Questionnaire connectionWithLgbt(Integer connectionWithLgbt) {
        this.connectionWithLgbt = connectionWithLgbt;
        return this;
    }

    public void setConnectionWithLgbt(Integer connectionWithLgbt) {
        this.connectionWithLgbt = connectionWithLgbt;
    }

    public Integer getExtent() {
        return extent;
    }

    public Questionnaire extent(Integer extent) {
        this.extent = extent;
        return this;
    }

    public void setExtent(Integer extent) {
        this.extent = extent;
    }

    public ZonedDateTime getTrueDateOfIncident() {
        return trueDateOfIncident;
    }

    public Questionnaire trueDateOfIncident(ZonedDateTime trueDateOfIncident) {
        this.trueDateOfIncident = trueDateOfIncident;
        return this;
    }

    public void setTrueDateOfIncident(ZonedDateTime trueDateOfIncident) {
        this.trueDateOfIncident = trueDateOfIncident;
    }

    public String getAdressIncident() {
        return adressIncident;
    }

    public Questionnaire adressIncident(String adressIncident) {
        this.adressIncident = adressIncident;
        return this;
    }

    public void setAdressIncident(String adressIncident) {
        this.adressIncident = adressIncident;
    }

    public Integer getPlaceIncident() {
        return placeIncident;
    }

    public Questionnaire placeIncident(Integer placeIncident) {
        this.placeIncident = placeIncident;
        return this;
    }

    public void setPlaceIncident(Integer placeIncident) {
        this.placeIncident = placeIncident;
    }

    public String getTypeOfIncident() {
        return typeOfIncident;
    }

    public Questionnaire typeOfIncident(String typeOfIncident) {
        this.typeOfIncident = typeOfIncident;
        return this;
    }

    public void setTypeOfIncident(String typeOfIncident) {
        this.typeOfIncident = typeOfIncident;
    }

    public String getIncidentDescription() {
        return incidentDescription;
    }

    public Questionnaire incidentDescription(String incidentDescription) {
        this.incidentDescription = incidentDescription;
        return this;
    }

    public void setIncidentDescription(String incidentDescription) {
        this.incidentDescription = incidentDescription;
    }

    public Integer getOccurenceIncident() {
        return occurenceIncident;
    }

    public Questionnaire occurenceIncident(Integer occurenceIncident) {
        this.occurenceIncident = occurenceIncident;
        return this;
    }

    public void setOccurenceIncident(Integer occurenceIncident) {
        this.occurenceIncident = occurenceIncident;
    }

    public Integer getNumberPerpetrators() {
        return numberPerpetrators;
    }

    public Questionnaire numberPerpetrators(Integer numberPerpetrators) {
        this.numberPerpetrators = numberPerpetrators;
        return this;
    }

    public void setNumberPerpetrators(Integer numberPerpetrators) {
        this.numberPerpetrators = numberPerpetrators;
    }

    public String getThreatDescription() {
        return threatDescription;
    }

    public Questionnaire threatDescription(String threatDescription) {
        this.threatDescription = threatDescription;
        return this;
    }

    public void setThreatDescription(String threatDescription) {
        this.threatDescription = threatDescription;
    }

    public Integer getIdentityperpetrator() {
        return identityperpetrator;
    }

    public Questionnaire identityperpetrator(Integer identityperpetrator) {
        this.identityperpetrator = identityperpetrator;
        return this;
    }

    public void setIdentityperpetrator(Integer identityperpetrator) {
        this.identityperpetrator = identityperpetrator;
    }

    public String getIdentifierPerpetrator() {
        return identifierPerpetrator;
    }

    public Questionnaire identifierPerpetrator(String identifierPerpetrator) {
        this.identifierPerpetrator = identifierPerpetrator;
        return this;
    }

    public void setIdentifierPerpetrator(String identifierPerpetrator) {
        this.identifierPerpetrator = identifierPerpetrator;
    }

    public String getAgePerpetrator() {
        return agePerpetrator;
    }

    public Questionnaire agePerpetrator(String agePerpetrator) {
        this.agePerpetrator = agePerpetrator;
        return this;
    }

    public void setAgePerpetrator(String agePerpetrator) {
        this.agePerpetrator = agePerpetrator;
    }

    public String getWitnessReaction() {
        return witnessReaction;
    }

    public Questionnaire witnessReaction(String witnessReaction) {
        this.witnessReaction = witnessReaction;
        return this;
    }

    public void setWitnessReaction(String witnessReaction) {
        this.witnessReaction = witnessReaction;
    }

    public String getWitnessDetail() {
        return witnessDetail;
    }

    public Questionnaire witnessDetail(String witnessDetail) {
        this.witnessDetail = witnessDetail;
        return this;
    }

    public void setWitnessDetail(String witnessDetail) {
        this.witnessDetail = witnessDetail;
    }

    public Integer getIncidentLink() {
        return incidentLink;
    }

    public Questionnaire incidentLink(Integer incidentLink) {
        this.incidentLink = incidentLink;
        return this;
    }

    public void setIncidentLink(Integer incidentLink) {
        this.incidentLink = incidentLink;
    }

    public Integer getIncidentLinkIndic() {
        return incidentLinkIndic;
    }

    public Questionnaire incidentLinkIndic(Integer incidentLinkIndic) {
        this.incidentLinkIndic = incidentLinkIndic;
        return this;
    }

    public void setIncidentLinkIndic(Integer incidentLinkIndic) {
        this.incidentLinkIndic = incidentLinkIndic;
    }

    public Integer getImpact() {
        return impact;
    }

    public Questionnaire impact(Integer impact) {
        this.impact = impact;
        return this;
    }

    public void setImpact(Integer impact) {
        this.impact = impact;
    }

    public String getImpactDetail() {
        return impactDetail;
    }

    public Questionnaire impactDetail(String impactDetail) {
        this.impactDetail = impactDetail;
        return this;
    }

    public void setImpactDetail(String impactDetail) {
        this.impactDetail = impactDetail;
    }

    public Integer getMedicalCare() {
        return medicalCare;
    }

    public Questionnaire medicalCare(Integer medicalCare) {
        this.medicalCare = medicalCare;
        return this;
    }

    public void setMedicalCare(Integer medicalCare) {
        this.medicalCare = medicalCare;
    }

    public String getMedicalStaff() {
        return medicalStaff;
    }

    public Questionnaire medicalStaff(String medicalStaff) {
        this.medicalStaff = medicalStaff;
        return this;
    }

    public void setMedicalStaff(String medicalStaff) {
        this.medicalStaff = medicalStaff;
    }

    public String getPsyImact() {
        return psyImact;
    }

    public Questionnaire psyImact(String psyImact) {
        this.psyImact = psyImact;
        return this;
    }

    public void setPsyImact(String psyImact) {
        this.psyImact = psyImact;
    }

    public Integer getSupportKind() {
        return supportKind;
    }

    public Questionnaire supportKind(Integer supportKind) {
        this.supportKind = supportKind;
        return this;
    }

    public void setSupportKind(Integer supportKind) {
        this.supportKind = supportKind;
    }

    public Integer getPolice() {
        return police;
    }

    public Questionnaire police(Integer police) {
        this.police = police;
        return this;
    }

    public void setPolice(Integer police) {
        this.police = police;
    }

    public String getPoliceDesc() {
        return policeDesc;
    }

    public Questionnaire policeDesc(String policeDesc) {
        this.policeDesc = policeDesc;
        return this;
    }

    public void setPoliceDesc(String policeDesc) {
        this.policeDesc = policeDesc;
    }

    public String getForciblyPolice() {
        return forciblyPolice;
    }

    public Questionnaire forciblyPolice(String forciblyPolice) {
        this.forciblyPolice = forciblyPolice;
        return this;
    }

    public void setForciblyPolice(String forciblyPolice) {
        this.forciblyPolice = forciblyPolice;
    }

    public String getArrestPeriod() {
        return arrestPeriod;
    }

    public Questionnaire arrestPeriod(String arrestPeriod) {
        this.arrestPeriod = arrestPeriod;
        return this;
    }

    public void setArrestPeriod(String arrestPeriod) {
        this.arrestPeriod = arrestPeriod;
    }

    public Integer getArrestMotif() {
        return arrestMotif;
    }

    public Questionnaire arrestMotif(Integer arrestMotif) {
        this.arrestMotif = arrestMotif;
        return this;
    }

    public void setArrestMotif(Integer arrestMotif) {
        this.arrestMotif = arrestMotif;
    }

    public String getArrestDetail() {
        return arrestDetail;
    }

    public Questionnaire arrestDetail(String arrestDetail) {
        this.arrestDetail = arrestDetail;
        return this;
    }

    public void setArrestDetail(String arrestDetail) {
        this.arrestDetail = arrestDetail;
    }

    public Integer getIncidentReport() {
        return incidentReport;
    }

    public Questionnaire incidentReport(Integer incidentReport) {
        this.incidentReport = incidentReport;
        return this;
    }

    public void setIncidentReport(Integer incidentReport) {
        this.incidentReport = incidentReport;
    }

    public String getAnythingToAdd() {
        return anythingToAdd;
    }

    public Questionnaire anythingToAdd(String anythingToAdd) {
        this.anythingToAdd = anythingToAdd;
        return this;
    }

    public void setAnythingToAdd(String anythingToAdd) {
        this.anythingToAdd = anythingToAdd;
    }

    public ZonedDateTime getCreationDate() {
        return creationDate;
    }

    public Questionnaire creationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Questionnaire questionnaire = (Questionnaire) o;
        if (questionnaire.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), questionnaire.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Questionnaire{" +
            "id=" + getId() +
            ", fullName='" + getFullName() + "'" +
            ", biologicalSex='" + getBiologicalSex() + "'" +
            ", genderIdentity='" + getGenderIdentity() + "'" +
            ", sexualOrientation='" + getSexualOrientation() + "'" +
            ", age='" + getAge() + "'" +
            ", adress='" + getAdress() + "'" +
            ", liveIn='" + getLiveIn() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", email='" + getEmail() + "'" +
            ", nationality='" + getNationality() + "'" +
            ", connectionWithLgbt='" + getConnectionWithLgbt() + "'" +
            ", extent='" + getExtent() + "'" +
            ", trueDateOfIncident='" + getTrueDateOfIncident() + "'" +
            ", adressIncident='" + getAdressIncident() + "'" +
            ", placeIncident='" + getPlaceIncident() + "'" +
            ", typeOfIncident='" + getTypeOfIncident() + "'" +
            ", incidentDescription='" + getIncidentDescription() + "'" +
            ", occurenceIncident='" + getOccurenceIncident() + "'" +
            ", numberPerpetrators='" + getNumberPerpetrators() + "'" +
            ", threatDescription='" + getThreatDescription() + "'" +
            ", identityperpetrator='" + getIdentityperpetrator() + "'" +
            ", identifierPerpetrator='" + getIdentifierPerpetrator() + "'" +
            ", agePerpetrator='" + getAgePerpetrator() + "'" +
            ", witnessReaction='" + getWitnessReaction() + "'" +
            ", witnessDetail='" + getWitnessDetail() + "'" +
            ", incidentLink='" + getIncidentLink() + "'" +
            ", incidentLinkIndic='" + getIncidentLinkIndic() + "'" +
            ", impact='" + getImpact() + "'" +
            ", impactDetail='" + getImpactDetail() + "'" +
            ", medicalCare='" + getMedicalCare() + "'" +
            ", medicalStaff='" + getMedicalStaff() + "'" +
            ", psyImact='" + getPsyImact() + "'" +
            ", supportKind='" + getSupportKind() + "'" +
            ", police='" + getPolice() + "'" +
            ", policeDesc='" + getPoliceDesc() + "'" +
            ", forciblyPolice='" + getForciblyPolice() + "'" +
            ", arrestPeriod='" + getArrestPeriod() + "'" +
            ", arrestMotif='" + getArrestMotif() + "'" +
            ", arrestDetail='" + getArrestDetail() + "'" +
            ", incidentReport='" + getIncidentReport() + "'" +
            ", anythingToAdd='" + getAnythingToAdd() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            "}";
    }
}
