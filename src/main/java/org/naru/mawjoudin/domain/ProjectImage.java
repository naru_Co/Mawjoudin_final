package org.naru.mawjoudin.domain;
import org.naru.mawjoudin.domain.Project;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public class ProjectImage {

    private Project project;
    private byte [] file;
    private String name;
    private String type;
   
    public ProjectImage(){
        
    }

    public ProjectImage(Project project, byte [] file,String name,String type){
     
        this.project=project;
        this.file=file;
        this.name=name;
        this.type=type;
  }   
   public Project getProject() {
        return project;
    }
   public void setProject(Project project) {
        this.project = project;
    }
    
    public byte [] getFile() {
        return file;
    }

    public void setFile(byte [] file) {
        this.file = file;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}