package org.naru.mawjoudin.domain;
import org.naru.mawjoudin.domain.News;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public class NewsImage {

    private News news;
    private byte [] file;
    private String name;
    private String type;
   
    public NewsImage(){
        
    }

    public NewsImage(News news, byte [] file,String name,String type){
     
        this.news=news;
        this.file=file;
        this.name=name;
        this.type=type;
  }   
   public News getNews() {
        return news;
    }
   public void setNews(News news) {
        this.news = news;
    }
    
    public byte [] getFile() {
        return file;
    }

    public void setFile(byte [] file) {
        this.file = file;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}