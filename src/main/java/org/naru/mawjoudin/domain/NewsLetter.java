package org.naru.mawjoudin.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A NewsLetter.
 */
@Entity
@Table(name = "news_letter")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class NewsLetter implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "creation_date")
    private ZonedDateTime creationDate;

    @Column(name = "sending_date")
    private ZonedDateTime sendingDate;

    @Column(name = "content")
    private String content;

    @Column(name = "status")
    private Integer status;

    @OneToMany(mappedBy = "newsLetter")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Image> images = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public NewsLetter title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ZonedDateTime getCreationDate() {
        return creationDate;
    }

    public NewsLetter creationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public ZonedDateTime getSendingDate() {
        return sendingDate;
    }

    public NewsLetter sendingDate(ZonedDateTime sendingDate) {
        this.sendingDate = sendingDate;
        return this;
    }

    public void setSendingDate(ZonedDateTime sendingDate) {
        this.sendingDate = sendingDate;
    }

    public String getContent() {
        return content;
    }

    public NewsLetter content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getStatus() {
        return status;
    }

    public NewsLetter status(Integer status) {
        this.status = status;
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Set<Image> getImages() {
        return images;
    }

    public NewsLetter images(Set<Image> images) {
        this.images = images;
        return this;
    }

    public NewsLetter addImages(Image image) {
        this.images.add(image);
        image.setNewsLetter(this);
        return this;
    }

    public NewsLetter removeImages(Image image) {
        this.images.remove(image);
        image.setNewsLetter(null);
        return this;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NewsLetter newsLetter = (NewsLetter) o;
        if (newsLetter.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), newsLetter.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NewsLetter{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", sendingDate='" + getSendingDate() + "'" +
            ", content='" + getContent() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
