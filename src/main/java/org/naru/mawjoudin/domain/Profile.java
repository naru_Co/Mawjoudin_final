package org.naru.mawjoudin.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Profile.
 */
@Entity
@Table(name = "profile")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Profile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "bio")
    private String bio;

    @Column(name = "birthdate")
    private LocalDate birthdate;

    @Column(name = "adress")
    private String adress;

    @Column(name = "nickname")
    private String nickname;

    @Column(name = "membership")
    private Boolean membership;

    @Column(name = "motivation")
    private String motivation;

    @OneToOne
    @JoinColumn(unique = true)
    private Image image;

    @ManyToMany(mappedBy = "profiles")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Agenda> agenda = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public Profile firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Profile lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBio() {
        return bio;
    }

    public Profile bio(String bio) {
        this.bio = bio;
        return this;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public Profile birthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
        return this;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public String getAdress() {
        return adress;
    }

    public Profile adress(String adress) {
        this.adress = adress;
        return this;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getNickname() {
        return nickname;
    }

    public Profile nickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Boolean isMembership() {
        return membership;
    }

    public Profile membership(Boolean membership) {
        this.membership = membership;
        return this;
    }

    public void setMembership(Boolean membership) {
        this.membership = membership;
    }

    public String getMotivation() {
        return motivation;
    }

    public Profile motivation(String motivation) {
        this.motivation = motivation;
        return this;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    public Image getImage() {
        return image;
    }

    public Profile image(Image image) {
        this.image = image;
        return this;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Set<Agenda> getAgenda() {
        return agenda;
    }

    public Profile agenda(Set<Agenda> agenda) {
        this.agenda = agenda;
        return this;
    }

    public Profile addAgenda(Agenda agenda) {
        this.agenda.add(agenda);
        agenda.getProfiles().add(this);
        return this;
    }

    public Profile removeAgenda(Agenda agenda) {
        this.agenda.remove(agenda);
        agenda.getProfiles().remove(this);
        return this;
    }

    public void setAgenda(Set<Agenda> agenda) {
        this.agenda = agenda;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Profile profile = (Profile) o;
        if (profile.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), profile.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Profile{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", bio='" + getBio() + "'" +
            ", birthdate='" + getBirthdate() + "'" +
            ", adress='" + getAdress() + "'" +
            ", nickname='" + getNickname() + "'" +
            ", membership='" + isMembership() + "'" +
            ", motivation='" + getMotivation() + "'" +
            "}";
    }
}
