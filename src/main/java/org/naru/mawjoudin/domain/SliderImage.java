package org.naru.mawjoudin.domain;
import org.naru.mawjoudin.domain.Slider;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public class SliderImage {

    private Slider slider;
    private byte [] file;
   
   
    public SliderImage(){
        
    }

    public SliderImage(Slider slider, byte [] file){
     
        this.slider=slider;
        this.file=file;
       
  }   
   public Slider getSlider() {
        return slider;
    }
   public void setSlider(Slider slider) {
        this.slider = slider;
    }
    
    public byte [] getFile() {
        return file;
    }

    public void setFile(byte [] file) {
        this.file = file;
    }
   
}