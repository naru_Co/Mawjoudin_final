package org.naru.mawjoudin.domain;
import org.naru.mawjoudin.domain.Partner;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public class PartnerImage {

    private Partner partner;
    private byte [] file;
    private String name;
    private String type;
   
    public PartnerImage(){
        
    }

    public PartnerImage(Partner partner, byte [] file,String name,String type){
     
        this.partner=partner;
        this.file=file;
        this.name=name;
        this.type=type;
  }   
   public Partner getPartner() {
        return partner;
    }
   public void setPartner(Partner partner) {
        this.partner = partner;
    }
    
    public byte [] getFile() {
        return file;
    }

    public void setFile(byte [] file) {
        this.file = file;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}