package org.naru.mawjoudin.domain;
import org.naru.mawjoudin.domain.Blog;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public class BlogImage {

    private Blog blog;
    private byte [] file;
    private String name;
    private String type;
   
    public BlogImage(){
        
    }

    public BlogImage(Blog blog, byte [] file,String name,String type){
     
        this.blog=blog;
        this.file=file;
        this.name=name;
        this.type=type;
  }   
   public Blog getBlog() {
        return blog;
    }
   public void setProfile(Blog blog) {
        this.blog = blog;
    }
    
    public byte [] getFile() {
        return file;
    }

    public void setFile(byte [] file) {
        this.file = file;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}