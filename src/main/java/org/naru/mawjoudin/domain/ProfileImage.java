package org.naru.mawjoudin.domain;
import org.naru.mawjoudin.domain.Profile;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public class ProfileImage {

    private Profile profile;
    private byte [] file;
    private String name;
    private String type;
   
    public ProfileImage(){
        
    }

    public ProfileImage(Profile profile, byte [] file,String name,String type){
     
        this.profile=profile;
        this.file=file;
        this.name=name;
        this.type=type;
  }   
   public Profile getProfile() {
        return profile;
    }
   public void setProfile(Profile profile) {
        this.profile = profile;
    }
    
    public byte [] getFile() {
        return file;
    }

    public void setFile(byte [] file) {
        this.file = file;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}