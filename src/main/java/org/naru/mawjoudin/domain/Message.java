package org.naru.mawjoudin.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Message.
 */
@Entity
@Table(name = "message")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "content")
    private String content;

    @Column(name = "creation_date")
    private ZonedDateTime creationDate;

    @Column(name = "state")
    private Long state;

    @Column(name = "seen_date")
    private ZonedDateTime seenDate;

    @Column(name = "jhi_uid")
    private String uid;

    @OneToOne
    @JoinColumn(unique = true)
    private Image image;

    @ManyToOne
    private Profile sender;

    @ManyToOne
    private Profile receiver;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public Message content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ZonedDateTime getCreationDate() {
        return creationDate;
    }

    public Message creationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(ZonedDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public Long getState() {
        return state;
    }

    public Message state(Long state) {
        this.state = state;
        return this;
    }

    public void setState(Long state) {
        this.state = state;
    }

    public ZonedDateTime getSeenDate() {
        return seenDate;
    }

    public Message seenDate(ZonedDateTime seenDate) {
        this.seenDate = seenDate;
        return this;
    }

    public void setSeenDate(ZonedDateTime seenDate) {
        this.seenDate = seenDate;
    }

    public String getUid() {
        return uid;
    }

    public Message uid(String uid) {
        this.uid = uid;
        return this;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Image getImage() {
        return image;
    }

    public Message image(Image image) {
        this.image = image;
        return this;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Profile getSender() {
        return sender;
    }

    public Message sender(Profile profile) {
        this.sender = profile;
        return this;
    }

    public void setSender(Profile profile) {
        this.sender = profile;
    }

    public Profile getReceiver() {
        return receiver;
    }

    public Message receiver(Profile profile) {
        this.receiver = profile;
        return this;
    }

    public void setReceiver(Profile profile) {
        this.receiver = profile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Message message = (Message) o;
        if (message.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), message.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Message{" +
            "id=" + getId() +
            ", content='" + getContent() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", state='" + getState() + "'" +
            ", seenDate='" + getSeenDate() + "'" +
            ", uid='" + getUid() + "'" +
            "}";
    }
}
