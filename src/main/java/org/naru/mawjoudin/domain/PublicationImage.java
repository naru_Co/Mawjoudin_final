package org.naru.mawjoudin.domain;
import org.naru.mawjoudin.domain.Publication;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public class PublicationImage {

    private Publication publication;
    private byte [] file;
    private String name;
    private String type;
   
    public PublicationImage(){
        
    }

    public PublicationImage(Publication publication, byte [] file,String name,String type){
     
        this.publication=publication;
        this.file=file;
        this.name=name;
        this.type=type;
  }   
   public Publication getPublication() {
        return publication;
    }
   public void setPublication(Publication publication) {
        this.publication = publication;
    }
    
    public byte [] getFile() {
        return file;
    }

    public void setFile(byte [] file) {
        this.file = file;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}