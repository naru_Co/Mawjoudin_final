package org.naru.mawjoudin.domain;
import org.naru.mawjoudin.domain.Resource;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public class ResourceFile {

    private Resource resource;
    private byte [] file;
    private String name;
    private String type;
   
    public ResourceFile(){
        
    }

    public ResourceFile(Resource resource, byte [] file,String name,String type){
     
        this.resource=resource;
        this.file=file;
        this.name=name;
        this.type=type;
  }   
   public Resource getResource() {
        return resource;
    }
   public void setResource(Resource resource) {
        this.resource = resource;
    }
    
    public byte [] getFile() {
        return file;
    }

    public void setFile(byte [] file) {
        this.file = file;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}