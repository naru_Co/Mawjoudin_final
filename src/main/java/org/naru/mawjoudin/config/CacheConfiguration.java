package org.naru.mawjoudin.config;

import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(org.naru.mawjoudin.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Profile.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Profile.class.getName() + ".agenda", jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Image.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.About.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Project.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.News.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Partner.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Glossaire.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Form.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.NewsLetter.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.NewsLetter.class.getName() + ".images", jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Agenda.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Agenda.class.getName() + ".profiles", jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Subject.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Subject.class.getName() + ".comments", jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Comment.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.ImageGallery.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Friend.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Publication.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Publication.class.getName() + ".comments", jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Message.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Blog.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.ImageGallery.class.getName() + ".images", jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.SiteDescription.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.ChatControl.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Slider.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Qanda.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Resource.class.getName(), jcacheConfiguration);
            cm.createCache(org.naru.mawjoudin.domain.Questionnaire.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
