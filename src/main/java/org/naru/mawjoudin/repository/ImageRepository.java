package org.naru.mawjoudin.repository;

import java.util.List;

import org.naru.mawjoudin.domain.Image;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Image entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ImageRepository extends JpaRepository<Image,Long> {


Page<Image> findAllByImageGallery_id(long imageGallery_id, Pageable pageable);

    
    
}
