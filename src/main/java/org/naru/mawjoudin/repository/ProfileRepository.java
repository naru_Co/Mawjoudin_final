package org.naru.mawjoudin.repository;

import org.naru.mawjoudin.domain.Profile;
import org.springframework.stereotype.Repository;
import java.util.List;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Profile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProfileRepository extends JpaRepository<Profile,Long> {
    List<Profile> findAllByFirstNameContainingIgnoreCase(String title);

    
}
