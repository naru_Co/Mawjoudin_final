package org.naru.mawjoudin.repository;

import org.naru.mawjoudin.domain.ImageGallery;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.jpa.repository.*;
import java.util.*;


/**
 * Spring Data JPA repository for the ImageGallery entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ImageGalleryRepository extends JpaRepository<ImageGallery,Long> {
    ImageGallery  findOneByProfile_idAndType(long profile_id,Integer type);

    List<ImageGallery> findAllByProfile_idAndType(long profil_id,Integer type);
    
}
