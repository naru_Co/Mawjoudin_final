package org.naru.mawjoudin.repository;

import org.naru.mawjoudin.domain.Qanda;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Qanda entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QandaRepository extends JpaRepository<Qanda,Long> {
    
}
