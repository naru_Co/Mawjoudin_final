package org.naru.mawjoudin.repository;

import org.naru.mawjoudin.domain.Publication;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.jpa.repository.*;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Spring Data JPA repository for the Publication entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PublicationRepository extends JpaRepository<Publication,Long> {
    Page<Publication> findAllByProfile_id(long profil_id, Pageable pageable);

    List<Publication> findAllByProfile_idAndCreationDateAfter(long profil_id,ZonedDateTime yesterday);

    
}
