package org.naru.mawjoudin.repository;

import java.util.List;
import org.naru.mawjoudin.domain.Resource;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Resource entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ResourceRepository extends JpaRepository<Resource,Long> {

	List<Resource> findAllByDescriptionContainingIgnoreCase(String desc);

	List<Resource> findAllByDescriptionContainingIgnoreCaseOrTitleContainingIgnoreCase(String desc, String desc2);
    
}
