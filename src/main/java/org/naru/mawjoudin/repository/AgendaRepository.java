package org.naru.mawjoudin.repository;

import org.naru.mawjoudin.domain.Agenda;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * Spring Data JPA repository for the Agenda entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgendaRepository extends JpaRepository<Agenda,Long> {
    
    @Query("select distinct agenda from Agenda agenda left join fetch agenda.profiles")
    List<Agenda> findAllWithEagerRelationships();

    @Query("select agenda from Agenda agenda left join fetch agenda.profiles where agenda.id =:id")
    Agenda findOneWithEagerRelationships(@Param("id") Long id);

    List<Agenda> findAllByProfiles_id(long profil_id);

	List<Agenda> findAllWithEagerRelationshipsByProfiles_idAndStartsAtBeforeAndEndsAtAfter(Long profil_id, ZonedDateTime now,
			ZonedDateTime now2);
    
}
