package org.naru.mawjoudin.repository;

import org.naru.mawjoudin.domain.SiteDescription;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the SiteDescription entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SiteDescriptionRepository extends JpaRepository<SiteDescription,Long> {
    
}
