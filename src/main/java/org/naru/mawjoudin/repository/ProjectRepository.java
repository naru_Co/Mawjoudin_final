package org.naru.mawjoudin.repository;

import java.time.ZonedDateTime;
import java.util.List;
import org.naru.mawjoudin.domain.Project;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Project entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProjectRepository extends JpaRepository<Project,Long> {

	List<Project> findAllByCreationDateAfter(ZonedDateTime yesterday);
    
}
