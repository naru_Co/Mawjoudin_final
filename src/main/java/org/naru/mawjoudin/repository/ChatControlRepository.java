package org.naru.mawjoudin.repository;

import org.naru.mawjoudin.domain.ChatControl;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ChatControl entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChatControlRepository extends JpaRepository<ChatControl,Long> {
    
}
