package org.naru.mawjoudin.repository;

import org.naru.mawjoudin.domain.Slider;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Slider entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SliderRepository extends JpaRepository<Slider,Long> {
    
}
