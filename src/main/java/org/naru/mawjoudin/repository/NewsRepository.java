package org.naru.mawjoudin.repository;

import java.time.ZonedDateTime;
import java.util.List;
import org.naru.mawjoudin.domain.News;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the News entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsRepository extends JpaRepository<News,Long> {

	List<News> findAllByCreationDateAfter(ZonedDateTime yesterday);
    
}
