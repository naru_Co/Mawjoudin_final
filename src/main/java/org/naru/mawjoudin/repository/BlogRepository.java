package org.naru.mawjoudin.repository;

import java.util.List;
import org.naru.mawjoudin.domain.Blog;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import java.time.ZonedDateTime;


/**
 * Spring Data JPA repository for the Blog entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BlogRepository extends JpaRepository<Blog,Long> {
    Page<Blog> findAllByProfile_idAndState(long profil_id,long state, Pageable pageable);
    List<Blog> findAllByState(long state);

	List<Blog> findAllByProfile_idAndCreationDateAfterAndState(Long profil_id, ZonedDateTime yesterday, long i);
}
