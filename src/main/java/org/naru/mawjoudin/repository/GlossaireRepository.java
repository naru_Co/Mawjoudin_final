package org.naru.mawjoudin.repository;

import org.naru.mawjoudin.domain.Glossaire;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Glossaire entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GlossaireRepository extends JpaRepository<Glossaire,Long> {
    
}
