package org.naru.mawjoudin.repository;

import java.util.List;
import org.naru.mawjoudin.domain.Friend;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Friend entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FriendRepository extends JpaRepository<Friend,Long> {
    Page<Friend> findAllByReceiver_idAndState(long receiver_id,long state, Pageable pageable);


	List<Friend> findAllByReceiver_idOrSender_idAndState(Long receiver_id, Long receiver_id2, Long state);

    List<Friend> findAllByReceiver_idAndState(Long receiver_id, Long state);

    List<Friend> findAllBySender_idAndState(Long senders_id, Long state);

	Friend findOneByReceiver_idAndSender_id(Long receiver_id, Long sender_id);
    
}
