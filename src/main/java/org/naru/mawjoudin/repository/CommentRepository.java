package org.naru.mawjoudin.repository;

import java.util.List;

import org.naru.mawjoudin.domain.Comment;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Comment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CommentRepository extends JpaRepository<Comment,Long> {
    
    List<Comment> findAllBySubjectId(long subject_id);
    Page<Comment> findAllBySubject_id(long subject_id, Pageable pageable);
    List<Comment> findAllByPublication_id(long pub_id);
    
}
