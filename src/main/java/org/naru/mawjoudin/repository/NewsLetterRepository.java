package org.naru.mawjoudin.repository;

import org.naru.mawjoudin.domain.NewsLetter;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the NewsLetter entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NewsLetterRepository extends JpaRepository<NewsLetter,Long> {
    
}
