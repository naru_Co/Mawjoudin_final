package org.naru.mawjoudin.repository;

import java.util.List;
import org.naru.mawjoudin.domain.Message;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Message entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MessageRepository extends JpaRepository<Message,Long> {

	List<Message> findByUid(String uid);

	List<Message> findBySeenDateIsNullAndReceiver_Id(Long id);
    
}
