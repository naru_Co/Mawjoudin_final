package org.naru.mawjoudin.repository;

import org.naru.mawjoudin.domain.Form;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Form entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FormRepository extends JpaRepository<Form,Long> {
    
}
